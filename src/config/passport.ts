import passport from "passport";
import {generateJWT, validPassword} from '../models/public/IAccount';
import {Strategy as LocalStrategy} from "passport-local";
import {Strategy as JwtStrategy, ExtractJwt} from 'passport-jwt';
import {Request, Response, NextFunction} from "express";
import httpResponseHandler from "../cors/httpResponseHandler";
import AuthenticationServices from "../services/commonServices/AuthenticationServices";
import DatabaseCommand from "../utils/DatabaseCommand";
import UserServices from "../services/public/UserServices";
import CompanyServices from "../services/public/CompanyServices";
import BranchServices from "../services/BranchServices";
import {IRequestUser} from "../models/temp/IRequestUser";
import CurrencyServices from "../services/public/CurrencyServices";

passport.use('local', new LocalStrategy(async (username: string, password: string, done: CallableFunction) => {
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const splitFromUsername = username.split('@');
      const companyCode = splitFromUsername[1] ? splitFromUsername[1].toUpperCase() : '';
      const availableCompanies = await CompanyServices.getCompanies([{
        code: companyCode, statusid: 2
      }], DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!availableCompanies.length) return done(null, false, `Company has code ${companyCode} not found or unavailable.`);
      const businessContract = await AuthenticationServices.checkBusinessContractOfAvailableCompany(availableCompanies[0], client);
      await DatabaseCommand.commitTransaction(client);
      if (!businessContract) return done(null, false, `Company has code ${splitFromUsername[1]} not have a valid business contract.`);
      const account = await AuthenticationServices.getAccountByUsernameAndCompanyId(username, businessContract.companyid);
      if (!account) return done(null, false, `Account has username ${username} not found or unavailable.`);
      if (!validPassword(password, account)) return done(null, false, `Incorrect password.`);
      const users = await UserServices.getUsers([{
        id: account.userid, companyid: account.companyid, statusid: 2
      }]);
      if (!users.length) return done(null, false, `User has id ${account.userid} not found or unavailable.`);
      return done(null, {account, businessContract, company: availableCompanies[0], ...users[0]} as IRequestUser);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return done(e, false);
    } finally {
      client.release();
    }
  }
));

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET
};

passport.use('jwt', new JwtStrategy(jwtOptions, async (jwt_payload, done) => {
  const client = await DatabaseCommand.newClient();
  try {
    await DatabaseCommand.beginTransaction(client);
    const companyId = jwt_payload.companyid;
    const availableCompanies = await CompanyServices.getCompanies([{
      id: companyId, statusid: 2
    }], DatabaseCommand.lockStrength.KEY_SHARE, client);
    if (!availableCompanies.length) return done(null, false, `Company has id ${companyId} not found or unavailable.`);
    const businessContract = await AuthenticationServices.checkBusinessContractOfAvailableCompany(availableCompanies[0], client);
    await DatabaseCommand.commitTransaction(client);
    if (!businessContract) return done(null, false, `Company has id ${companyId} not have a valid business contract.`);
    const account = await AuthenticationServices.getAccountByIdAndCompanyId(jwt_payload.id, businessContract.companyid);
    if (!account) return done(null, false, `Account has id ${jwt_payload.id} not found or unavailable.`);
    const users = await UserServices.getUsers([{
      id: account.userid, companyid: account.companyid, statusid: 2
    }]);
    if (!users.length) return done(null, false, `User has id ${account.userid} not found or unavailable.`);
    return done(null, {account, businessContract, company: availableCompanies[0], ...users[0]} as IRequestUser);
  } catch (e) {
    console.log(e);
    await DatabaseCommand.rollbackTransaction(client);
    return done(e, false);
  } finally {
    client.release();
  }
}));

export const passportJwt = (req: Request, res: Response, next: NextFunction) => {
  passport.authenticate('jwt', async (error, user, info) => {
    if (error) return httpResponseHandler.responseErrorToClient(res, 500);
    if (user) {
      req.user = user;
      next();
    } else httpResponseHandler.responseErrorToClient(res, 401, info);
  })(req, res, next);
}

export const passportLocal = (req: Request, res: Response, next: NextFunction) => {
  passport.authenticate('local', async (error, account, info) => {
    if (error) return res.status(500).json({messages: 'Something wrong.'});
    if (account) {
      const requestUser = account as IRequestUser;
      try {
        const roles = await AuthenticationServices.getRolesAndItsPermissionsOfUser(requestUser, requestUser.businessContract.typeid);
        const token = generateJWT(requestUser.account);
        const branches = await BranchServices.getBranchesOfUser(requestUser, undefined, undefined, true);
        const company = requestUser.company as any;
        company.currency = (await CurrencyServices.getCurrencies([{
          id: company.currencyid
        }]))[0];
        return httpResponseHandler.responseToClient(res, 200, {token, roles, branches, company, user: requestUser.id});
      } catch (e) {
        console.log(e);
        return httpResponseHandler.responseToClient(res, 500);
      }
    } else return httpResponseHandler.responseErrorToClient(res, 401, info);
  })(req, res, next);
}
