import { Router, Application } from 'express';
import AuthenticationController from './AuthenticationController';
import Routes from '../../constant/Routes';
import { passportLocal } from "../../config/passport";
// import AccountValidator from '../../validation/AccountValidation';


const router: Router = Router();

export default (app: Application) => {
  router.route('/login')
    .post(passportLocal);

  // router.route('/register')
  //   .post(AuthenticationController.register);

  app.use(Routes.AUTH, router)
};