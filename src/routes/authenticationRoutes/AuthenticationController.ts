import { validationResult } from "express-validator";
import { Request, Response, NextFunction } from "express";
import { IAccount, generateJWT, setPassword } from "../../models/public/IAccount";
import AccountServices from "../../services/public/AccountServices";
import UserServices from "../../services/public/UserServices";
import httpResponseHandler from "../../cors/httpResponseHandler";
import RoleServices from "../../services/public/RoleServices";

export default {
  /**
   * Create account and return the jwt
   * @param req
   * @param res
   */
  // register: async (req: Request, res: Response) => {
  //   try {
  //     const { userId, validatetoken, username, password } = req.body;
  //     const account = await AccountServices.getAccountByUsername(username);
  //     if (account)
  //       return res.status(409).json({ message: "IAccount already exists" });
  //     const user = await UserServices.getUserById(userId);
  //     if (!user) return res.status(404).json({ message: "User not found." });
  //     const thisDate = new Date();
  //     console.log(thisDate);
  //     if (
  //       user.validatetoken !== validatetoken ||
  //       user.validatetokenexpiredat < thisDate
  //     )
  //       return res
  //         .status(403)
  //         .json({ message: "This validate token is expired." });
  //     let newAccount: IAccount = {
  //       id: 0,
  //       userid: userId,
  //       username: username,
  //       hash: "",
  //       salt: "",
  //       validatetoken: "",
  //       validatetokenexpiredat: new Date(),
  //       statusid: 0,
  //       statuscode: "",
  //       createdat: new Date()
  //     };
  //     setPassword(password, newAccount);
  //     const result: IAccount | null = await AccountServices.createAccount(
  //       newAccount
  //     );
  //     if (result) {
  //       console.log(result);
  //       console.log(result.userid);
  //       // const token: string = generateJWT(result);
  //       // return res.status(201).json({ token });
  //       return httpResponseHandler.responseToClient(res, 201, 'IAccount created');
  //     } else return res.status(400).json({ message: "Can not create account." });
  //   } catch (err) {
  //     console.log(err);
  //     httpResponseHandler.responseErrorToClient(res, 500);
  //   }
  // },
};
