import {Application, Router} from "express";
import {passportJwt} from "../../config/passport";
import checkUser from "../../cors/checkUser";
import ItemTypeController from "./ItemTypeController";
import Routes from "../../constant/Routes";

const router = Router();

export default function(app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, ItemTypeController.getItemTypes)
    .post(passportJwt, checkUser, ItemTypeController.createItemType, ItemTypeController.getItemType);

  router.route('/:typeId')
    .get(passportJwt, checkUser, ItemTypeController.getItemType)
    .put(passportJwt, checkUser, ItemTypeController.updateItemType, ItemTypeController.getItemType)
    .delete(passportJwt, checkUser, ItemTypeController.deleteItemType);

  app.use(Routes.ITEM_TYPES, router);
}
