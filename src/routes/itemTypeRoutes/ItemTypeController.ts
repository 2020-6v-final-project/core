import {NextFunction, Request, Response} from "express";
import httpResponseHandler from "../../cors/httpResponseHandler";
import ItemTypeServices from "../../services/ItemTypeServices";
import {IUser} from "../../models/public/IUser";
import ItemServices from "../../services/ItemServices";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IItem} from "../../models/IItem";
import {IRequestUser} from "../../models/temp/IRequestUser";

export default {

  async getItemTypes(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    try {
      let itemTypes = await ItemTypeServices.getItemTypes([], thisUser.companycode);
      if (typeof (req.query.withitems) !== 'undefined') {
        itemTypes = await Promise.all(itemTypes.map(async itemType => {
          const thisItemType = itemType as any;
          thisItemType.items = await ItemServices.getItems([{
            typeid: thisItemType.id
          }], thisUser.companycode);
          return thisItemType;
        }));
      }
      return httpResponseHandler.responseToClient(res, 200, itemTypes);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async createItemType(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const body = req.body;
    delete body.id;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const type = await ItemTypeServices.addItemType(body, thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      req.params.typeId = type.id.toString();
      next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async getItemType(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    const typeId = parseInt(req.params.typeId);
    try {
      let itemTypes = await ItemTypeServices.getItemTypes([{
        id: typeId
      }], thisUser.companycode);
      if (!itemTypes.length) return httpResponseHandler.responseErrorToClient(res, 404, `Item type has id ${typeId} not found.`);
      const itemType = itemTypes[0] as any;
      itemType.items = await ItemServices.getItems([{
        typeid: itemType.id
      }], thisUser.companycode);
      return httpResponseHandler.responseToClient(res, 200, itemType);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async updateItemType(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const typeId = parseInt(req.params.typeId);
    const body = req.body;
    delete body.id;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const type = (await ItemTypeServices.getItemTypes([{
        id: typeId
      }], thisUser.companycode, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client))[0];
      if (!type) return httpResponseHandler.responseErrorToClient(res, 404, `Item type has id ${typeId} not found.`);
      Object.assign(type, body);
      const updatedType = (await ItemTypeServices.updateItemTypes(type, [{
        id: typeId
      }], thisUser.companycode, client))[0];
      if (!updatedType) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async deleteItemType(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    const typeId = parseInt(req.params.typeId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const type = (await ItemTypeServices.getItemTypes([{
        id: typeId
      }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
      if (!type) return httpResponseHandler.responseErrorToClient(res, 404, `Item type has id ${typeId} not found.`);
      const dependencyItems = await ItemServices.getItems([{
        typeid: typeId
      }], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (dependencyItems.length) return httpResponseHandler.responseErrorToClient(res, 400, `Please delete items of type first.`);
      await ItemTypeServices.deleteItemTypes([{
        id: typeId
      }], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 202);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
