import {Router} from "express";
import {passportJwt} from "../../../../config/passport";
import checkUser from "../../../../cors/checkUser";
import ItemOptionController from "./ItemOptionController";
import ItemController from "../ItemController";

export default function(router: Router) {
	router.route('/:itemId/options')
		.post(passportJwt, checkUser, ItemOptionController.createItemOption, ItemController.getItemById);

	router.route('/:itemId/options/:optionId')
		.put(passportJwt, checkUser, ItemOptionController.updateItemOption, ItemController.getItemById)
		.delete(passportJwt, checkUser, ItemOptionController.deleteItemOption, ItemController.getItemById)
}