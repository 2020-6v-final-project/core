import {Request, Response, NextFunction} from "express";
import httpResponseHandler from "../../../../cors/httpResponseHandler";
import DatabaseCommand from "../../../../utils/DatabaseCommand";
import ItemOptionServices from "../../../../services/ItemOptionServices";
import {IRequestUser} from "../../../../models/temp/IRequestUser";
import OptionServices from "../../../../services/OptionServices";
import ItemOptionIngredientServices from "../../../../services/ItemOptionIngredientServices";
import { IOption } from "../../../../models/IOption";

export default {

	async createItemOption(req: Request, res: Response, next: NextFunction) {
		const thisUser = req.user as IRequestUser;
		const itemId = parseInt(req.params.itemId);
		const bodyOption = req.body as {
			id?: number,
			name: string,
			price: number,
			note: string
			ingredients: {
				ingredientid: number;
				quantity: number;
			}[]
		}
		const client = await DatabaseCommand.newClient();
		try {
			await DatabaseCommand.beginTransaction(client);
			let thisOption: IOption;
			if (bodyOption.id) thisOption = (await OptionServices.getOptions([{
				id: bodyOption.id
			}], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client))[0];
			else thisOption = (await OptionServices.createOptions([{name: bodyOption.name}], thisUser.companycode, client))[0];
			if (thisOption) {
				await ItemOptionServices.createItemOption({
					itemid: itemId, optionid: thisOption.id, note: bodyOption.note, price: bodyOption.price
				}, thisUser.companycode, client);
				await ItemOptionIngredientServices.createItemOptionIngredients(bodyOption.ingredients.map(ingredient => ({
					itemid: itemId, optionid: thisOption.id, ingredientid: ingredient.ingredientid, ingredientquantity: ingredient.quantity
				})), thisUser.companycode, client);
			} else {
				await DatabaseCommand.rollbackTransaction(client);
				return httpResponseHandler.responseErrorToClient(res, 400);
			}
			await DatabaseCommand.commitTransaction(client);
			return next();
		} catch (e) {
			console.log(e);
			await DatabaseCommand.rollbackTransaction(client);
			return httpResponseHandler.responseErrorToClient(res, 500, e.message);
		} finally {
			client.release();
		}
	},
	async updateItemOption(req: Request, res: Response, next: NextFunction) {
		const thisUser = req.user as IRequestUser;
		const itemId = parseInt(req.params.itemId);
		const optionId = parseInt(req.params.optionId);
		const bodyOption = req.body as {
			id: number,
			price: number,
			note: string
			ingredients: {
				ingredientid: number;
				quantity: number;
			}[]
		}
		const client = await DatabaseCommand.newClient();
		try {
			await DatabaseCommand.beginTransaction(client);
			const itemOption = (await ItemOptionServices.getItemOptions([{
				itemid: itemId, optionid: optionId
			}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
			if (!itemOption) return httpResponseHandler.responseErrorToClient(res, 404);
			await ItemOptionServices.updateItemOptions({
				price: bodyOption.price, note: bodyOption.note
			}, [{
				itemid: itemOption.itemid, optionid: itemOption.optionid
			}], thisUser.companycode, client);
			const oldItemOptionIngredients = await ItemOptionIngredientServices.getItemOptionIngredients([{
				itemid: itemOption.itemid, optionid: itemOption.optionid
			}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
			const bodyIngredients = bodyOption.ingredients;
			for (const oldItemOptionIngredient of oldItemOptionIngredients) {
				const foundIndex = bodyIngredients.findIndex(bodyIngredient => bodyIngredient.ingredientid === oldItemOptionIngredient.ingredientid);
				if (foundIndex > -1) {
					await ItemOptionIngredientServices.updateItemOptionIngredients({
						ingredientquantity: bodyIngredients[foundIndex].quantity
					}, [{
						itemid: oldItemOptionIngredient.itemid, optionid: oldItemOptionIngredient.optionid, ingredientid: oldItemOptionIngredient.ingredientid
					}], thisUser.companycode, client);
					bodyIngredients.splice(foundIndex, 1);
				} else {
					await ItemOptionIngredientServices.deleteItemOptionIngredients([{
						itemid: oldItemOptionIngredient.itemid, optionid: oldItemOptionIngredient.optionid, ingredientid: oldItemOptionIngredient.ingredientid
					}], thisUser.companycode, client);
				}
			}
			await ItemOptionIngredientServices.createItemOptionIngredients(bodyIngredients.map(ingredient => ({
				itemid: itemId, optionid: itemOption.optionid, ingredientid: ingredient.ingredientid, ingredientquantity: ingredient.quantity
			})), thisUser.companycode, client);
			await DatabaseCommand.commitTransaction(client);
			return next();
		} catch (e) {
			console.log(e);
			await DatabaseCommand.rollbackTransaction(client);
			return httpResponseHandler.responseErrorToClient(res, 500, e.message);
		} finally {
			client.release();
		}
	},
	async deleteItemOption(req: Request, res: Response, next: NextFunction) {
		const thisUser = req.user as IRequestUser;
		const itemId = parseInt(req.params.itemId);
		const optionId = parseInt(req.params.optionId);
		const client = await DatabaseCommand.newClient();
		try {
			await DatabaseCommand.beginTransaction(client);
			const itemOption = (await ItemOptionServices.getItemOptions([{
				itemid: itemId, optionid: optionId
			}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
			if (!itemOption) return httpResponseHandler.responseErrorToClient(res, 404);
			await ItemOptionIngredientServices.deleteItemOptionIngredients([{
				itemid: itemId, optionid: itemOption.optionid
			}], thisUser.companycode, client);
			await ItemOptionServices.deleteItemOptions([{
				itemid: itemId, optionid: itemOption.optionid
			}], thisUser.companycode, client);
			await DatabaseCommand.commitTransaction(client);
			return next();
		} catch (e) {
			console.log(e);
			await DatabaseCommand.rollbackTransaction(client);
			return httpResponseHandler.responseErrorToClient(res, 500, e.message);
		} finally {
			client.release();
		}
	}
}