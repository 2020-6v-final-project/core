import { NextFunction, Request, Response } from "express";
import httpResponseHandler from "../../../cors/httpResponseHandler";
import ItemServices from "../../../services/ItemServices";
import ItemIngredientService from "../../../services/ItemIngredientServices";
import StatusServices from "../../../services/public/StatusServices";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import { IItemIngredient } from "../../../models/IItemIngredient";
import IngredientServices from "../../../services/IngredientServices";
import {IRequestUser} from "../../../models/temp/IRequestUser";
import ItemOptionServices from "../../../services/ItemOptionServices";
import ItemOptionIngredientServices from "../../../services/ItemOptionIngredientServices";
import {IItem} from "../../../models/IItem";
import ItemTypeServices from "../../../services/ItemTypeServices";
import { IOption } from "../../../models/IOption";
import { IItemOption } from "../../../models/IItemOption";
import { IItemOptionIngredient } from "../../../models/IItemOptionIngredient";
import {IIngredient} from "../../../models/IIngredient";
import OptionServices from "../../../services/OptionServices";

export default {
  createItem: async (req: Request, res: Response) => {
    const bodyItem = req.body;
    const ingredientList = bodyItem.ingredients;
    const optionList = bodyItem.options;
    const thisUser = req.user as IRequestUser;
    delete bodyItem.id;
    delete bodyItem.lastmodified;
    delete bodyItem.statusid;
    delete bodyItem.statuscode;
    delete bodyItem.ingredients;
    delete bodyItem.options;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const newItem = await ItemServices.createItem(bodyItem, thisUser.companycode, client);
      const ingredients = await Promise.all(ingredientList.map(async (ingredient: any) => {
        ingredient = { ...ingredient, itemid: newItem.id };
        return await ItemIngredientService.addOne(ingredient, thisUser.companycode, client);
      }));
      const result = newItem as any;
      result.ingredients = ingredients;
      result.options = [];
      await DatabaseCommand.commitTransaction(client);
      httpResponseHandler.responseToClient(res, 201, result);
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  updateItem: async(req: Request, res: Response, next: NextFunction) => {
    const itemId = parseInt(req.params.itemId);
    const bodyItem = req.body;
    const ingredientList = bodyItem.ingredients as IItemIngredient[];
    const optionList = bodyItem.options as any[];
    const thisUser = req.user as IRequestUser;
    delete bodyItem.id;
    delete bodyItem.lastmodified;
    delete bodyItem.statusid;
    delete bodyItem.statuscode;
    delete bodyItem.ingredients;
    delete bodyItem.options;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let listItem = await ItemServices.getItems([{ id: itemId }], thisUser.companycode, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      let targetItem = listItem[0];
      if (!targetItem) return httpResponseHandler.responseErrorToClient(res, 404, `Item (id: ${itemId}) not found!`);
      targetItem.lastmodified = new Date();
      Object.assign(targetItem, bodyItem);
      await ItemServices.updateItem(targetItem, thisUser.companycode, client);
      // Compare ingredientList diff
      const oldIngredientList = await ItemIngredientService.getAllByItemId(itemId, thisUser.companycode, client, DatabaseCommand.lockStrength.UPDATE);
      for(let oldIngredient of oldIngredientList) {
        const containIndex = ingredientList?.findIndex(ingredient => ingredient.ingredientid === oldIngredient.ingredientid);
        if (containIndex > -1) {
          if (oldIngredient.quantity !== ingredientList[containIndex].quantity) {
            await ItemIngredientService.updateItemIngredient(ingredientList[containIndex], thisUser.companycode, client);
          }
          ingredientList.splice(containIndex, 1);
        } else await ItemIngredientService.removeOne(oldIngredient, thisUser.companycode, client);
      }
      await Promise.all(ingredientList.map(async ingredient => {
        await ItemIngredientService.addOne(ingredient, thisUser.companycode, client);
      }));
      
      // // Compare option list
      // // Old list is oldOptionList, new list is optionList
      // const oldOptionList = await ItemOptionServices.getItemOptions([{itemid: itemId}], thisUser.companycode,
      //    DatabaseCommand.lockStrength.UPDATE, client);
      // //the general section of option that will be updated
      // const updateOptionList = optionList.filter(option => oldOptionList.find(value => value.optionid === option.optionid));
      // const updatedOptionList = await Promise.all(updateOptionList.map(async (option: any) => {
      //   const itemOptionIngredientList = option.itemoptioningredients;
      //   delete option.itemoptioningredients;
      //   //check option change information or not ?
      //   const optionChanges = await oldOptionList.find(value => value.optionid === option.optionid);
      //   if (option.price !== optionChanges?.price || option.note !== optionChanges?.note) {
      //     const updatedOption = await ItemOptionServices.updateItemOptions(option, [{optionid: option.optionid, itemid: itemId}],
      //        thisUser.companycode, client);
      //   }
      //   const oldItemOptionIngredientList = await ItemOptionIngredientServices.getItemOptionIngredients([{itemid: itemId, optionid: option.optionid}],
      //     thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
      //   //Update Item Option Ingredient
      //   //general --> update
      //   const updateItemOptionIngredients = itemOptionIngredientList.filter((itemOptionIngre: any) =>
      //     oldItemOptionIngredientList.find(value => value.ingredientid === itemOptionIngre.ingredientid));
      //   const updatedItemOptionIngredients = await Promise.all(updateItemOptionIngredients.map(async (itemOptionIngre: any) => {
      //     const changeRow = oldItemOptionIngredientList.find((value: any) => value.ingredientid === itemOptionIngre.ingredientid &&
      //     value.ingredientquantity !== itemOptionIngre.ingredientquantity);
      //     if (changeRow) {
      //       changeRow.ingredientquantity = itemOptionIngre.ingredientquantity;
      //       const updatedRow = await ItemOptionIngredientServices.updateItemOptionIngredients(changeRow,
      //          [{itemid: changeRow.itemid, optionid: changeRow.optionid, ingredientid: changeRow.ingredientid}], thisUser.companycode, client);
      //     }
      //   }))
      //   //new
      //   const newItemOptionIngredients = itemOptionIngredientList.filter((itemOptionIngre:any) => !oldItemOptionIngredientList.find(
      //     value => value.ingredientid === itemOptionIngre.ingredientid));
      //   const addedItemOptionIngredients = await Promise.all(newItemOptionIngredients.map(async (itemOptionIngre:any) => {
      //     const newRow = {...itemOptionIngre, itemid: itemId, optionid: option.optionid};
      //     const addedRow = await ItemOptionIngredientServices.createItemOptionIngredient(newRow, thisUser.companycode, client);
      //   }));
      //   //delete
      //   const deleteItemOptionIngredients = oldItemOptionIngredientList.filter((itemOptionIngre:any) => !itemOptionIngredientList.find(
      //     (value:any) => value.ingredientid === itemOptionIngre.ingredientid));
      //   const deletedItemOptionIngredients = await Promise.all(deleteItemOptionIngredients.map(async (itemOptionIngre: any) => {
      //     const deleteRow = {...itemOptionIngre, itemid: itemId, optionid: option.optionid};
      //     const deletedRow = await ItemOptionIngredientServices.deleteItemOptionIngredients([deleteRow], thisUser.companycode, client);
      //   }))
      // }))
      // //the new options
      // const newOptionList = optionList.filter(option => !oldOptionList.find(value => value.optionid === option.optionid));
      // const addedOptionList = await Promise.all(newOptionList.map(async (option: any) => {
      //   const itemOptionIngredientList = option.itemoptioningredients;
      //   delete option.itemoptioningredients;
      //   const newItemOption = await ItemOptionServices.createItemOption({...option, itemid: itemId}, thisUser.companycode, client);
      //   const addedItemOptionIngredients = await Promise.all(itemOptionIngredientList.map(async (itemOptionIngre: any) => {
      //     const addedRow = await ItemOptionIngredientServices.createItemOptionIngredient({...itemOptionIngre, itemid: itemId, optionid: option.optionid },
      //       thisUser.companycode, client);
      //   }))
      // }))
      // //the old options
      // const deleteOptionList = oldOptionList.filter(option => !optionList.find(value => value.optionid === option.optionid));
      // const deletedOptionList = await Promise.all(deleteOptionList.map(async (option:any) => {
      //   const thisItemOptionIngredientList = await ItemOptionIngredientServices.getItemOptionIngredients([{itemid: itemId, optionid: option.optionid}],
      //     thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
      //   for (let itemOptionIngre of thisItemOptionIngredientList) {
      //     await ItemOptionIngredientServices.deleteItemOptionIngredients([{itemid: itemId, optionid: option.optionid, ingredientid: itemOptionIngre.ingredientid}],
      //       thisUser.companycode, client);
      //   }
      //   const deletedRow = await ItemOptionServices.deleteItemOptions([{itemid: itemId, optionid: option.optionid}],
      //     thisUser.companycode, client);
      // }))
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  deactivateItem: async(req: Request, res: Response, next: NextFunction) => {
    const itemId = parseInt(req.params.itemId);
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const listItem = await ItemServices.getItems([{ id: itemId }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
      let targetItem = listItem[0];
      if (!targetItem) return httpResponseHandler.responseErrorToClient(res, 404, `Item (id: ${itemId}) not found!`);
      await StatusServices.setStatusCodeToObjects(ItemServices.IItemTableName, "DISABLED", [{ id: itemId }], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  reactivateItem: async(req: Request, res: Response, next: NextFunction) => {
    const itemId = parseInt(req.params.itemId);
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const listItem = await ItemServices.getItems([{ id: itemId }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
      let targetItem = listItem[0];
      if (!targetItem) return httpResponseHandler.responseErrorToClient(res, 404, `Item (id: ${itemId}) not found!`);
      await StatusServices.setStatusCodeToObjects(ItemServices.IItemTableName, "AVAILABLE", [{ id: itemId }], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  getItems: async(req: Request, res: Response) => {
    const thisUser = req.user as IRequestUser;
    try {
      let items = await ItemServices.getItems([], thisUser.companycode);
      if (typeof (req.query.withingredients) !== "undefined") {
        items = await Promise.all(items.map(async item => {
          const tempItem = item as any;
          tempItem.ingredients = await Promise.all((await ItemIngredientService.getAllByItemId(item.id, thisUser.companycode)).map(async itemIngredient => {
            const ingredient = (await IngredientServices.getIngredients([{
              id: itemIngredient.ingredientid
            }], thisUser.companycode))[0];
            return Object.assign(itemIngredient, ingredient);
          }));
          return tempItem;
        }));
      }
      if (typeof (req.query.withtypename) !== 'undefined') {
        items = await Promise.all(items.map(async item => {
          const thisItem = item as IItem & {
            typename: string;
          }
          const type = (await ItemTypeServices.getItemTypes([{
            id: thisItem.typeid
          }], thisUser.companycode))[0];
          if (type) thisItem.typename = type.name;
          return thisItem;
        }));
      }
      if (typeof (req.query.withoptions) !== "undefined") {
        items = await Promise.all(items.map(async item => {
          const tempItem = item as IItem & {
            options: (IOption & IItemOption & {
              ingredients: (IIngredient & IItemOptionIngredient)[]
            })[]
          };
          tempItem.options = await Promise.all((await ItemOptionServices.getItemOptions([{
            itemid: tempItem.id
          }], thisUser.companycode)).map(async itemOption => {
            let tempOption = (await OptionServices.getOptions([{
              id: itemOption.optionid
            }], thisUser.companycode))[0] as IOption & IItemOption & {
              ingredients: (IIngredient & IItemOptionIngredient)[]
            };
            Object.assign(tempOption, itemOption);
            tempOption.ingredients = await Promise.all((await ItemOptionIngredientServices.getItemOptionIngredients([{
              itemid: tempOption.itemid, optionid: tempOption.optionid
            }], thisUser.companycode)).map(async itemOptionIngredient => {
              const tempIngredient = itemOptionIngredient as IIngredient & IItemOptionIngredient;
              const ingredient = (await IngredientServices.getIngredients([{
                id: itemOptionIngredient.ingredientid
              }], thisUser.companycode))[0];
              Object.assign(tempIngredient, ingredient);
              return tempIngredient;
            }));
            return tempOption;
            // tempItemOption.itemoptioningredients = await Promise.all((await ItemOptionIngredientServices.getItemOptionIngredients([{
            //   itemid: tempItemOption.itemid, optionid: tempItemOption.optionid
            // }], thisUser.companycode)).map(async itemOptionIngredient => {
            //   const ingredient = (await IngredientServices.getIngredients([{
            //     id: itemOptionIngredient.ingredientid
            //   }], thisUser.companycode))[0];
            //   return Object.assign(itemOptionIngredient, ingredient);
            // }));
            // return tempItemOption;
          }));
          return tempItem;
        }));
      }
      httpResponseHandler.responseToClient(res, 200, items);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  getItemById: async(req: Request, res: Response) => {
    const itemId = parseInt(req.params.itemId);
    const thisUser = req.user as IRequestUser;
    try {
      const itemList = await ItemServices.getItems([{ id: itemId }], thisUser.companycode, undefined, undefined);
      let targetItem = itemList[0] as any;
      if (!targetItem) return httpResponseHandler.responseErrorToClient(res, 404, `Item (id: ${itemId}) not found!`);
      targetItem.ingredients = await Promise.all((await ItemIngredientService.getAllByItemId(targetItem.id, thisUser.companycode)).map(async itemIngredient => {
        const ingredient = (await IngredientServices.getIngredients([{
          id: itemIngredient.ingredientid
        }], thisUser.companycode))[0];
        return Object.assign(itemIngredient, ingredient);
      }));
      targetItem.options = await Promise.all((await ItemOptionServices.getItemOptions([{
        itemid: targetItem.id
      }], thisUser.companycode)).map(async itemOption => {
        let tempOption = (await OptionServices.getOptions([{
          id: itemOption.optionid
        }], thisUser.companycode))[0] as IOption & IItemOption & {
          ingredients: (IIngredient & IItemOptionIngredient)[]
        };
        Object.assign(tempOption, itemOption);
        tempOption.ingredients = await Promise.all((await ItemOptionIngredientServices.getItemOptionIngredients([{
          itemid: tempOption.itemid, optionid: tempOption.optionid
        }], thisUser.companycode)).map(async itemOptionIngredient => {
          const tempIngredient = itemOptionIngredient as IIngredient & IItemOptionIngredient;
          const ingredient = (await IngredientServices.getIngredients([{
            id: itemOptionIngredient.ingredientid
          }], thisUser.companycode))[0];
          Object.assign(tempIngredient, ingredient);
          return tempIngredient;
        }));
        return tempOption;
        // targetItemOption.itemoptioningredients = await Promise.all((await ItemOptionIngredientServices.getItemOptionIngredients([{
        //   itemid: tempItemOption.itemid, optionid: tempItemOption.optionid
        // }], thisUser.companycode)).map(async itemOptionIngredient => {
        //   const ingredient = (await IngredientServices.getIngredients([{
        //     id: itemOptionIngredient.ingredientid
        //   }], thisUser.companycode))[0];
        //   return Object.assign(itemOptionIngredient, ingredient);
        // }));
        // return tempItemOption;
      }));
      // targetItem.options = await Promise.all((await ItemOptionServices.getItemOptions([{
      //   itemid: targetItem.id
      // }], thisUser.companycode)).map(async itemOption => {
      //   const tempItemOption = itemOption as any;
      //   tempItemOption.itemoptioningredients = await Promise.all((await ItemOptionIngredientServices.getItemOptionIngredients([{
      //     itemid: tempItemOption.itemid, optionid: tempItemOption.optionid
      //   }], thisUser.companycode)).map(async itemOptionIngredient => {
      //     const ingredient = (await IngredientServices.getIngredients([{
      //       id: itemOptionIngredient.ingredientid
      //     }], thisUser.companycode))[0];
      //     return Object.assign(itemOptionIngredient, ingredient);
      //   }));
      //   return tempItemOption;
      // }));
      const type = (await ItemTypeServices.getItemTypes([{
        id: targetItem.typeid
      }], thisUser.companycode))[0];
      if (type) targetItem.typename = type.name;
      httpResponseHandler.responseToClient(res, 200, targetItem);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  async changOptions(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const itemId = parseInt(req.params.itemId);
    const client = await DatabaseCommand.newClient();
    const bodyOptions = req.body as {
      id: number,
      price: number,
      note: string
      ingredients: {
        id: number;
        ingredientquantity: number;
      }[]
    }[];
    try {
      await DatabaseCommand.beginTransaction(client);
      const thisItem = (await ItemServices.getItems([{
        id: itemId
      }], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client))[0];
      if (!thisItem) return httpResponseHandler.responseErrorToClient(res, 404, `Item has id ${itemId} not found.`);
      const oldItemOptions = await ItemOptionServices.getItemOptions([{
        itemid: itemId
      }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
      for (const oldItemOption of oldItemOptions) {
        const foundIndex = bodyOptions.findIndex(option => option.id === oldItemOption.optionid);
        if (foundIndex > -1) {
          await ItemOptionServices.updateItemOptions({
            price: bodyOptions[foundIndex].price, note: bodyOptions[foundIndex].note
          }, [{
            itemid: oldItemOption.itemid, optionid: oldItemOption.optionid
          }], thisUser.companycode, client);
          const oldItemOptionIngredients = await ItemOptionIngredientServices.getItemOptionIngredients([{
            itemid: oldItemOption.itemid, optionid: oldItemOption.optionid
          }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
          const bodyIngredients = bodyOptions[foundIndex].ingredients;
          for (const oldItemOptionIngredient of oldItemOptionIngredients) {
            const foundIndex = bodyIngredients.findIndex(bodyIngredient => bodyIngredient.id === oldItemOptionIngredient.ingredientid);
            if (foundIndex > -1) {
              await ItemOptionIngredientServices.updateItemOptionIngredients({
                ingredientquantity: bodyIngredients[foundIndex].ingredientquantity
              }, [{
                itemid: oldItemOptionIngredient.itemid, optionid: oldItemOptionIngredient.optionid, ingredientid: oldItemOptionIngredient.ingredientid
              }], thisUser.companycode, client);
              bodyIngredients.splice(foundIndex, 1);
            } else {
              await ItemOptionIngredientServices.deleteItemOptionIngredients([{
                itemid: oldItemOptionIngredient.itemid, optionid: oldItemOptionIngredient.optionid, ingredientid: oldItemOptionIngredient.ingredientid
              }], thisUser.companycode, client);
            }
          }
          await ItemOptionIngredientServices.createItemOptionIngredients(bodyIngredients.map(ingredient => ({
            itemid: itemId, optionid: oldItemOption.optionid, ingredientid: ingredient.id, ingredientquantity: ingredient.ingredientquantity
          })), thisUser.companycode, client);
          bodyOptions.splice(foundIndex, 1);
        } else {
          await ItemOptionIngredientServices.deleteItemOptionIngredients([{
            itemid: itemId, optionid: oldItemOption.optionid
          }], thisUser.companycode, client);
          await ItemOptionServices.deleteItemOptions([{
            itemid: itemId, optionid: oldItemOption.optionid
          }], thisUser.companycode, client);
        }
      }
      await Promise.all(bodyOptions.map(async bodyOption => {
        await ItemOptionServices.createItemOption({
          itemid: thisItem.id, optionid: bodyOption.id, note: bodyOption.note, price: bodyOption.price
        }, thisUser.companycode, client);
        await ItemOptionIngredientServices.createItemOptionIngredients(bodyOption.ingredients.map(ingredient => ({
          itemid: thisItem.id, optionid: bodyOption.id, ingredientid: ingredient.id, ingredientquantity: ingredient.ingredientquantity
        })), thisUser.companycode, client);
      }));
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500, e.message);
    } finally {
      client.release();
    }
  }
}
