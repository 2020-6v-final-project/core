import {Application, Router} from "express";
import {passportJwt} from "../../../config/passport";
import Routes from "../../../constant/Routes";
import checkUser from "../../../cors/checkUser";
import ItemController from "./ItemController";
import ItemOptionRoutes from "./itemOptionsRoutes/ItemOptionRoutes";

const router: Router = Router();

export default (app: Application) => {
  router.route('/')
    .get(passportJwt, checkUser, ItemController.getItems)
    .post(passportJwt, checkUser, ItemController.createItem);

  router.route("/deactivate/:itemId").post(passportJwt, checkUser, ItemController.deactivateItem, ItemController.getItemById);
  router.route("/reactivate/:itemId").post(passportJwt, checkUser, ItemController.reactivateItem, ItemController.getItemById);

  router.route('/:itemId')
    .get(passportJwt, checkUser, ItemController.getItemById)
    .put(passportJwt, checkUser, ItemController.updateItem, ItemController.getItemById);

  ItemOptionRoutes(router);

  // router.route('/:itemId/options')
  //   .post(passportJwt, checkUser, ItemController.changOptions, ItemController.getItemById)

  app.use(Routes.ADMIN_ITEMS, router);
}
