import {NextFunction, Request, Response} from "express";
import httpResponseHandler from "../../../cors/httpResponseHandler";
import {IMenu} from "../../../models/IMenu";
import MenuItemServices from "../../../services/MenuItemServices";
import MenuServices from "../../../services/MenuServices";
import StatusServices from "../../../services/public/StatusServices";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import {IRequestUser} from "../../../models/temp/IRequestUser";
import ItemServices from "../../../services/ItemServices";
import { IMenuItem } from "../../../models/IMenuItem";
import {IItem} from "../../../models/IItem";
import MenuItemOptionServices from "../../../services/MenuItemOptionServices";
import {IMenuItemOption} from "../../../models/IMenuItemOption";
import ItemOptionServices from "../../../services/ItemOptionServices";
import RequestServies from "../../../services/RequestServies";
import BranchServices from "../../../services/BranchServices";

export default {
  createMenu: async (req: Request, res: Response) => {
    const bodyMenu = req.body;
    const itemList = bodyMenu.itemList as IItem[];
    const thisUser = req.user as IRequestUser;
    delete bodyMenu.id;
    delete bodyMenu.statusid;
    delete bodyMenu.statuscode;
    delete bodyMenu.itemList;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      bodyMenu.createdby = thisUser.id;
      const newMenu = await MenuServices.createMenu(bodyMenu, thisUser.companycode, client);
      const items = await Promise.all(itemList.map(async item => {
        const confirmItem = (await ItemServices.getItems([{id: item.id}], thisUser.companycode, DatabaseCommand.lockStrength.SHARE, client))[0];
        if (!confirmItem) return httpResponseHandler.responseErrorToClient(res, 404, `Item (id: ${item.id}) not found`);
        const itemOptionList = await ItemOptionServices.getItemOptions([{ itemid: confirmItem.id }], thisUser.companycode, DatabaseCommand.lockStrength.SHARE, client);
        const menuItemOptionList = itemOptionList.map(itemOption => {
          return {menuid: newMenu.id, itemid: itemOption.itemid, optionid: itemOption.optionid, price: itemOption.price};    
        })
        await MenuItemOptionServices.createMenuItemOptions(menuItemOptionList, thisUser.companycode, client);
        const menuItem: IMenuItem = {menuid: newMenu.id, itemid: confirmItem.id, price: confirmItem.price};
        return await MenuItemServices.addOne(menuItem, thisUser.companycode, client);
      }));
      const result = newMenu as any;
      result.itemList = items;
      delete result.predata;
      await DatabaseCommand.commitTransaction(client);
      httpResponseHandler.responseToClient(res, 201, result);
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  updateMenu: async (req: Request, res: Response, next: NextFunction) => {
    const menuId = parseInt(req.params.menuid);
    const bodyMenu = req.body;
    const itemList = bodyMenu.itemList as IItem[];
    const thisUser = req.user as IRequestUser;
    delete bodyMenu.id;
    delete bodyMenu.itemList;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let listMenu = await MenuServices.getMenus([{id: menuId}], thisUser.companycode, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      let targetMenu = listMenu[0] as any;
      if (!targetMenu) return httpResponseHandler.responseErrorToClient(res, 404, `Menu (id: ${menuId}) not found!`);
      Object.assign(targetMenu, bodyMenu);
      const updatedMenu = await MenuServices.updateMenu(targetMenu, thisUser.companycode, client);
      // Compare ingredientList diff
      const oldItemList = await MenuItemServices.getAllByMenuId(menuId, thisUser.companycode, client, DatabaseCommand.lockStrength.UPDATE);
      if (oldItemList.length > 0) {
        for (const oldItem of oldItemList) {
          const containIndex = itemList?.findIndex(item => item.id === oldItem.itemid);
          if (containIndex > -1) {
            itemList.splice(containIndex, 1);
          } else await MenuItemServices.removeOne(oldItem, thisUser.companycode, client);
        }
      }
      await Promise.all(itemList.map(async item => {
        const confirmItem = (await ItemServices.getItems([{id: item.id}], thisUser.companycode, DatabaseCommand.lockStrength.SHARE, client))[0];
        if (!confirmItem) return httpResponseHandler.responseErrorToClient(res, 404, `Item (id: ${item.id}) not found`);
        const menuItem: IMenuItem = {menuid: menuId, itemid: confirmItem.id, price: confirmItem.price};
        await MenuItemServices.addOne(menuItem, thisUser.companycode, client);
      }));
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  deactivateMenu: async (req: Request, res: Response, next: NextFunction) => {
    const menuId = parseInt(req.params.menuid);
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const listMenu = await MenuServices.getMenus([{id: menuId}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
      let targetMenu = listMenu[0];
      if (!targetMenu) return httpResponseHandler.responseErrorToClient(res, 404, `Menu (id: ${menuId}) not found!`);
      await StatusServices.setStatusCodeToObjects(MenuServices.IMenuTableName, "DISABLED", [{id: menuId}], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  reactivateMenu: async (req: Request, res: Response, next: NextFunction) => {
    const menuId = parseInt(req.params.menuid);
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const listMenu = await MenuServices.getMenus([{id: menuId}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
      let targetMenu = listMenu[0];
      if (!targetMenu) return httpResponseHandler.responseErrorToClient(res, 404, `Menu (id: ${menuId}) not found!`);
      await StatusServices.setStatusCodeToObjects(MenuServices.IMenuTableName, "AVAILABLE", [{id: menuId}], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  getMenus: async (req: Request, res: Response) => {
    const thisUser = req.user as IRequestUser;
    try {
      let menus = await MenuServices.getMenus([], thisUser.companycode, undefined, undefined);
      menus = await Promise.all(menus.map(async (menu: IMenu) => {
        const tempMenu = menu as IMenu & {
          itemList: (IMenuItem & {
            optionList: IMenuItemOption[]
          })[]
        };
        tempMenu.itemList = await Promise.all((await MenuItemServices.getAllByMenuId(menu.id, thisUser.companycode)).map(async item => {
          const tempItem = item as IMenuItem & {
            optionList: IMenuItemOption[]
          };
          tempItem.optionList = await MenuItemOptionServices.getMenuItemOptions([{
            menuid: tempMenu.id,
            itemid: tempItem.itemid,
          }], thisUser.companycode);
          return tempItem;
        }));
        return tempMenu;
      }));
      httpResponseHandler.responseToClient(res, 200, menus);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  getMenuById: async (req: Request, res: Response) => {
    const menuId = parseInt(req.params.menuid);
    const thisUser = req.user as IRequestUser;
    try {
      const menuList = await MenuServices.getMenus([{id: menuId}], thisUser.companycode);
      if (!menuList.length) return httpResponseHandler.responseErrorToClient(res, 404, `Menu (id: ${menuId}) not found!`);
      const tempMenu = menuList[0] as IMenu & {
        itemList: (IMenuItem & {
          optionList: IMenuItemOption[]
        })[]
      };
      tempMenu.itemList = await Promise.all((await MenuItemServices.getAllByMenuId(tempMenu.id, thisUser.companycode)).map(async item => {
        const tempItem = item as IMenuItem & {
          optionList: IMenuItemOption[]
        };
        tempItem.optionList = await MenuItemOptionServices.getMenuItemOptions([{
          menuid: tempMenu.id,
          itemid: tempItem.itemid,
        }], thisUser.companycode);
        return tempItem;
      }));
      httpResponseHandler.responseToClient(res, 200, tempMenu);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  removeMenu: async (req: Request, res: Response, next: NextFunction) => {
    const menuId = parseInt(req.params.menuid);
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const menuList = await MenuServices.getMenus([{id: menuId}], thisUser.companycode, undefined, client);
      let targetMenu = menuList[0] as IMenu;
      if (!targetMenu) return httpResponseHandler.responseErrorToClient(res, 404, `Menu (id: ${menuId}) not found!`);
      const itemList = await MenuItemServices.getAllByMenuId(menuId, thisUser.companycode, client, undefined);
      for (let a = 0; a < itemList.length; a++) {
        await MenuItemServices.removeOne(itemList[a], thisUser.companycode, client);
      }
      await MenuServices.removeOne(targetMenu, thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseToClient(res, 403, err.message);
    } finally {
      client.release();
    }
  },
  async changePrice(req: Request, res: Response, next: NextFunction) {
    const menuId = parseInt(req.params.menuid);
    const thisUser = req.user as IRequestUser;
    const body = req.body.menu as { 
      id: number,
      price: number,
      options: {
        id: number,
        price: number,
      }[]
    }[];
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const menus = await MenuServices.getMenus([{ id: menuId }], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client);
      const menu = menus[0] as IMenu;
      if (!menu) return httpResponseHandler.responseToClient(res, 404, `Menu (id: ${menuId}) not found!`);
      // Generate new menu
      let newMenuData = menu as any;
      delete newMenuData.id;
      newMenuData.name = menu.name + " (" + (await BranchServices.getBranches([{ id: req.body.branchId }], thisUser.companycode, DatabaseCommand.lockStrength.SHARE, client))[0]?.code.toUpperCase() + ")"; 
      delete newMenuData.statusid;
      delete newMenuData.statuscode;
      delete newMenuData.createdat;
      newMenuData.createdby = thisUser.id;
      delete newMenuData.approvedrequestid;
      newMenuData.basemenu = menuId;
      const newMenu = await MenuServices.createMenu(newMenuData, thisUser.companycode, client);
      body.forEach(async item => {
        await MenuItemServices.addOne({ menuid: newMenu.id, itemid: item.id, price: item.price }, thisUser.companycode, client);
        const menuItemOptionList = item.options?.map(option => {
          return { menuid: newMenu.id, itemid: item.id, option: option.id, price: option.price }
        })
        await MenuItemOptionServices.createMenuItemOptions(menuItemOptionList, thisUser.companycode, client);
      })
      // Request for menu
      const newRequest = {
        requesttypeid: 1,
        createdby: thisUser.id,
        referenceid: newMenu.id,
        note: req.body.note,
      }
      await RequestServies.createRequest(newRequest, thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseToClient(res, 403, err.message);
    } finally {
      client.release();
    }
  }
}
