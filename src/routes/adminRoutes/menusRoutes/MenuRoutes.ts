import {Application, Router} from "express";
import {passportJwt} from "../../../config/passport";
import Routes from "../../../constant/Routes";
import checkUser from "../../../cors/checkUser";
import menuController from "./MenuController";

const router: Router = Router();

export default (app: Application) => {
	router.route('/')
		.get(passportJwt, checkUser, menuController.getMenus)
		.post(passportJwt, checkUser, menuController.createMenu);
		
	router.route("/deactivate/:menuid")
    	.post(passportJwt, checkUser, menuController.deactivateMenu, menuController.getMenuById);

	router.route("/reactivate/:menuid")
    	.post(passportJwt, checkUser, menuController.reactivateMenu, menuController.getMenuById);

	router.route('/:menuid')
		.get(passportJwt, checkUser, menuController.getMenuById)
		.put(passportJwt, checkUser, menuController.updateMenu, menuController.getMenuById)
		.delete(passportJwt, checkUser, menuController.removeMenu, menuController.getMenus);

	router.route('/:menuid/requestchangeprice')
		.post(passportJwt, checkUser, menuController.changePrice, menuController.getMenus);

	app.use(Routes.ADMIN_MENUS, router);
}
