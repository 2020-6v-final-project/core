import {Application, Router} from "express";
import {passportJwt} from "../../../config/passport";
import Routes from "../../../constant/Routes";
import checkUser from "../../../cors/checkUser";
import repositoryController from "./RepositoryController";

const router: Router = Router();

export default (app: Application) => {
  //routes for repository
  router.route('/')
    .get(passportJwt, checkUser, repositoryController.getRepositories)
    .post(passportJwt, checkUser, repositoryController.createRepository,repositoryController.getRepositories);

  router.route("/deactivate/:repositoryid").post(passportJwt, checkUser, repositoryController.deactivateRepository,repositoryController.getRepositories);
  router.route("/reactivate/:repositoryid").post(passportJwt, checkUser, repositoryController.reactiveRepository,repositoryController.getRepositories);

  //routes for ingredients of repository
  router.route('/ingredients/')
  .get(passportJwt, checkUser, repositoryController.getRepositorieIngredients);

  router.route('/ingredients/:repositoryid')
  .post(passportJwt, checkUser, repositoryController.addIngredientToRepository,repositoryController.getRepositorieIngredients)
  .put(passportJwt, checkUser, repositoryController.updateRepositoryIngredient,repositoryController.getRepositorieIngredients)
  .delete(passportJwt, checkUser, repositoryController.removeRepositoryIngredientById,repositoryController.getRepositorieIngredients);

  router.route("/ingredients/deactivate/:repositoryid")
    .post(passportJwt, checkUser, repositoryController.deactiveRepositoryIngredient,repositoryController.getRepositorieIngredients);

  router.route("/ingredients/reactivate/:repositoryid")
    .post(passportJwt, checkUser, repositoryController.reactiveRepositoryIngredient,repositoryController.getRepositorieIngredients);


  router.route('/:repositoryid')
    .get(passportJwt, checkUser, repositoryController.getRepositoryById)
    .put(passportJwt, checkUser, repositoryController.updateRepository);

  

  app.use(Routes.ADMIN_REPOSITORIES, router);
}
