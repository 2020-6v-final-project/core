import  { NextFunction, Request, Response } from "express";
import DbSingleton from "../../../config/database";
import httpResponseHandler from "../../../cors/httpResponseHandler";
import { IRepository } from "../../../models/IRepository";
import RepositoryServices from "../../../services/RepositoryServices";
import BranchServices from "../../../services/BranchServices";
import StatusServices from "../../../services/public/StatusServices";
import RepositoryIngredientServices from "../../../services/RepositoryIngredientServices";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import { IUser } from "../../../models/public/IUser";
import { IBranch } from "../../../models/IBranch";
import {IRequestUser} from "../../../models/temp/IRequestUser";
import { IRepositoryIngredient } from "../../../models/IRepositoryIngredient";

const db = DbSingleton.getInstance();
export default {
  //repository controller
  createRepository: async (req: Request, res: Response,next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    try {
    await DatabaseCommand.beginTransaction(client);
    const bodyRepository = req.body as IRepository;
    const thisUser = req.user as IRequestUser;
    console.log(thisUser);
    const newRepository = await RepositoryServices.createRepository(bodyRepository, thisUser.companycode, client);
    await DatabaseCommand.commitTransaction(client);
    next();
    } catch (err) {
    console.log(err);
    await DatabaseCommand.rollbackTransaction(client);
    httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
    client.release();
    }
  },

  updateRepository: async(req: Request, res: Response,next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    const bodyRepository= req.body as IRepository;
    const thisUser = req.user as IRequestUser;
    try {
    await DatabaseCommand.beginTransaction(client);
    let listRepository = await RepositoryServices.getRepositories([{ id: bodyRepository.id }], thisUser.companycode, undefined,client);
    let targetRepository = listRepository[0];
    if (!targetRepository) return httpResponseHandler.responseErrorToClient(res, 404, "Target repository not found!");
    if (req.body.name) targetRepository.name = req.body.name;

    const updatedRepository = await RepositoryServices.updateRepository(targetRepository, thisUser.companycode, client);
    await DatabaseCommand.commitTransaction(client);
    next();
  } catch (err) {
    console.log(err);
    await DatabaseCommand.rollbackTransaction(client);
    httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
    client.release();
    }
  },

  deactivateRepository: async(req: Request, res: Response,next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    const thisUser = req.user as IRequestUser;
    try {
      await DatabaseCommand.beginTransaction(client);
      const listRepository = await RepositoryServices.getRepositories([{ id: req.params.repositoryid }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE,client);
      let targetRepository = listRepository[0];
      if (!targetRepository) return httpResponseHandler.responseErrorToClient(res, 404, "Target Repository not found!");
      // let setStatusResult = await StatusServices.setStatus(targetRepository, RepositoryServices.IRepositoryTableName, "DISABLED");
      let setStatusResult = await StatusServices.setStatusCodeToObjects(RepositoryServices.IRepositoryTableName, "DISABLED",[{id:targetRepository.id}],thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },

  reactiveRepository: async(req: Request, res: Response,next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    const thisUser = req.user as IRequestUser;
    try {
      await DatabaseCommand.beginTransaction(client);
      const listRepository = await RepositoryServices.getRepositories([{ id: req.params.repositoryid }], thisUser.companycode.toLowerCase(), DatabaseCommand.lockStrength.UPDATE, client);
      let targetRepository = listRepository[0];
      if (!targetRepository) return httpResponseHandler.responseErrorToClient(res, 404, "Target Repository not found!");
      // let setStatusResult = await StatusServices.setStatus(targetRepository, RepositoryServices.IRepositoryTableName, "AVAILABLE");
      let setStatusResult = await StatusServices.setStatusCodeToObjects(RepositoryServices.IRepositoryTableName, "AVAILABLE",[{id:targetRepository.id}],thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },

  getRepositories: async(req: Request, res: Response) => {
    const thisUser = req.user as IRequestUser;
    try {
      //find the companyid or code of logged in account -> User
      const repositories = await RepositoryServices.getRepositories([], thisUser.companycode, undefined, undefined);
      httpResponseHandler.responseToClient(res, 200, repositories);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },

  getRepositoryById: async(req: Request, res: Response) => {
    const thisUser = req.user as IRequestUser;
    try {
      const repositoryList = await RepositoryServices.getRepositories([{ id: parseInt(req.params.repositoryid) }], thisUser.companycode, undefined, undefined);
      let targetRepository = repositoryList[0];
      if (!targetRepository) return httpResponseHandler.responseErrorToClient(res, 404, "Target IRepository not found!");
      //attach information of Manager or not ?
      httpResponseHandler.responseToClient(res, 200, targetRepository);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },


  //ingredients of repository controller


  addIngredientToRepository: async (req: Request, res: Response,next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    try {
    await DatabaseCommand.beginTransaction(client);
    const bodyRepository = req.body as IRepository;
    const thisUser = req.user as IRequestUser;

    const newRepository = await RepositoryServices.createRepository(bodyRepository, thisUser.companycode, client);
    await DatabaseCommand.commitTransaction(client);
    next();
    } catch (err) {
    console.log(err);
    await DatabaseCommand.rollbackTransaction(client);
    httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
    client.release();
    }
  },

  updateRepositoryIngredient: async(req: Request, res: Response,next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    const bodyRepository= req.body as IRepository;
    const thisUser = req.user as IRequestUser;
    try {
    await DatabaseCommand.beginTransaction(client);
    let listRepository = await RepositoryServices.getRepositories([{ id: bodyRepository.id }], thisUser.companycode.toLowerCase(), undefined,client);
    let targetRepository = listRepository[0];
    if (!targetRepository) return httpResponseHandler.responseErrorToClient(res, 404, "Target repository not found!");
    if (req.body.name) targetRepository.name = req.body.name;

    const updatedRepository = await RepositoryServices.updateRepository(targetRepository, thisUser.companycode.toLowerCase(), client);
    await DatabaseCommand.commitTransaction(client);
    next();
  } catch (err) {
    console.log(err);
    await DatabaseCommand.rollbackTransaction(client);
    httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
    client.release();
    }
  },
  
  reactiveRepositoryIngredient: async(req: Request, res: Response,next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    const thisUser = req.user as IRequestUser;
    try {
      await DatabaseCommand.beginTransaction(client);
      const listRepository = await RepositoryServices.getRepositories([{ id: req.params.repositoryid }], thisUser.companycode.toLowerCase(), DatabaseCommand.lockStrength.UPDATE, client);
      let targetRepository = listRepository[0];
      if (!targetRepository) return httpResponseHandler.responseErrorToClient(res, 404, "Target Repository not found!");
      // let setStatusResult = await StatusServices.setStatus(targetRepository, RepositoryServices.IRepositoryTableName, "AVAILABLE");
      let setStatusResult = await StatusServices.setStatusCodeToObjects(RepositoryServices.IRepositoryTableName, "AVAILABLE",[targetRepository],thisUser.companycode.toLowerCase(), client);

      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },

  deactiveRepositoryIngredient: async(req: Request, res: Response,next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    const thisUser = req.user as IRequestUser;
    try {
      await DatabaseCommand.beginTransaction(client);
      const listRepository = await RepositoryServices.getRepositories([{ id: req.params.repositoryid }], thisUser.companycode.toLowerCase(), DatabaseCommand.lockStrength.UPDATE, client);
      let targetRepository = listRepository[0];
      if (!targetRepository) return httpResponseHandler.responseErrorToClient(res, 404, "Target Repository not found!");
      // let setStatusResult = await StatusServices.setStatus(targetRepository, RepositoryServices.IRepositoryTableName, "AVAILABLE");
      let setStatusResult = await StatusServices.setStatusCodeToObjects(RepositoryServices.IRepositoryTableName, "DISABLED",[targetRepository],thisUser.companycode.toLowerCase(), client);

      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },

  getRepositorieIngredients: async(req: Request, res: Response) => {
    const thisUser = req.user as IRequestUser;
    const userBranches = thisUser.branchIds ;
    try {
      let repoList = [] as any[];
      for(let a=0;a<userBranches.length;a++)
      {
          const branch = await BranchServices.getBranches([{id:userBranches[a]}], thisUser.companycode, undefined, undefined) as IBranch[];
          const repo = await RepositoryServices.getRepositories([{id:branch[0].repositoryid}],thisUser.companycode, undefined, undefined) as IRepository[];
          repoList = repoList.concat(repo);
      }
      let repoIngredientsList = [] as any[];
      for(let a=0;a<repoList.length;a++)
      {
          const repoIngredient = await RepositoryIngredientServices.getAll([{repositoryid:repoList[a].id}], thisUser.companycode, undefined, undefined) as IRepositoryIngredient[];
          repoIngredientsList = repoIngredientsList.concat(repoIngredient);
      }
      httpResponseHandler.responseToClient(res, 200, repoIngredientsList);
    } catch (error) {
        console.log(error);
        httpResponseHandler.responseErrorToClient(res, 400, error.message);
    }
  },

  removeRepositoryIngredientById: async (req: Request, res: Response,next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    const bodyRepoIngre = req.body as any;
    try {
        await DatabaseCommand.beginTransaction(client);
        const thisUser = req.user as IRequestUser;
        await RepositoryIngredientServices.removeOne(bodyRepoIngre, thisUser.companycode, client);
        await DatabaseCommand.commitTransaction(client);
        next();
    } catch (error) {
        console.log(error);
        await DatabaseCommand.rollbackTransaction(client);
        httpResponseHandler.responseErrorToClient(res, 400, error.message);
    } finally {
        client.release();
    }
},
};
