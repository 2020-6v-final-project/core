import {Request, Response} from 'express';
import httpResponseHandler from "../../../cors/httpResponseHandler";
import PaymentTypeServices from "../../../services/public/PaymentTypeServices";

export default {

  async getPaymentTypes(req: Request, res: Response) {
    try {
      const paymentTypes = await PaymentTypeServices.getPaymentTypes([]);
      return httpResponseHandler.responseToClient(res, 200, paymentTypes);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  }
}
