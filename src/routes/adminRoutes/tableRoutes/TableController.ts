import { NextFunction,Request, Response } from "express";
import httpResponseHandler from '../../../cors/httpResponseHandler';
import DatabaseCommand from "../../../utils/DatabaseCommand";
import { ITable } from "../../../models/ITable";
import TableServices from "../../../services/TableServices"
import {IRequestUser} from "../../../models/temp/IRequestUser";
import StatusServices from "../../../services/public/StatusServices";
export default {
    createTable: async function (req: Request, res: Response, next: NextFunction) {
        const thisUser = req.user as IRequestUser;
        if (!thisUser.branchIds.includes(req.body.branchid)) return httpResponseHandler.responseErrorToClient(res, 403, `You do not have permission on this branch.`);
        const client = await DatabaseCommand.newClient();
        try {
            await DatabaseCommand.beginTransaction(client);
            const cntTable: number = req.body.countTable;
            for (let i: number = 0; i < cntTable; i++) {
                const table = {} as ITable;
                // TODO: should be use serial type for id
                table.branchid = req.body.branchid;
                table.floor = req.body.floor;
                // TODO: should be use default value in db
                table.slots = req.body.slots;
                const newTable = await TableServices.createTable(table, thisUser.companycode, client);
                newTable.name = "Bàn " + newTable.id;
                await TableServices.updateTable(newTable, thisUser.companycode, client);
            }
            await DatabaseCommand.commitTransaction(client);
            next();
        } catch (error) {
            console.log(error);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 400, error.message);
        } finally {
            client.release();
        }
    },
    getAll: async function (req: Request, res: Response) {
        try {
            const thisUser = req.user as IRequestUser;
            const userBranches = thisUser.branchIds as number[];
            let tableList = [] as any[];
            for(let a=0;a<userBranches.length;a++)
            {
                const branchTables = await TableServices.getTables([{branchid:userBranches[a]}], thisUser.companycode, undefined, undefined) as ITable[];
                tableList = tableList.concat(branchTables);
            }
            httpResponseHandler.responseToClient(res, 200, tableList);
        } catch (error) {
            console.log(error);
            httpResponseHandler.responseErrorToClient(res, 400, error.message);
        }
    },
    getTableById: async function (req: Request, res: Response) {
        try {
            const thisUser = req.user as IRequestUser;
            const bodyTable = req.body as ITable;
            const table = await TableServices.getTableById(bodyTable, thisUser.companycode, undefined, undefined);
            httpResponseHandler.responseToClient(res, 200, table);
        } catch (error) {
            console.log(error);
            httpResponseHandler.responseErrorToClient(res, 400, error.message);
        }
    },
    updateTableById: async function (req: Request, res: Response) {
        const client = await DatabaseCommand.newClient();
        try {
            await DatabaseCommand.beginTransaction(client);
            const thisUser = req.user as IRequestUser;
            const updateTable = {} as ITable;
            updateTable.id = parseInt(req.params.tableId);
            if (req.body.branchid) updateTable.branchid = req.body.branchid;
            if (req.body.slots) updateTable.slots = req.body.slots;
            if (req.body.statusid) updateTable.statusid = req.body.statusid;
            if (req.body.statuscode) updateTable.statuscode = req.body.statuscode;
            if (req.body.floor) updateTable.floor = req.body.floor;
            const tabelAfterUpdate = await TableServices.updateTable(updateTable, thisUser.companycode, client);
            await DatabaseCommand.commitTransaction(client);
            httpResponseHandler.responseToClient(res, 200, tabelAfterUpdate);
        } catch (error) {
            console.log(error);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 400, error.message);
        } finally {
            client.release();
        }
    },
    removeTableById: async function (req: Request, res: Response,next: NextFunction) {
        const client = await DatabaseCommand.newClient();
        const bodyTable = req.body as ITable;
        try {
            await DatabaseCommand.beginTransaction(client);
            const thisUser = req.user as IRequestUser;
            await TableServices.removeTableById(bodyTable, thisUser.companycode, client);
            await DatabaseCommand.commitTransaction(client);
            next();
        } catch (error) {
            console.log(error);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 400, error.message);
        } finally {
            client.release();
        }
    },
    deactivateTable: async (req: Request, res: Response, next: NextFunction) => {
        const bodyTable = req.body as ITable;
        const thisUser = req.user as IRequestUser;
        const client = await DatabaseCommand.newClient();
        try {
          await DatabaseCommand.beginTransaction(client);
          const listTable = await TableServices.getTables([bodyTable], thisUser.companycode, client,DatabaseCommand.lockStrength.UPDATE);
          let targetTable = listTable[0];
          if (!targetTable) return httpResponseHandler.responseErrorToClient(res, 404, `Table (id: ${bodyTable.id}) not found!`);
          await StatusServices.setStatusCodeToObjects(TableServices.ITableTableName, "DISABLED", [{id: bodyTable.id,branchid:bodyTable.branchid}], thisUser.companycode, client);
          await DatabaseCommand.commitTransaction(client);
          next();
        } catch (err) {
          console.log(err);
          await DatabaseCommand.rollbackTransaction(client);
          httpResponseHandler.responseErrorToClient(res, 500, err.message);
        } finally {
          client.release();
        }
    },
    reactivateTable: async (req: Request, res: Response, next: NextFunction) => {
    const bodyTable = req.body as ITable;
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    try {
        await DatabaseCommand.beginTransaction(client);
        const listTable = await TableServices.getTables([bodyTable], thisUser.companycode, client,DatabaseCommand.lockStrength.UPDATE);
        let targetTable = listTable[0];
        if (!targetTable) return httpResponseHandler.responseErrorToClient(res, 404, `Table (id: ${bodyTable.id}) not found!`);
        await StatusServices.setStatusCodeToObjects(TableServices.ITableTableName, "AVAILABLE", [{id: bodyTable.id,branchid:bodyTable.branchid}], thisUser.companycode, client);
        await DatabaseCommand.commitTransaction(client);
        next();
    } catch (err) {
        console.log(err);
        await DatabaseCommand.rollbackTransaction(client);
        httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
        client.release();
    }
    },
}
