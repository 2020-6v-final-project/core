import { Router, Application } from 'express';
import Routes from "../../../constant/Routes";
import TableController from "./TableController";
import checkUser from "../../../cors/checkUser";
import { passportJwt } from "../../../config/passport";

const router: Router = Router();

export default function (app: Application) {
    router.route('/')
        .get(passportJwt, checkUser, TableController.getAll)
        .post(passportJwt, checkUser, TableController.createTable,TableController.getAll);
    router.route('/:tableId')
        .get(passportJwt, checkUser, TableController.getTableById)
        .put(passportJwt, checkUser, TableController.updateTableById)
        .delete(passportJwt, checkUser, TableController.removeTableById,TableController.getAll);
    router.route("/deactivate/:tableId").post(passportJwt, checkUser, TableController.deactivateTable, TableController.getAll);
    router.route("/reactivate/:tableId").post(passportJwt, checkUser, TableController.reactivateTable,  TableController.getAll);

    app.use(Routes.ADMIN_TABLES, router);
}
