import { Router, Application } from 'express';
import Routes from "../../../constant/Routes";
import ImageController from "./ImageController";
import checkUser from "../../../cors/checkUser";
import { passportJwt } from "../../../config/passport";

const router: Router = Router();
export default async function (app: Application) {
    router.route('/')
        .get(passportJwt, checkUser, ImageController.getSASToken)
    app.use(Routes.ADMIN_IMAGES, router);
}
