import { Request, Response } from "express";
import httpResponseHandler from '../../../cors/httpResponseHandler';
import DatabaseCommand from "../../../utils/DatabaseCommand";
import { IUser } from "../../../models/public/IUser";
import { generateSASToken } from "../../../utils/GenerateSASToken";

export default {
    getSASToken: async (req: Request, res: Response) => {
        try {
            let token = generateSASToken();
            httpResponseHandler.responseToClient(res, 200, token);
        } catch (err) {
            console.log(err);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        }
    }
}