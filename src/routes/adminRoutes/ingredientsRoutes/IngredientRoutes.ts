import {Application, Router} from "express";
import {passportJwt} from "../../../config/passport";
import Routes from "../../../constant/Routes";
import checkUser from "../../../cors/checkUser";
import ingredientController from "./IngredientController";

const router: Router = Router();

export default (app: Application) => {
  router.route('/')
    .get(passportJwt, checkUser, ingredientController.getIngredients)
    .post(passportJwt, checkUser, ingredientController.createIngredient);

  router.route("/deactivate/:ingredientId").post(passportJwt, checkUser, ingredientController.deactivateIngredient, ingredientController.getIngredientById);
  router.route("/reactivate/:ingredientId").post(passportJwt, checkUser, ingredientController.reactivateIngredient, ingredientController.getIngredientById);

  router.route('/:ingredientId')
    .get(passportJwt, checkUser, ingredientController.getIngredientById)
    .put(passportJwt, checkUser, ingredientController.updateIngredient, ingredientController.getIngredientById);

  app.use(Routes.ADMIN_INGREDIENTS, router);
}
