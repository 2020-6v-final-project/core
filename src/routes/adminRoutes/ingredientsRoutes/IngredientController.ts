import {NextFunction, Request, Response} from "express";
import DbSingleton from "../../../config/database";
import httpResponseHandler from "../../../cors/httpResponseHandler";
import {IIngredient} from "../../../models/IIngredient";
import IngredientServices from "../../../services/IngredientServices";
import StatusServices from "../../../services/public/StatusServices";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import {IUser} from "../../../models/public/IUser";
import ItemIngredientServices from "../../../services/ItemIngredientServices";
import ItemServices from "../../../services/ItemServices";
import {IRequestUser} from "../../../models/temp/IRequestUser";

const db = DbSingleton.getInstance();
export default {
  createIngredient: async (req: Request, res: Response) => {
    const bodyIngredient = req.body;
    const thisUser = req.user as IRequestUser;
    delete bodyIngredient.id;
    delete bodyIngredient.statusid;
    delete bodyIngredient.statuscode;
    const {withitems} = req.query;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let newIngredient = await IngredientServices.createIngredient(bodyIngredient, thisUser.companycode, client) as any;
      if (typeof withitems !== 'undefined') {
        newIngredient.items = await ItemIngredientServices.getAllByIngredientId(newIngredient.id, thisUser.companycode, client, DatabaseCommand.lockStrength.SHARE);
      }
      await DatabaseCommand.commitTransaction(client);
      httpResponseHandler.responseToClient(res, 201, newIngredient);
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  updateIngredient: async (req: Request, res: Response, next: NextFunction) => {
    const ingredientId = parseInt(req.params.ingredientId);
    const thisUser = req.user as IRequestUser;
    const bodyIngredient = req.body;
    delete bodyIngredient.id;
    delete bodyIngredient.statusid;
    delete bodyIngredient.statuscode;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let listIngredient = await IngredientServices.getIngredients([{id: ingredientId}], thisUser.companycode, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      let targetIngredient = listIngredient[0];
      if (!targetIngredient) return httpResponseHandler.responseErrorToClient(res, 404, `Ingredient (id: ${ingredientId}) not found!`);
      Object.assign(targetIngredient, bodyIngredient);
      const updatedIngredient = await IngredientServices.updateIngredient(targetIngredient, thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  deactivateIngredient: async (req: Request, res: Response, next: NextFunction) => {
    const ingredientId = parseInt(req.params.ingredientId);
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const listIngredient = await IngredientServices.getIngredients([{id: ingredientId}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
      let targetIngredient = listIngredient[0];
      if (!targetIngredient) return httpResponseHandler.responseErrorToClient(res, 404, `Ingredient (id: ${ingredientId}) not found!`);
      await StatusServices.setStatusCodeToObjects(IngredientServices.IIngredientTableName, "DISABLED", [{id: ingredientId}], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  reactivateIngredient: async (req: Request, res: Response, next: NextFunction) => {
    const ingredientId = parseInt(req.params.ingredientId);
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const listIngredient = await IngredientServices.getIngredients([{id: ingredientId}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
      let targetIngredient = listIngredient[0];
      if (!targetIngredient) return httpResponseHandler.responseErrorToClient(res, 404, `Ingredient (id: ${ingredientId}) not found!`);
      await StatusServices.setStatusCodeToObjects(IngredientServices.IIngredientTableName, "AVAILABLE", [{id: ingredientId}], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  getIngredients: async (req: Request, res: Response) => {
    const thisUser = req.user as IRequestUser;
    const {withitems} = req.query;
    try {
      let ingredients = await IngredientServices.getIngredients([], thisUser.companycode, undefined, undefined);
      if (typeof withitems !== 'undefined') {
        ingredients = await Promise.all(ingredients.map(async ingredient => {
          const tempIngredient = ingredient as any;
          tempIngredient.items = await Promise.all((await ItemIngredientServices.getAllByIngredientId(ingredient.id, thisUser.companycode)).map(async itemIngredient => {
            const item = (await ItemServices.getItems([{
              id: itemIngredient.itemid
            }], thisUser.companycode))[0];
            return Object.assign(itemIngredient, item);
          }));
          return tempIngredient;
        }));
      }
      httpResponseHandler.responseToClient(res, 200, ingredients);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  getIngredientById: async (req: Request, res: Response) => {
    const ingredientId = parseInt(req.params.ingredientId);
    const thisUser = req.user as IRequestUser;
    try {
      const ingredientList = await IngredientServices.getIngredients([{id: ingredientId}], thisUser.companycode, undefined, undefined);
      let targetIngredient = ingredientList[0] as any;
      if (!targetIngredient) return httpResponseHandler.responseErrorToClient(res, 404, `Ingredient (id: ${ingredientId}) not found!`);
      targetIngredient.items = await Promise.all((await ItemIngredientServices.getAllByIngredientId(targetIngredient.id, thisUser.companycode)).map(async itemIngredient => {
        const item = (await ItemServices.getItems([{
          id: itemIngredient.itemid
        }], thisUser.companycode))[0];
        return Object.assign(itemIngredient, item);
      }));
      httpResponseHandler.responseToClient(res, 200, targetIngredient);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
}
