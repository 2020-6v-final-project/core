import {Router, Application} from 'express';
import Routes from "../../../constant/Routes";
import AdminRoleController from "./RoleController";
import checkUser from "../../../cors/checkUser";
import {passportJwt} from "../../../config/passport";

const router: Router = Router();

export default function (app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, AdminRoleController.getRoles);

  router.route('/:roleId')
    .get(passportJwt, checkUser, AdminRoleController.getRole)
    .put(passportJwt, checkUser, AdminRoleController.updateRole, AdminRoleController.getRole);

  app.use(Routes.ADMIN_ROLES, router);
}
