import {NextFunction, Request, Response} from "express";
import httpResponseHandler from '../../../cors/httpResponseHandler';
import RoleServices from "../../../services/public/RoleServices";
import UserRoleServices from "../../../services/public/UserRoleServices";
import UserServices from "../../../services/public/UserServices";
import { IUserRole } from "../../../models/public/IUserRole";
import { IUser } from "../../../models/public/IUser";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import {IRequestUser} from "../../../models/temp/IRequestUser";

export default {
  getRoles: async function (req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    try {
      let roles = await RoleServices.getRoles([], thisUser.companyid);
      if (typeof (req.query.withusers) !== 'undefined') {
        roles = await Promise.all(roles.map(async role => {
          const thisRole = role as any;
          thisRole.hasusers = await UserServices.getUsersOfRole(thisRole, thisUser.companyid);
          thisRole.nothaveusers = (await UserServices.getUsers([{
            companyid: thisUser.companyid
          }])).filter(user => {
            return thisRole.hasusers.find((item: { id: number; }) => item.id === user.id) === undefined;
          });
          return thisRole;
        }))
      }
      return httpResponseHandler.responseToClient(res, 200, roles)
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  getRole: async function (req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    const roleId = parseInt(req.params.roleId);
    try {
      const roles = await RoleServices.getRoles([{
        id: roleId
      }], thisUser.companyid);
      if (!roles.length) return httpResponseHandler.responseErrorToClient(res, 404, `Role has id ${roleId} not found.`);
      const thisRole = roles[0] as any;
      thisRole.hasusers = await UserServices.getUsersOfRole(thisRole, thisUser.companyid);
      thisRole.nothaveusers = (await UserServices.getUsers([{
        companyid: thisUser.companyid
      }])).filter(user => {
        return thisRole.hasusers.find((item: { id: number; }) => item.id === user.id) === undefined;
      });
      return httpResponseHandler.responseToClient(res, 200, thisRole);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async updateRole(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const roleId = parseInt(req.params.roleId);
    const bodyRole = req.body;
    let {hasusers, nothaveusers} = bodyRole;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const roles = await RoleServices.getRoles([{id: roleId}], thisUser.companyid, DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!roles.length) return httpResponseHandler.responseErrorToClient(res, 404, `Role has id ${roleId} not found.`);
      const thisRole = roles[0];
      if (nothaveusers instanceof Array) {
        await Promise.all(nothaveusers.map(async user => {
          await UserRoleServices.removeUserRole({
            userid: user.id, companyid: user.companyid, roleid: thisRole.id
          }, client);
        }))
      }
      if (hasusers instanceof Array) {
        await Promise.all(hasusers.map(async user => {
          const userRoles = await UserRoleServices.getUserRoles([{
            userid: user.id, companyid: user.companyid, roleid: thisRole.id
          }], DatabaseCommand.lockStrength.KEY_SHARE, client);
          if (!userRoles.length) {
            await UserRoleServices.addUserRole({
              userid: user.id, companyid: user.companyid, roleid: thisRole.id
            }, client);
          }
        }));
      }
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
};
