import {Router, Application} from "express";
import Routes from "../../../constant/Routes";
import AdminUserController from "./UserController";
import {passportJwt} from "../../../config/passport";
import checkUser from "../../../cors/checkUser";

const router: Router = Router();

export default (app: Application) => {
  router.route("/")
    .get(passportJwt, checkUser, AdminUserController.getUsers)
    .post(passportJwt, checkUser, AdminUserController.addUser);

  router.route('/:userId')
    .get(passportJwt, checkUser, AdminUserController.getUser)
    .put(passportJwt, checkUser, AdminUserController.updateUser, AdminUserController.getUser);

  router.route("/:userId/disabled")
    .post(passportJwt, checkUser, AdminUserController.markUserAsDisabled, AdminUserController.getUser);

  router.route("/:userId/available")
    .post(passportJwt, checkUser, AdminUserController.markUserAsAvailable, AdminUserController.getUser);

  router.route('/:userId/validatetoken')
    .get(passportJwt, checkUser, AdminUserController.getUserValidateToken);

  app.use(Routes.ADMIN_USERS, router);
};
