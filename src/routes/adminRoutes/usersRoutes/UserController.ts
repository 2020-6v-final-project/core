import { NextFunction, Request, Response } from "express";
import httpResponseHandler from "../../../cors/httpResponseHandler";
import { IBranch } from "../../../models/IBranch";
import { IRole } from "../../../models/public/IRole";
import { getValidateToken } from "../../../models/public/IUser";
import { IRequestUser } from "../../../models/temp/IRequestUser";
import BranchServices from "../../../services/BranchServices";
import CompanyServices from "../../../services/public/CompanyServices";
import RoleServices from "../../../services/public/RoleServices";
import StatusServices from "../../../services/public/StatusServices";
import UserRoleServices from "../../../services/public/UserRoleServices";
import UserServices from "../../../services/public/UserServices";
import UserBranchServices from "../../../services/UserBranchServices";
import DatabaseCommand from "../../../utils/DatabaseCommand";

export default {
  getUserValidateToken: async (req: Request, res: Response) => {
    const thisUser = req.user as IRequestUser;
    const userId = parseInt(req.params.userId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let users = await UserServices.getUsers([{
        id: userId, companyid: thisUser.companyid
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${userId} not found.`);
      const user = users[0];
      getValidateToken(user);
      const updatedUser = await UserServices.updateOrSaveUser(user, client);
      if (!updatedUser) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 200, {
        userid: updatedUser.id,
        validatetoken: user.validatetoken
      });
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  getUsers: async (req: Request, res: Response) => {
    const thisUser = req.user as IRequestUser;
    try {
      let users = await UserServices.getUsers([{
        companyid: thisUser.companyid
      }]);
      if (typeof (req.query.withroles) !== 'undefined') {
        users = await Promise.all(users.map(async user => {
          const thisUser = user as any;
          thisUser.hasroles = await RoleServices.getRolesOfUser(user);
          // thisUser.nothaveroles = (await RoleServices.getRoles([], thisUser.companyid)).filter(role => {
          //   const found = thisUser.hasroles.find((item: { id: number; }) => item.id === role.id);
          //   return found === undefined;
          // });
          return thisUser;
        }))
      }
      if (typeof (req.query.withbranches) !== 'undefined') {
        users = await Promise.all(users.map(async user => {
          const thisUser = user as any;
          thisUser.branches = await BranchServices.getBranchesOfUser(user);
          // thisUser.nothaveroles = (await RoleServices.getRoles([], thisUser.companyid)).filter(role => {
          //   const found = thisUser.hasroles.find((item: { id: number; }) => item.id === role.id);
          //   return found === undefined;
          // });
          return thisUser;
        }))
      }
      return httpResponseHandler.responseToClient(res, 200, users);
    } catch (err) {
      console.log(err);
      return httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  getUser: async (req: Request, res: Response) => {
    const thisUser = req.user as IRequestUser;
    const userId = parseInt(req.params.userId);
    try {
      let users = await UserServices.getUsers([{
        id: userId, companyid: thisUser.companyid
      }]);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${userId} not found.`);
      const user = users[0] as any;
      user.hasroles = await RoleServices.getRolesOfUser(user);
      // user.nothaveroles = (await RoleServices.getRoles([], user.companyid)).filter(role => {
      //   const found = user.hasroles.find((item: { id: number; }) => item.id === role.id);
      //   return found === undefined;
      // });
      user.branches = await BranchServices.getBranchesOfUser(user);
      return httpResponseHandler.responseToClient(res, 200, user);
    } catch (err) {
      console.log(err);
      return httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  addUser: async (req: Request, res: Response) => {
    const thisUser = req.user as IRequestUser;
    const body = req.body;
    delete body.id;
    delete body.joinedat;
    delete body.validatetoken;
    delete body.validatetokenexpiredat;
    delete body.statusid;
    delete body.statuscode;
    const roles = body.hasroles as IRole[];
    const branches = body.branches as IBranch[];
    delete body.branches;
    delete body.hasroles;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let companies = await CompanyServices.getCompanies([{ id: thisUser.companyid }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!companies.length) return httpResponseHandler.responseErrorToClient(res, 404, `Company has id ${thisUser.companyid} not found.`);
      let userCompany = companies[0];
      body.companyid = userCompany.id;
      body.companycode = userCompany.code;
      userCompany.numberofusers++;
      const updatedCompany = await CompanyServices.updateCompanies(userCompany, client);
      if (updatedCompany) {
        let { newUser, newAccount } = await UserServices.createUserWithAccount(body, client);
        if (roles) {
          await Promise.all(roles.map(async role => {
            await UserRoleServices.addUserRole({
              userid: newUser.id, companyid: newUser.companyid, roleid: role.id
            }, client);
          }));
        }
        if (branches) {
          await Promise.all(branches.map(async branch => {
            const thisBranch = (await BranchServices.getBranches([{
              id: branch.id
            }], thisUser.companycode, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client))[0];
            await UserBranchServices.addOne({
              userid: newUser.id, branchid: branch.id
            }, newUser.companycode, client);
            thisBranch.numberofusers++;
            await BranchServices.updateBranch(thisBranch, thisUser.companycode, client);
          }))
        }
        await DatabaseCommand.commitTransaction(client);
        return httpResponseHandler.responseToClient(res, 201, {
          newUser,
          newAccount
        });
      } else {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },
  updateUser: async (req: Request, res: Response, next: NextFunction) => {
    const thisUser = req.user as IRequestUser;
    const userId = parseInt(req.params.userId);
    const body = req.body;
    delete body.id;
    delete body.joinedat;
    delete body.validatetoken;
    delete body.validatetokenexpiredat;
    delete body.statusid;
    delete body.statuscode;
    delete body.companycode;
    delete body.companyid;
    let roles = body.hasroles as IRole[];
    let branches = body.branches as IBranch[];
    delete body.branches;
    delete body.hasroles;
    delete body.nothaveroles;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let users = await UserServices.getUsers([{
        id: userId,
        companyid: thisUser.companyid
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${userId} not found.`);
      let user = users[0];
      Object.assign(user, body);
      const updatedUser = await UserServices.updateOrSaveUser(user, client);
      if (!updatedUser) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      // if (nothaveroles instanceof Array) {
      //   await Promise.all(nothaveroles.map(async (role: { id: number; }) => {
      //     await UserRoleServices.removeUserRole({
      //       userid: updatedUser.id, companyid: updatedUser.companyid, roleid: role.id
      //     }, client);
      //   }));
      // }
      if (roles) {
        await Promise.all((await UserRoleServices.getUserRoles([{
          userid: updatedUser.id, companyid: updatedUser.companyid
        }], DatabaseCommand.lockStrength.UPDATE, client)).map(async userRole => {
          const foundIndex = roles.findIndex(item => item.id === userRole.roleid);
          if (foundIndex > -1) roles.splice(foundIndex, 1);
          else await UserRoleServices.removeUserRole(userRole, client);
        }));
        await Promise.all(roles.map(async role => {
          await UserRoleServices.addUserRole({
            userid: updatedUser.id, companyid: updatedUser.companyid, roleid: role.id
          }, client);
        }));
      }
      if (branches) {
        const userBranches = await UserBranchServices.getUserBranches([{
          userid: updatedUser.id, companyid: updatedUser.companyid
        }], updatedUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
        for (const userBranch of userBranches) {
          const foundIndex = branches.findIndex(item => item.id === userBranch.branchid);
          if (foundIndex > -1) branches.splice(foundIndex, 1);
          else {
            const thisBranch = (await BranchServices.getBranches([{
              id: userBranch.branchid
            }], thisUser.companycode, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client))[0];
            thisBranch.numberofusers--;
            await UserBranchServices.removeOne(userBranch, updatedUser.companycode, client);
            await BranchServices.updateBranch(thisBranch, updatedUser.companycode, client);
          }
        }
        for (const branch of branches) {
          const thisBranch = (await BranchServices.getBranches([{
            id: branch.id
          }], thisUser.companycode, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client))[0];
          thisBranch.numberofusers++;
          await UserBranchServices.addOne({
            userid: updatedUser.id, branchid: branch.id
          }, updatedUser.companycode, client);
          await BranchServices.updateBranch(thisBranch, updatedUser.companycode, client);
        }
      }
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },
  async markUserAsDisabled(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const userId = parseInt(req.params.userId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let users = await UserServices.getUsers([{
        id: userId,
        companyid: thisUser.companyid,
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${userId} not found.`);
      await StatusServices.setStatusCodeToObjects(UserServices.IUserTableName, 'DISABLED', [{
        id: users[0].id,
        companyid: users[0].companyid
      }], undefined, client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },
  async markUserAsAvailable(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const userId = parseInt(req.params.userId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let users = await UserServices.getUsers([{
        id: userId,
        companyid: thisUser.companyid,
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${userId} not found.`);
      await StatusServices.setStatusCodeToObjects(UserServices.IUserTableName, 'AVAILABLE', [{
        id: users[0].id,
        companyid: users[0].companyid
      }], undefined, client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  }
};
