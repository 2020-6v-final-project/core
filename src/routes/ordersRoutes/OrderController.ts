// import { Request, Response } from "express";
// import httpResponseHandler from "../../../cors/httpResponseHandler";
// import { IOrder } from "../../../models/IOrder";
// import { IOrderItem } from "../../../models/IOrderItem";
// import { IUser } from "../../../models/public/IUser";
// import ItemServices from "../../../services/ItemServices";
// import OrderItemServices from "../../../services/OrderItemServices";
// import OrderServices from "../../../services/OrderServices";
// import StatusServices from "../../../services/public/StatusServices";
// import UserBranchServices from "../../../services/UserBranchServices";
// import DatabaseCommand from "../../../utils/DatabaseCommand";
// import {IRequestUser} from "../../../models/temp/IRequestUser";
//
// export default {
//     createOrder: async (req: Request, res: Response) => {
//       //check ThisUser is in this Branch, check Item is in the menu used by this Branch
//         const client = await DatabaseCommand.newClient();
//         const thisUser = req.user as IRequestUser;
//         const orderInformation = req.body.orderInformation as IOrder;
//         const listItemOrder = req.body.listItemOrder as IOrderItem[];
//         //list ItemOrder will be a list of Object defined by {itemid, `optionid`, quanlity}
//         const listItemToChef = [];
//         //list ItemToChef will be a list of Object defined by {itemid, name, quanlity, tableid/ numerical order}
//         try {
//         await DatabaseCommand.beginTransaction(client);
//         //IUserBranch to get branchid from user
//         const listBranchOfthisUser = await UserBranchServices.getAllUserId(thisUser.id, thisUser.companycode.toLowerCase(), client, undefined);
//         const foundUserBranch = listBranchOfthisUser.find(value => value.branchid == orderInformation.branchid);
//         if (!foundUserBranch) return httpResponseHandler.responseErrorToClient(res, 404, "IUser can not access that IBranch");
//         orderInformation.waiter = thisUser.id;
//         orderInformation.createdat = new Date();
//         orderInformation.ordertype = "STAY";
//         const newOrder = await OrderServices.createOrder(orderInformation, thisUser.companycode.toLowerCase(), client);
//         const listItem = await ItemServices.getItems([], thisUser.companycode.toLowerCase(), undefined, client);
//         let total = 0;
//         for (let orderItem of listItemOrder) {
//           orderItem.orderid = newOrder.id;
//           //must check from menu_item, btw check by List Item for now
//           const thisItem = listItem.find(value => value.id == orderItem.itemid);
//           if (thisItem) {
//             orderItem.price = thisItem.price;
//             orderItem.total = thisItem.price*orderItem.quantity;
//             listItemToChef.push({name: thisItem.name, quantity: orderItem.quantity});
//           }
//           total+=orderItem.total;
//           await OrderItemServices.addOne(orderItem, thisUser.companycode.toLowerCase(), client);
//         }
//         if (total != orderInformation.total) return httpResponseHandler.responseErrorToClient(res, 404, "Error in calculate of payment amount!!!!");
//         await DatabaseCommand.commitTransaction(client);
//         const responseObject = {newOrder, listItemToChef};
//         httpResponseHandler.responseToClient(res, 200, responseObject);
//         } catch (err) {
//         console.log(err);
//         await DatabaseCommand.rollbackTransaction(client);
//         httpResponseHandler.responseErrorToClient(res, 400, err.message);
//         } finally {
//         client.release();
//         }
//     },
//     expendOrder: async (req: Request, res: Response) => {
//       const client = await DatabaseCommand.newClient();
//       const thisUser = req.user as IRequestUser;
//       const listExpendItem = req.body.listExpendItem as IOrderItem[];
//       console.log(listExpendItem);
//       if (!listExpendItem) return httpResponseHandler.responseErrorToClient(res, 404, "Expend list is empty!");
//       //list ItemOrder will be a list of Object defined by {itemid, `optionid`, quanlity}
//       const listItemToChef = [];
//       //list ItemToChef will be a list of Object defined by {itemid, name, quanlity, tableid/ numerical order}
//       try {
//         const listOrder = await OrderServices.getOrders([{id: req.params.orderid}], thisUser.companycode.toLowerCase(), client, undefined);
//         const targetOrder = listOrder[0];
//         if (!targetOrder) return httpResponseHandler.responseErrorToClient(res, 404, "This IOrder not found!");
//         if (targetOrder.statuscode === "DONE") return httpResponseHandler.responseErrorToClient(res, 404, "Cannot access that IOrder");
//         const listItem = await ItemServices.getItems([], thisUser.companycode.toLowerCase(), undefined, client);
//         const thisOrderItem = await OrderItemServices.getAllByOrderId(targetOrder.id, thisUser.companycode.toLowerCase(), client, undefined);
//         for (let orderItem of listExpendItem) {
//           const foundOrderItem = thisOrderItem.find(value => value.itemid === orderItem.itemid);
//           const thisItem = listItem.find(value => value.id === orderItem.itemid);
//           if (thisItem) {
//             listItemToChef.push({name: thisItem.name, quantity: orderItem.quantity});
//           }
//           if (foundOrderItem) {
//             foundOrderItem.quantity+=orderItem.quantity;
//             foundOrderItem.total = foundOrderItem.quantity*foundOrderItem.price;
//             await OrderItemServices.updateOne(foundOrderItem, thisUser.companycode.toLowerCase(), client);
//           } else {
//             if (thisItem) {
//               orderItem.orderid = targetOrder.id;
//               orderItem.price = thisItem.price;
//               orderItem.total = orderItem.price*orderItem.quantity;
//               await OrderItemServices.addOne(orderItem, thisUser.companycode.toLowerCase(), client);
//             } else {
//               return httpResponseHandler.responseErrorToClient(res, 404, "Targer IItem not found!");
//             }
//           }
//         }
//         await DatabaseCommand.commitTransaction(client);
//         httpResponseHandler.responseToClient(res, 200, listItemToChef);
//       } catch (err) {
//         console.log(err);
//         await DatabaseCommand.rollbackTransaction(client);
//         httpResponseHandler.responseErrorToClient(res, 403, err.message);
//       } finally {
//         client.release();
//       }
//
//     },
//     paidOrder: async (req: Request, res: Response) => {
//       const client = await DatabaseCommand.newClient();
//       const thisUser = req.user as IRequestUser;
//       try {
//         const listOrder = await OrderServices.getOrders([{id: parseInt(req.params.orderid)}], thisUser.companycode.toLowerCase(), client, undefined);
//         const targetOrder = listOrder[0];
//         if (!targetOrder) return httpResponseHandler.responseErrorToClient(res, 404, "IOrder not found!");
//         if (targetOrder.statuscode === "DONE") return httpResponseHandler.responseErrorToClient(res, 404, "Cannot access IOrder");
//         const targetItemOrder = await OrderItemServices.getOrdersWithDetail(parseInt(req.params.orderid), thisUser.companycode.toLowerCase(), client, undefined);
//         let tmpTotal = 0;
//         for (let item of targetItemOrder) {
//           tmpTotal+=parseInt(item.total);
//         }
//         console.log(tmpTotal);
//         targetOrder.total = tmpTotal;
//         targetOrder.cashier = thisUser.id;
//         targetOrder.paidat = new Date();
//         const setResultStatus = await StatusServices.setStatusCodeToObjects(OrderServices.IOrderTableName, "DONE", [targetOrder], thisUser.companycode.toLowerCase(), client);
//         const updatedOrder = await OrderServices.updateOrder(targetOrder, thisUser.companycode.toLowerCase(), client);
//         const responseObject = {updatedOrder, targetItemOrder};
//         await DatabaseCommand.commitTransaction(client);
//         httpResponseHandler.responseToClient(res, 200, responseObject);
//       } catch (err) {
//         console.log(err);
//         await DatabaseCommand.rollbackTransaction(client);
//         httpResponseHandler.responseErrorToClient(res, 403, err.message);
//       } finally {
//         client.release();
//       }
//     },
//     createTakeAwayOrder: async (req: Request, res: Response) => {
//       const thisUser = req.user as IRequestUser;
//       const client = await DatabaseCommand.newClient();
//       const orderInformation = req.body.orderInformation as IOrder;
//       const listItemOrder = req.body.listItemOrder as IOrderItem[];
//       let listItemToChef = [];
//       try {
//         //create order -> add Order Item -> paid
//         orderInformation.createdat = new Date();
//         orderInformation.waiter = thisUser.id;
//         orderInformation.ordertype = "TAKEAWAY";
//         const createdOrder = await OrderServices.createOrder(orderInformation, thisUser.companycode.toLowerCase(), client);
//         //check from menu item
//         const listItem = await ItemServices.getItems([], thisUser.companycode.toLowerCase(), undefined, client);
//         let total = 0;
//         for (let itemOrder  of listItemOrder) {
//           itemOrder.orderid = createdOrder.id;
//           const thisItem = listItem.find(value => itemOrder.itemid == value.id);
//           if (thisItem) {
//             itemOrder.price = thisItem.price;
//             itemOrder.total = itemOrder.price*itemOrder.quantity;
//             total+=itemOrder.total;
//             await OrderItemServices.addOne(itemOrder, thisUser.companycode.toLowerCase(), client);
//             listItemToChef.push({name: thisItem.name, quantity: itemOrder.quantity});
//           } else {
//             return httpResponseHandler.responseErrorToClient(res, 404, "This Item is not exist!" + itemOrder.itemid);
//           }
//         }
//         await StatusServices.setStatusCodeToObjects(OrderServices.IOrderTableName, "DONE", [createdOrder], thisUser.companycode.toLowerCase(), client);
//         createdOrder.cashier = thisUser.id;
//         createdOrder.total = total;
//         createdOrder.paidat = new Date();
//         const updatedOrder = await OrderServices.updateOrder(createdOrder, thisUser.companycode.toLowerCase(), client);
//         let targetItemOrder = []
//         if (updatedOrder) {
//           targetItemOrder = await OrderItemServices.getOrdersWithDetail(updatedOrder.id, thisUser.companycode.toLowerCase(), client);
//         }
//         const responseObject = {updatedOrder, listItemToChef, targetItemOrder};
//         await DatabaseCommand.commitTransaction(client);
//         httpResponseHandler.responseToClient(res, 200, responseObject);
//
//       } catch (err) {
//         console.log(err);
//         await DatabaseCommand.rollbackTransaction(client);
//         httpResponseHandler.responseErrorToClient(res, 404, err.message);
//       } finally {
//         client.release();
//       }
//     },
//     updateOrder: async(req: Request, res: Response) => {
//         const client = await DatabaseCommand.newClient();
//         const bodyOrder= req.body as IOrder;
//         const thisUser = req.user as IRequestUser;
//         try {
//         await DatabaseCommand.beginTransaction(client);
//         let listOrder = await OrderServices.getOrders([{ id: bodyOrder.id }], thisUser.companycode.toLowerCase(), client, undefined);
//         let targetOrder = listOrder[0];
//         if (!targetOrder) return httpResponseHandler.responseErrorToClient(res, 404, "Target Order not found!");
//         if (req.body.cashier) targetOrder.cashier = req.body.cashier;
//         if (req.body.waiter) targetOrder.waiter = req.body.waiter;
//         if (req.body.currency) targetOrder.currency = req.body.currency;
//         if (req.body.tableid) targetOrder.tableid = req.body.tableid;
//         if (req.body.total) targetOrder.total = req.body.total;
//         if (req.body.paymenttype) targetOrder.paymenttype = req.body.paymenttype;
//         if (req.body.paidat) targetOrder.paidat = req.body.paidat;
//         if (req.body.ordertype) targetOrder.ordertype = req.body.ordertype;
//
//         const updatedOrder = await OrderServices.updateOrder(targetOrder, thisUser.companycode.toLowerCase(), client);
//         await DatabaseCommand.commitTransaction(client);
//         httpResponseHandler.responseToClient(res, 200, updatedOrder);
//         } catch (err) {
//         console.log(err);
//         await DatabaseCommand.rollbackTransaction(client);
//         httpResponseHandler.responseErrorToClient(res, 400, err.message);
//         } finally {
//         client.release();
//         }
//     },
//
//     deactivateOrder: async(req: Request, res: Response) => {
//         const client = await DatabaseCommand.newClient();
//         const thisUser = req.user as IRequestUser;
//         try {
//           await DatabaseCommand.beginTransaction(client);
//           const listOrder = await OrderServices.getOrders([{ id: req.params.orderid }], thisUser.companycode.toLowerCase(), client, DatabaseCommand.lockStrength.UPDATE);
//           let targetOrder = listOrder[0];
//           if (!targetOrder) return httpResponseHandler.responseErrorToClient(res, 404, "Target Order not found!");
//           // let setStatusResult = await StatusServices.setStatus(targetOrder, OrderServices.IOrderTableName, "DISABLED");
//           let setStatusResult = await StatusServices.setStatusCodeToObjects(OrderServices.IOrderTableName, "DISABLED",[targetOrder],thisUser.companycode.toLowerCase(), client);
//
//           await DatabaseCommand.commitTransaction(client);
//           httpResponseHandler.responseToClient(res, 202, { result: setStatusResult });
//         } catch (err) {
//           console.log(err);
//           await DatabaseCommand.rollbackTransaction(client);
//           httpResponseHandler.responseErrorToClient(res, 400, err.message);
//         } finally {
//           client.release();
//         }
//       },
//     reactiveOrder: async(req: Request, res: Response) => {
//         const client = await DatabaseCommand.newClient();
//         const thisUser = req.user as IRequestUser;
//         try {
//           await DatabaseCommand.beginTransaction(client);
//           const listOrder = await OrderServices.getOrders([{ id: req.params.orderid }], thisUser.companycode.toLowerCase(), client, DatabaseCommand.lockStrength.UPDATE);
//           let targetOrder = listOrder[0];
//           if (!targetOrder) return httpResponseHandler.responseErrorToClient(res, 404, "Target Order not found!");
//           // let setStatusResult = await StatusServices.setStatus(targetOrder, OrderServices.IOrderTableName, "AVAILABLE");
//           let setStatusResult = await StatusServices.setStatusCodeToObjects(OrderServices.IOrderTableName, "AVAILABLE",[targetOrder],thisUser.companycode.toLowerCase(), client);
//
//           await DatabaseCommand.commitTransaction(client);
//           httpResponseHandler.responseToClient(res, 202, { result: setStatusResult });
//         } catch (err) {
//           console.log(err);
//           await DatabaseCommand.rollbackTransaction(client);
//           httpResponseHandler.responseErrorToClient(res, 400, err.message);
//         } finally {
//           client.release();
//         }
//       },
//     getOrders: async(req: Request, res: Response) => {
//         const thisUser = req.user as IRequestUser;
//         try {
//           //find the companyid or code of logged in account -> User
//           const orders = await OrderServices.getOrders([], thisUser.companycode.toLowerCase(), undefined, undefined);
//           console.log(orders);
//           httpResponseHandler.responseToClient(res, 200, orders);
//         } catch (err) {
//           console.log(err);
//           httpResponseHandler.responseErrorToClient(res, 500, err.message);
//         }
//       },
//     getOrderById: async(req: Request, res: Response) => {
//         const thisUser = req.user as IRequestUser;
//         try {
//           const orderList = await OrderServices.getOrders([{ id: parseInt(req.params.orderid) }], thisUser.companycode.toLowerCase(), undefined, undefined);
//           let targetOrder = orderList[0];
//           if (!targetOrder) return httpResponseHandler.responseErrorToClient(res, 404, "Target IOrder not found!");
//           const targetOrderItem = await OrderItemServices.getOrdersWithDetail(parseInt(req.params.orderid), thisUser.companycode.toLowerCase(), undefined, undefined);
//           //attach information of Manager or not ?
//           const responseObject = {targetOrder, targetOrderItem}
//           httpResponseHandler.responseToClient(res, 200, responseObject);
//         } catch (err) {
//           console.log(err);
//           httpResponseHandler.responseErrorToClient(res, 500, err.message);
//         }
//       },
// }
