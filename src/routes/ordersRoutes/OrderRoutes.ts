// import {Application, Router} from "express";
// import {passportJwt} from "../../config/passport";
// import Routes from "../../constant/Routes";
// import checkUser from "../../cors/checkUser";
// import orderController from "./OrderController";
//
// const router: Router = Router();
//
// export default (app: Application) => {
//   router.route('/')
//     .get(passportJwt, checkUser, orderController.getOrders)
//     .post(passportJwt, checkUser, orderController.createOrder);
//   router.route('/takeaway')
//     .post(passportJwt, checkUser, orderController.createTakeAwayOrder);
//
//   router.route("/deactivate/:orderid").post(passportJwt, checkUser, orderController.deactivateOrder);
//   router.route("/reactivate/:orderid").post(passportJwt, checkUser, orderController.reactiveOrder);
//
//   router.route('/:orderid')
//     .get(passportJwt, checkUser, orderController.getOrderById)
//     .put(passportJwt, checkUser, orderController.updateOrder);
//   router.route('/expand/:orderid')
//     .post(passportJwt, checkUser, orderController.expendOrder);
//   router.route('/pay/:orderid')
//     .get(passportJwt, checkUser, orderController.paidOrder);
//   app.use(Routes.ORDERS, router);
// }
