import {NextFunction, Request, Response} from "express";
import httpResponseHandler from "../../cors/httpResponseHandler";
import RequestTypeServices from "../../services/public/RequestTypeServices";
import {IUser} from "../../models/public/IUser";
import ItemServices from "../../services/ItemServices";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IRequestType} from "../../models/public/IRequestType";
import {IRequestUser} from "../../models/temp/IRequestUser";

export default {

  async getRequestTypes(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    try {
      let requestTypes = await RequestTypeServices.getRequestTypes([]);
      // if (typeof (req.query.withitems) !== 'undefined') {
      //   requestTypes = await Promise.all(requestTypes.map(async requestTypes => {
      //     const thisItemType = itemType as any;
      //     thisItemType.items = await ItemServices.getItems([{
      //       typeid: thisItemType.id
      //     }], thisUser.companycode);
      //     return thisItemType;
      //   }));
      // }
      return httpResponseHandler.responseToClient(res, 200, requestTypes);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
}