import {Application, Router} from "express";
import {passportJwt} from "../../config/passport";
import checkUser from "../../cors/checkUser";
import RequestTypeController from "./RequestTypeController";
import Routes from "../../constant/Routes";

const router = Router();

export default function(app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, RequestTypeController.getRequestTypes)
    

  app.use(Routes.REQUEST_TYPES, router);
}
