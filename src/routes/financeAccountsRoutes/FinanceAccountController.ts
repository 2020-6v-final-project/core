import {NextFunction, Request, Response} from "express";
import httpResponseHandler from "../../cors/httpResponseHandler";
import DatabaseCommand from "../../utils/DatabaseCommand";
import FinanceAccountServices from "../../services/FinanceAccountServices";
import {IRequestUser} from "../../models/temp/IRequestUser";
import StatusServices from "../../services/public/StatusServices";
import FinanceValidateServices from "../../services/FinanceValidateServices";

export default {

  async getFinanceAccounts(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    try {
      let accounts = await FinanceAccountServices.getFinanceAccounts([], thisUser.companycode);
      if (!thisUser.validRole.isall) {
        accounts = accounts.filter(account => thisUser.branchIds.includes(account.branchid));
      }
      return httpResponseHandler.responseToClient(res, 200, accounts);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async createFinanceAccount(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const body = req.body;
      delete body.id;
      delete body.statusid;
      delete body.statuscode;
      if (body.branchid) if (!thisUser.validRole.isall && !thisUser.branchIds.includes(body.branchid)) return httpResponseHandler.responseErrorToClient(res, 403);
      else if (!thisUser.validRole.isall) return httpResponseHandler.responseErrorToClient(res, 403);
      const insertedFinanceAccount = (await FinanceAccountServices.createFinanceAccounts([body], thisUser.companycode, client))[0];
      if (!insertedFinanceAccount) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 201, insertedFinanceAccount);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async getFinanceAccount(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const accountId = parseInt(req.params.accountId);
    try {
      const account = (await FinanceAccountServices.getFinanceAccounts([{
        id: accountId
      }], thisUser.companycode))[0];
      if (!thisUser.validRole.isall && !thisUser.branchIds.includes(account.branchid)) return httpResponseHandler.responseErrorToClient(res, 403);
      if (!account) return httpResponseHandler.responseErrorToClient(res, 404, `Account has id ${accountId} not found.`);
      return httpResponseHandler.responseToClient(res, 200, account);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  // async updateFinanceAccount(req: Request, res: Response, next: NextFunction) {
  //   const thisUser = req.user as IRequestUser;
  //   const accountId = parseInt(req.params.accountId);
  //   const client = await DatabaseCommand.newClient();
  //   try {
  //     await DatabaseCommand.beginTransaction(client);
  //     const account = (await FinanceAccountServices.getFinanceAccounts([{
  //       id: accountId
  //     }], thisUser.companycode, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client))[0];
  //     if (!account) return httpResponseHandler.responseErrorToClient(res, 404, `Account has id ${accountId} not found.`);
  //     const body = req.body;
  //     delete body.id;
  //     delete body.statusid;
  //     delete body.statuscode;
  //     delete body.currentbalance; //current balance must be change by check, validate or deposit
  //     delete body.currency;
  //     await DatabaseCommand.commitTransaction(client);
  //   } catch (e) {
  //     console.log(e);
  //     await DatabaseCommand.rollbackTransaction(client);
  //     return httpResponseHandler.responseErrorToClient(res, 500);
  //   } finally {
  //     client.release();
  //   }
  // },
  // async deleteFinanceAccount(req: Request, res: Response, next: NextFunction) {
  //   const thisUser = req.user as IRequestUser;
  //   const accountId = parseInt(req.params.accountId);
  //   const client = await DatabaseCommand.newClient();
  //   try {
  //     await DatabaseCommand.beginTransaction(client);
  //     await DatabaseCommand.commitTransaction(client);
  //   } catch (e) {
  //     console.log(e);
  //     await DatabaseCommand.rollbackTransaction(client);
  //     return httpResponseHandler.responseErrorToClient(res, 500);
  //   } finally {
  //     client.release();
  //   }
  // },
  async markFinanceAccountAsDisabled(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const accountId = parseInt(req.params.accountId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const account = (await FinanceAccountServices.getFinanceAccounts([{
        id: accountId
      }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
      if (!account || account.statuscode === 'DISABLED') return httpResponseHandler.responseErrorToClient(res, 404, `Account has id ${req.params.accountId} not found or unavailable.`);
      if (account.branchid) if (!thisUser.validRole.isall && !thisUser.branchIds.includes(account.branchid)) return httpResponseHandler.responseErrorToClient(res, 403);
      else if (!thisUser.validRole.isall) return httpResponseHandler.responseErrorToClient(res, 403);
      await StatusServices.setStatusCodeToObjects(FinanceAccountServices.IFinanceAccountTableName, 'DISABLED', [{
        id: accountId
      }], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async markFinanceAccountAsAvailable(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const accountId = parseInt(req.params.accountId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const account = (await FinanceAccountServices.getFinanceAccounts([{
        id: accountId
      }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
      if (!account || account.statuscode === 'AVAILABLE') return httpResponseHandler.responseErrorToClient(res, 404, `Account has id ${req.params.accountId} not found or already available.`);
      if (account.branchid) if (!thisUser.validRole.isall && !thisUser.branchIds.includes(account.branchid)) return httpResponseHandler.responseErrorToClient(res, 403);
      await StatusServices.setStatusCodeToObjects(FinanceAccountServices.IFinanceAccountTableName, 'AVAILABLE', [{
        id: accountId
      }], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async validateFinanceAccount(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const accountId = parseInt(req.params.accountId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const account = (await FinanceAccountServices.getFinanceAccounts([{
        id: accountId
      }], thisUser.companycode, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client))[0];
      if (!account) return httpResponseHandler.responseErrorToClient(res, 404, `Account has id ${req.params.accountId} not found.`);
      if (account.branchid) if (!thisUser.validRole.isall && !thisUser.branchIds.includes(account.branchid)) return httpResponseHandler.responseErrorToClient(res, 403);
      const body = req.body;
      const inputData = {
        accountid: accountId,
        currentbalance: account.currentbalance,
        actualbalance: parseFloat(body.actualbalance),
        note: body.note,
        validatedat: new Date(),
        validatedby: thisUser.id,
        difference: 0
      };
      if (!inputData.actualbalance) return httpResponseHandler.responseErrorToClient(res, 400, `Invalid actual balance: ${inputData.actualbalance}`);
      inputData.difference = inputData.actualbalance - inputData.currentbalance;
      await FinanceValidateServices.createFinanceValidates([inputData], thisUser.companycode, client);
      account.currentbalance = inputData.actualbalance;
      const updatedAccount = await FinanceAccountServices.updateFinanceAccounts(account, [{id: account.id}], thisUser.companycode, client);
      if (!updatedAccount) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
