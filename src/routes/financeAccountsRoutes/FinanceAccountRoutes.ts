import {Application, Router} from "express";
import Routes from "../../constant/Routes";
import {passportJwt} from "../../config/passport";
import checkUser from "../../cors/checkUser";
import FinanceAccountController from "./FinanceAccountController";

const router = Router();

//Finance accounts for companies
export default function (app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, FinanceAccountController.getFinanceAccounts)
    .post(passportJwt, checkUser, FinanceAccountController.createFinanceAccount);

  router.route('/:accountId')
    .get(passportJwt, checkUser, FinanceAccountController.getFinanceAccount)
    // .put(passportJwt, checkUser, FinanceAccountController.updateFinanceAccount, FinanceAccountController.getFinanceAccount)
    // .delete(passportJwt, checkUser, FinanceAccountController.deleteFinanceAccount);

  router.route('/:accountId/disabled')
    .post(passportJwt, checkUser, FinanceAccountController.markFinanceAccountAsDisabled, FinanceAccountController.getFinanceAccount);

  router.route('/:accountId/available')
    .post(passportJwt, checkUser, FinanceAccountController.markFinanceAccountAsAvailable, FinanceAccountController.getFinanceAccount);

  router.route('/:accountId/validate')
    .post(passportJwt, checkUser, FinanceAccountController.validateFinanceAccount, FinanceAccountController.getFinanceAccount)

  app.use(Routes.ADMIN_FINANCE_ACCOUNTS, router);
}
