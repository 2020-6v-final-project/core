import c from 'config';
import { NextFunction, Request, Response} from 'express';
import { isBuffer } from 'lodash';
import httpResponseHandler from '../../../cors/httpResponseHandler';
import { IReceiptIngredient } from '../../../models/IReceiptIngredient';
import { IVendorIngredient } from '../../../models/IVendorIngredient';
import { IRequestUser } from "../../../models/temp/IRequestUser"
import BranchServices from '../../../services/BranchServices';
import StatusServices from '../../../services/public/StatusServices';
import ReceiptIngredientServices from '../../../services/ReceiptIngredientServices';
import ReceiptServices from '../../../services/ReceiptServices';
import RepositoryServices from '../../../services/RepositoryServices';
import RequestServies from '../../../services/RequestServies';
import VendorIngredientServices from '../../../services/VendorIngredientServices';
import DatabaseCommand from '../../../utils/DatabaseCommand';
import types from 'pg-types';
import { body } from 'express-validator';

export default {

  async createReceipt (req: Request, res: Response, next: NextFunction) {
    //create receipt => receipt_ingre => request
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    const bodyReceipt = req.body;
    // const listIngredient = bodyReceipt.ingredients as IReceiptIngredient[];
    // delete bodyReceipt.ingredients;
    const branchid = bodyReceipt.branchid;
    try {
      await DatabaseCommand.beginTransaction(client);
      const targetBranch = (await BranchServices.getBranches([{id: branchid}], thisUser.companycode, undefined, client))[0];
      if (!targetBranch) {
        return httpResponseHandler.responseErrorToClient(res, 500, `Branch ${branchid} not found!`);
      }
      bodyReceipt.repositoryid = targetBranch.repositoryid;
      bodyReceipt.createdby = thisUser.id;
      bodyReceipt.createdat = new Date();
      await ReceiptServices.createReceipt(bodyReceipt, thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
      } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  async createRequest(req: Request, res: Response,next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const referenceid = req.params.receiptid;
    const client = await DatabaseCommand.newClient();
    const bodyRequest = req.body;
    delete bodyRequest.branchid;
    try {
      await DatabaseCommand.beginTransaction(client);
      const targetReceipt = (await ReceiptServices.getReceipts([{id: referenceid}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
      if (!targetReceipt) {
        return httpResponseHandler.responseErrorToClient(res, 500, `Receipt ${referenceid} not found!`);
      }
      if (targetReceipt.statuscode !== "EDITING") {
        return httpResponseHandler.responseErrorToClient(res, 500, `Cannot access Receipt ${referenceid}`);
      }
      const request = {requesttypeid: bodyRequest.requesttypeid, createdby: thisUser.id, referenceid: targetReceipt.id, createdat: new Date(), statusid: 17, statuscode: "PENDINGAPPROVED",note:bodyRequest.note};
      await RequestServies.createRequest(request, thisUser.companycode, client);
      const updatedReceipt = await StatusServices.setStatus(targetReceipt, `${thisUser.companycode}.${ReceiptServices.IReceiptTableName}`, "PENDINGAPPROVED", client);
      await DatabaseCommand.commitTransaction(client);
      next();
      } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  async getReceipts(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    const branchIds = thisUser.branchIds as any[];
    try {
      let receiptList = [] as any[];
      for (let a=0; a < branchIds.length; a++)
      {
        const listReceiptOfBranch = await ReceiptServices.getReceipts([{branchid: branchIds[a]}], thisUser.companycode, DatabaseCommand.lockStrength.SHARE, undefined) as any[];
        for( let b=0; b < listReceiptOfBranch.length; b++ ){
          const receiptIngredients = await ReceiptIngredientServices.getReceiptIngredients([{receiptid:listReceiptOfBranch[b].id}], thisUser.companycode, DatabaseCommand.lockStrength.SHARE, undefined) as any[];
          listReceiptOfBranch[b].listIngredients = receiptIngredients;
        };
        receiptList = receiptList.concat(listReceiptOfBranch);
      }
      httpResponseHandler.responseToClient(res, 200, receiptList);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  async updateReceipt(req: Request, res: Response) {
    //just update the information of Receipt.
    //if anychange in list ingredient use another route
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    const receiptId = parseInt(req.params.receiptid);
    const bodyReceipt = req.body;
    const ingredientList = req.body.ingredients as IReceiptIngredient[];
    delete bodyReceipt.ingredients;
    delete bodyReceipt.id;
    delete bodyReceipt.statusid;
    delete bodyReceipt.statuscode;
    try {
      await DatabaseCommand.beginTransaction(client);
      const targetReceipt = (await ReceiptServices.getReceipts([{id: receiptId, statuscode: "EDITING"}], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client))[0];
      if (!targetReceipt) {
        return httpResponseHandler.responseErrorToClient(res, 500, `Receipt ${receiptId} not found or cannot access!`);
      }
      if (targetReceipt.statuscode !== "EDITING") {
        return httpResponseHandler.responseErrorToClient(res, 500, `Cannot access Receipt ${receiptId}`);
      }
      const listVendorIngre = await VendorIngredientServices.getAll([], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client);
      //find update, add, del ingre to Receipt_Ingre
      const oldReceiptIngreList = await ReceiptIngredientServices.getReceiptIngredients([{receiptid: receiptId}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
      //general section =>> update if anychange
      const updateReceiptIngreList = ingredientList.filter(element => oldReceiptIngreList.find(value => value.ingredientid === element.ingredientid));
      const updatedReceiptIngreList = await Promise.all(updateReceiptIngreList.map(async (item) => {
        const found = oldReceiptIngreList.find(value => value.ingredientid === item.ingredientid && value.vendorid === item.vendorid);
        if (found) {       
          const tmp = found.price.toString();
          if (item.price !== parseFloat(tmp) || item.quantity !== found.quantity){
            types.setTypeParser(types.builtins.FLOAT8, (tmp) => {
              return parseFloat(tmp);
            });
            item.receiptid = targetReceipt.id;
            item.total = item.price*item.quantity;
            const updateRow = await ReceiptIngredientServices.updateReceiptIngredients(item, [{receiptid: item.receiptid, vendorid: item.vendorid, ingredientid: item.ingredientid}], thisUser.companycode, client);
            return updateRow;
          }
        }
      }));
      //new rows 
      const newReceiptIngreList = ingredientList.filter(element => !oldReceiptIngreList.find(value => value.ingredientid === element.ingredientid));
      const addedReceiptIngreList = await Promise.all(newReceiptIngreList.map(async (item: any) => {
        const found = listVendorIngre.find(value => item.ingredientid === value.ingredientid && item.vendorid === value.vendorid);
        if (found) {
          item.receiptid = targetReceipt.id;
          const addRow = await ReceiptIngredientServices.createReceiptIngredient(item, thisUser.companycode, client);
          return addRow;
        }  
      }));
      //delete rows 
      const deleteReceiptIngreList = oldReceiptIngreList.filter(element => !ingredientList.find(value => value.ingredientid === element.ingredientid));
      const deletedReceiptIngreList = await Promise.all(deleteReceiptIngreList.map(async (item: IReceiptIngredient) => {
        const delRow = await ReceiptIngredientServices.removeOne(item, thisUser.companycode, client);
        return delRow;
      }));
      //--> reject current request and create a new one.  
      await DatabaseCommand.commitTransaction(client);
      httpResponseHandler.responseToClient(res, 200, targetReceipt);
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  async getReceiptById(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    const receiptId = parseInt(req.params.receiptid);
    try {
      await DatabaseCommand.beginTransaction(client);
      const targetReceipt = (await ReceiptServices.getReceipts([{id: receiptId}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
      if (!targetReceipt) {
        return httpResponseHandler.responseErrorToClient(res, 500, `Receipt ${receiptId} not found!`);
      }
      const listReceiptIngredient = await ReceiptServices.getReceiptDetail(targetReceipt.id, thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client);
      const responseReceipt = targetReceipt as any;
      responseReceipt.receiptingredient = listReceiptIngredient;
      await DatabaseCommand.commitTransaction(client);
      httpResponseHandler.responseToClient(res, 200, responseReceipt);
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  async receiveFromReceipt (req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    const branchid = req.params.branchid;
    const receiptid = req.params.receiptid;
    try {
      await DatabaseCommand.beginTransaction(client);
      const targetReceipt = (await ReceiptServices.getReceipts([{id: receiptid}], thisUser.companycode, undefined, client))[0];
      if (!targetReceipt) {
        return httpResponseHandler.responseErrorToClient(res, 500, `Receipt ${receiptid} not found!`);
      }
      const receiptIngredientList = await ReceiptIngredientServices.getReceiptIngredients([{receiptid: targetReceipt.id}], thisUser.companycode, undefined, client);
      //add to repo
      await DatabaseCommand.commitTransaction(client);  
      httpResponseHandler.responseToClient(res, 200, receiptIngredientList);
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  async addIngredientToReceipt (req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    const receiptid = req.body.receiptid;
    const bodyRecIngre = req.body;
    bodyRecIngre.total = bodyRecIngre.price * bodyRecIngre.quantity;
    try {
      await DatabaseCommand.beginTransaction(client);
      const targetReceipt = (await ReceiptServices.getReceipts([{id: receiptid}], thisUser.companycode, undefined, client))[0];
      if (!targetReceipt) {
        return httpResponseHandler.responseErrorToClient(res, 500, `Receipt ${receiptid} not found!`);
      }
      await ReceiptIngredientServices.createReceiptIngredient(bodyRecIngre, thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
      } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  },
  async removeIngredientFromReceipt (req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    const bodyRecIngre = req.body as IReceiptIngredient;
    try {
      await DatabaseCommand.beginTransaction(client);
      const targetReceipt = (await ReceiptServices.getReceipts([{id: req.body.receiptid}], thisUser.companycode, undefined, client))[0];
      if (!targetReceipt) {
        return httpResponseHandler.responseErrorToClient(res, 500, `Receipt not found!`);
      }
      await ReceiptIngredientServices.removeOne(bodyRecIngre, thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      next();
      } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    } finally {
      client.release();
    }
  }
}
