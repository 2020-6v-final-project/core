import {Router} from "express";
import {passportJwt} from "../../../config/passport";
import checkUser from "../../../cors/checkUser";
import BranchReceiptController from "./BranchReceiptController";

export default function (router: Router) {
  router.route('/repositories/receipts/')
    .get(passportJwt, checkUser, BranchReceiptController.getReceipts)
    .post(passportJwt, checkUser, BranchReceiptController.createReceipt, BranchReceiptController.getReceipts);

  router.route('/repositories/receipts/ingredient/:receiptid')
    .post(passportJwt, checkUser, BranchReceiptController.addIngredientToReceipt, BranchReceiptController.getReceipts)
    .delete(passportJwt, checkUser, BranchReceiptController.removeIngredientFromReceipt, BranchReceiptController.getReceipts);
  
  router.route('/:branchid/receipts/:receiptid')
    .get(passportJwt, checkUser, BranchReceiptController.getReceiptById)
    .put(passportJwt, checkUser, BranchReceiptController.updateReceipt);

  // router.route('/:branchId/receipts/:receiptId')
  //   .get
  router.route('/:branchid/receipts/:receiptid/received')
    .post(passportJwt, checkUser, BranchReceiptController.receiveFromReceipt);
  router.route('/:branchid/receipts/:receiptid/request')
    .post(passportJwt, checkUser, BranchReceiptController.createRequest, BranchReceiptController.getReceipts);
}
