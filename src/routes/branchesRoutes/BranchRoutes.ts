import {Application, Router} from "express";
import {passportJwt} from "../../config/passport";
import Routes from "../../constant/Routes";
import checkUser from "../../cors/checkUser";
import BranchController from "./BranchController";
import BranchReceiptRoutes from "./receiptsRoutes/BranchReceiptRoutes";

const router: Router = Router();

export default (app: Application) => {
  router.route('/')
    .get(passportJwt, checkUser, BranchController.getBranches)
    .post(passportJwt, checkUser, BranchController.createBranch);

  router.route('/:branchId')
    .get(passportJwt, checkUser, BranchController.getBranch)
    .put(passportJwt, checkUser, BranchController.updateBranch, BranchController.getBranch);

  router.route("/:branchId/disabled")
    .post(passportJwt, checkUser, BranchController.markBranchAsDisabled, BranchController.getBranch);
  router.route("/:branchId/available")
    .post(passportJwt, checkUser, BranchController.markBranchAsAvailable, BranchController.getBranch);
  BranchReceiptRoutes(router);
  router.route('/menu/:branchId')
    // .get(passportJwt, checkUser, BranchController);
    .post(passportJwt, checkUser, BranchController.chooseBranchMenu);
  app.use(Routes.BRANCHES, router);
}
