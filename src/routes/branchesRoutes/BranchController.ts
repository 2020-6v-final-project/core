//when add a new branch, we will bind a User as manager, so should override the constraint between User and IRole in Users_Roles table.
import {NextFunction, Request, Response} from "express";
import DbSingleton from "../../config/database";
import httpResponseHandler from "../../cors/httpResponseHandler";
import { IBranch } from "../../models/IBranch";
import { IUser } from "../../models/public/IUser";
import BranchServices from "../../services/BranchServices";
import ItemServices from "../../services/ItemServices";
import MenuItemServices from "../../services/MenuItemServices";
import MenuServices from "../../services/MenuServices";
import CompanyServices from "../../services/public/CompanyServices";
import StatusServices from "../../services/public/StatusServices";
import DatabaseCommand from "../../utils/DatabaseCommand";
import TableServices from "../../services/TableServices";
import FinanceAccountServices from "../../services/FinanceAccountServices";
import {IRequestUser} from "../../models/temp/IRequestUser";
import RepositoryServices from "../../services/RepositoryServices";
import UserServices from "../../services/public/UserServices";
import RoleServices from "../../services/public/RoleServices";

export default {
  //create new branch
  createBranch: async (req: Request, res: Response) => {
    //Create branch with companyid is SAC companyid, code can be custom or by SAC companycode+number, assign manager.
    //Update manager user

    //body contains: code (custom), name, address, companyid, manager (custom)
    //check if that company is exist
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const body = req.body;
      delete body.id;
      delete body.statusid;
      delete body.statuscode;
      const thisUser = req.user as IRequestUser;
      let company = (await CompanyServices.getCompanies([{
        id: thisUser.companyid, statusid: 2
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client))[0];
      if (!company) return httpResponseHandler.responseErrorToClient(res, 404, `Company has id ${thisUser.companyid} not found or unavailable.`);
      company.numberofbranches++;
      if (company.numberofbranches > thisUser.businessContract.totalbranch) return httpResponseHandler.responseErrorToClient(res, 403, `Company has reached the total number of branches.`);
      await CompanyServices.updateCompanies(company, client);
      body.code = thisUser.companycode + "-" + body.code;
      if (body.mainmenu || body.mainmenu == 0) {
        const menu = (await MenuServices.getMenus([{
          id: body.mainmenu
        }], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client))[0];
        if (!menu) {
          await DatabaseCommand.rollbackTransaction(client);
          return httpResponseHandler.responseErrorToClient(res, 400, `Menu has id ${body.mainmenu} not found.`);
        }
      }
      const newBranch = await BranchServices.createBranch(body, thisUser.companycode, client);
      if (newBranch.manager || newBranch.manager == 0) {
        const user = (await UserServices.getUsers([{
          id: newBranch.manager, companyid: thisUser.companyid
        }], DatabaseCommand.lockStrength.KEY_SHARE, client))[0];
        if (!user) {
          await DatabaseCommand.rollbackTransaction(client);
          return httpResponseHandler.responseErrorToClient(res, 400, `User has id ${body.manager} not found.`);
        }
        await RoleServices.addRoleToUser(user, 'CA', client);
        await BranchServices.addUserToBranch(user, newBranch, client);
      }
      await Promise.all([
        FinanceAccountServices.createFinanceAccounts([{
          accounttype: 'CASH',
          branchid: newBranch.id
        }], thisUser.companycode, client),
        TableServices.createSequenceWithBranchId(newBranch.id, thisUser.companycode, client),
        RepositoryServices.createRepository({
          branchid: newBranch.id, name: newBranch.code,
        }, thisUser.companycode, client)
      ]);
      await DatabaseCommand.commitTransaction(client);
      httpResponseHandler.responseToClient(res, 201, newBranch);
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },

  //update information of existed branch
  updateBranch: async (req: Request, res: Response, next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    const branchId = parseInt(req.params.branchId);
    const body = req.body;
    delete body.id;
    delete body.numberofusers;
    delete body.statusid;
    delete body.statuscode;
    delete body.managerInfo;
    const thisUser = req.user as IRequestUser;
    try {
      await DatabaseCommand.beginTransaction(client);
      let branch = (await BranchServices.getBranches([{ id: branchId }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
      if (!branch) return httpResponseHandler.responseErrorToClient(res, 404, `Branch has id ${branchId} not found.`);
      if ((body.mainmenu || body.mainmenu == 0) && branch.mainmenu !== body.mainmenu) {
        const menu = (await MenuServices.getMenus([{
          id: body.mainmenu
        }], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client))[0];
        if (!menu) return httpResponseHandler.responseErrorToClient(res, 400, `Menu has id ${body.mainmenu} not found.`);
      }
      if ((body.manager || body.manager == 0) && branch.manager !== body.manager) {
        const user = (await UserServices.getUsers([{
          id: body.manager, companyid: thisUser.companyid
        }], DatabaseCommand.lockStrength.KEY_SHARE, client))[0];
        if (!user) return httpResponseHandler.responseErrorToClient(res, 400, `User has id ${body.manager} not found.`);
        await RoleServices.addRoleToUser(user, 'CA', client);
        await BranchServices.addUserToBranch(user, branch, client);
      }
      Object.assign(branch, body);
      const updatedBranch = await BranchServices.updateBranch(branch, thisUser.companycode, client);
      if (!updatedBranch) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },

  //get branches of company by id
  getBranches: async (req: Request, res: Response) => {
    const thisUser = req.user as IRequestUser;
    try {
      //find the companyid or code of logged in account -> User
      let branches: IBranch[];
      if (thisUser.validRole.isall) branches = await BranchServices.getBranches([], thisUser.companycode, undefined, undefined);
      else branches = await BranchServices.getBranches(thisUser.branchIds.map(id => ({id})), thisUser.companycode, undefined, undefined);

      if (typeof (req.query.withmanager) !== 'undefined') {
        branches = await Promise.all(branches.map(async branch => {
          const thisBranch = branch as IBranch & {
            managerInfo: IUser
          };
          thisBranch.managerInfo = (await UserServices.getUsers([{
            id: thisBranch.manager, companyid: thisUser.companyid
          }]))[0];
          return thisBranch;
        }))
      }
      //attach information of Manager or not ?
      httpResponseHandler.responseToClient(res, 200, branches);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },

  //get branch' detail by branch' id
  getBranch: async (req: Request, res: Response) => {
    //check thay branch belong to that SAC or not ???
    // console.log(req.params.companycode);
    const thisUser = req.user as IRequestUser;
    const branchId = parseInt(req.params.branchId);
    // if (thisUser.branchIds.includes(branchId) || )
    try {
      const branch = (await BranchServices.getBranches([{ id: branchId }], thisUser.companycode))[0] as any;
      if (!branch) return httpResponseHandler.responseErrorToClient(res, 404, `Branch has id ${branchId} not found.`);
      branch.managerInfo = (await UserServices.getUsers([{
        id: branch.manager, companyid: thisUser.companyid
      }]))[0];
      // if (thisUser.companyid !== targetBranch.companyid) return httpResponseHandler.responseErrorToClient(res, 403);
      //attach information of Manager or not ?
      // const listMenu = await MenuServices.getMenuDetail([{id: branch.mainmenu}], thisUser.companycode);
      // const targetMenu = listMenu[0];
      // if (!targetMenu) return httpResponseHandler.responseErrorToClient(res, 404, "Target IMenu not found!");
      httpResponseHandler.responseToClient(res, 200, branch);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  markBranchAsDisabled: async (req: Request, res: Response, next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    const branchId = parseInt(req.params.branchId);
    const thisUser = req.user as IRequestUser;
    try {
      await DatabaseCommand.beginTransaction(client);
      let branch = (await BranchServices.getBranches([{ id: branchId }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
      if (!branch) return httpResponseHandler.responseErrorToClient(res, 404, `Branch has id ${branchId} not found.`);
      await StatusServices.setStatusCodeToObjects(BranchServices.IBranchTableName, "DISABLED",[{
        id: branchId
      }], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },
  markBranchAsAvailable: async (req: Request, res: Response, next: NextFunction) => {
    const client = await DatabaseCommand.newClient();
    const branchId = parseInt(req.params.branchId);
    const thisUser = req.user as IRequestUser;
    try {
      await DatabaseCommand.beginTransaction(client);
      let branch = (await BranchServices.getBranches([{ id: branchId }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
      if (!branch) return httpResponseHandler.responseErrorToClient(res, 404, `Branch has id ${branchId} not found.`);
      await StatusServices.setStatusCodeToObjects(BranchServices.IBranchTableName, "AVAILABLE",[{
        id: branchId
      }], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },
  chooseBranchMenu: async (req: Request, res: Response) => {
    //flow: choose menu -> check Repo -> update BranchItem table
    const thisUser = req.user as IUser;
    const client = await DatabaseCommand.newClient();
    const listHasItem = [];
    const listNotHasItem = [];
    try {
      const listBranch = await BranchServices.getBranches([{id: parseInt(req.params.branchid)}], thisUser.companycode, undefined, client);
      let targetBranch = listBranch[0];
      if (!targetBranch) return httpResponseHandler.responseErrorToClient(res, 404, "Target IBranch not found!");
      targetBranch.mainmenu = parseInt(req.body.menuid);
      const branch = await BranchServices.updateBranch(targetBranch, thisUser.companycode, client);
      const listItem = await ItemServices.getItems([], thisUser.companycode, undefined, client);
      const listMenu = await MenuServices.getMenus([{id: parseInt(req.body.menuid)}], thisUser.companycode, undefined, client);
      const targetMenu = listMenu[0];
      if (!targetMenu) return httpResponseHandler.responseErrorToClient(res, 404, "Target IMenu not found!");
      const listItemInMenu = await MenuItemServices.getAllByMenuId(parseInt(req.body.menuid), thisUser.companycode, client, undefined);
      for (let item of listItem) {
        if (listItemInMenu.find(value => item.id === value.itemid)) {
          listHasItem.push(item);
        } else {
          listNotHasItem.push(item);
        }
      }
      const responseObject = {branch, targetMenu, listHasItem, listNotHasItem};
      await DatabaseCommand.commitTransaction(client);
      httpResponseHandler.responseToClient(res, 200, responseObject);
    }  catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 404, err.message);
    } finally {
      client.release();
    }
  }
};
