import {NextFunction, Request, Response} from "express";
import httpResponseHandler from "../../cors/httpResponseHandler";
import ReceiptTypeServices from "../../services/public/ReceiptTypeServices";
import {IUser} from "../../models/public/IUser";
import ItemServices from "../../services/ItemServices";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IReceiptType} from "../../models/public/IReceiptType";
import {IRequestUser} from "../../models/temp/IRequestUser";

export default {

  async getReceiptTypes(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    try {
      let receiptTypes = await ReceiptTypeServices.getReceiptTypes([]);
      // if (typeof (req.query.withitems) !== 'undefined') {
      //   receiptTypes = await Promise.all(receiptTypes.map(async receiptTypes => {
      //     const thisItemType = itemType as any;
      //     thisItemType.items = await ItemServices.getItems([{
      //       typeid: thisItemType.id
      //     }], thisUser.companycode);
      //     return thisItemType;
      //   }));
      // }
      return httpResponseHandler.responseToClient(res, 200, receiptTypes);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
}