import {Application, Router} from "express";
import {passportJwt} from "../../config/passport";
import checkUser from "../../cors/checkUser";
import ReceiptTypeController from "./ReceiptTypeController";
import Routes from "../../constant/Routes";

const router = Router();

export default function(app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, ReceiptTypeController.getReceiptTypes)
    

  app.use(Routes.RECEIPT_TYPES, router);
}
