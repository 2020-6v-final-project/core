import {Application, Router} from "express";
import CurrencyServices from "../../services/public/CurrencyServices";
import Routes from "../../constant/Routes";

const router = Router();

export default function(app: Application) {
  router.route('/')
    .get(async (req, res) => {
      const currencies = await CurrencyServices.getCurrencies([]);
      return res.status(200).json(currencies);
    });

  app.use(Routes.CURRENCIES, router);
}
