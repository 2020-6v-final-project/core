import { Application, Router } from 'express' 
import { passportJwt } from '../../config/passport';
import checkUser from '../../cors/checkUser';
import VendorController from './VendorController';
import Routes from "../../constant/Routes";
const router: Router = Router();
export default function (app: Application) {
    router.route('/')
        .get(passportJwt, checkUser, VendorController.getVendors)
        .post(passportJwt, checkUser, VendorController.createVendor, VendorController.getVendors);
    router.route('/:vendorid')
        .get(passportJwt, checkUser, VendorController.getVendorById)
        .put(passportJwt, checkUser, VendorController.updateVendor, VendorController.getVendors);
    router.route('/:vendorid/disable')
        .post(passportJwt, checkUser, VendorController.deactiveVendor);
    router.route('/:vendorid/activate')
        .post(passportJwt, checkUser, VendorController.activateVendor);
    app.use(Routes.VENDORS, router);
}