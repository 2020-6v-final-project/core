import { NextFunction, Request, Response } from "express";
import httpResponseHandler from "../../cors/httpResponseHandler";
import { IVendor } from "../../models/IVendor";
import { IRequestUser } from "../../models/temp/IRequestUser";
import StatusServices from "../../services/public/StatusServices";
import VendorServices from "../../services/VendorServices";
import VendorIngredientServices from "../../services/VendorIngredientServices";
import DatabaseCommand from "../../utils/DatabaseCommand";
import IngredientServices from "../../services/IngredientServices";
import { IVendorIngredient } from "../../models/IVendorIngredient";
import { nextTick } from "process";

export default {
    async createVendor(req: Request, res: Response, next: NextFunction) {
        const thisUser = req.user as IRequestUser;
        const client = await DatabaseCommand.newClient();
        const bodyVendor = req.body;
        const ingredientList = bodyVendor.ingredients as IVendorIngredient[];
        delete bodyVendor.ingredients;
        try {
            await DatabaseCommand.beginTransaction(client);
            const newVendor = await VendorServices.createVendor(bodyVendor, thisUser.companycode, client);
            const vendorIngredient = await Promise.all(ingredientList.map(async (item:IVendorIngredient) =>{
                item.vendorid = newVendor.id;
                await VendorIngredientServices.addOne(item, thisUser.companycode, client);
            }))
            await DatabaseCommand.commitTransaction(client);
            next();
        } catch (err) {
            console.log(err);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        } finally {
            client.release();
        }
    },
    async updateVendor(req: Request, res: Response, next: NextFunction) {
        const thisUser = req.user as IRequestUser;
        const client = await DatabaseCommand.newClient();
        const bodyVendor = req.body;
        const ingredientList = bodyVendor.ingredients as IVendorIngredient[];
        delete bodyVendor.id;
        delete bodyVendor.ingredients;
        const vendorid = req.params.vendorid as any;
        try {
            await DatabaseCommand.beginTransaction(client);
            const targetVendor = (await VendorServices.getVendors([{id: vendorid}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
            if (!targetVendor) {
                return httpResponseHandler.responseErrorToClient(res, 500, `Vendor ${vendorid} not found!`);
            }
            Object.assign(targetVendor, bodyVendor);
            const updatedVendor = await VendorServices.updateVendor(bodyVendor, thisUser.companycode, client);
            // Compare ingredientList diff
            const oldIngredientList = await VendorIngredientServices.getAllByVendorId(vendorid, thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client);
            for(let oldIngredient of oldIngredientList) {
                const containIndex = ingredientList?.findIndex(ingredient => ingredient.ingredientid === oldIngredient.ingredientid);
                if (containIndex > -1) {
                    ingredientList.splice(containIndex, 1);
                } else await VendorIngredientServices.removeOne(oldIngredient, thisUser.companycode, client);
            }
            await Promise.all(ingredientList.map(async ingredient => {
                await VendorIngredientServices.addOne({vendorid:vendorid,ingredientid:ingredient.ingredientid}, thisUser.companycode, client);
            }));
            await DatabaseCommand.commitTransaction(client);
            next();
        } catch (err) {
            console.log(err);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        } finally {
            client.release();
        }
    },
    async getVendors(req: Request, res: Response) {
        const thisUser = req.user as IRequestUser;
        try {
            const vendors = await VendorServices.getVendors([], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, undefined) as any;
            if (typeof (req.query.withingredients) !== "undefined") {
                const vendorIngredients = await VendorIngredientServices.getAllWithDetail([],thisUser.companycode,DatabaseCommand.lockStrength.KEY_SHARE,undefined);
                for (let a = 0; a < vendors.length; a++) {
                    const ingredientsOfVendor = vendorIngredients.filter(value => value.vendorid === vendors[a].id) as any;
                    ingredientsOfVendor.forEach((item:any)=>{
                        delete item.vendorid;
                        delete item.ingredientid;
                    })
                    vendors[a].ingredients = ingredientsOfVendor;
                }
            }
            httpResponseHandler.responseToClient(res, 200, vendors);
        } catch (err) { 
            console.log(err);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        }
    },
    async getVendorById(req: Request, res: Response) {
        const thisUser = req.user as IRequestUser;
        const vendorid = req.params.vendorid;
        try {
            const targetVendor = (await VendorServices.getVendors([{id: vendorid}], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, undefined))[0];
            if (!targetVendor) {
                return httpResponseHandler.responseErrorToClient(res, 500, `Vendor ${vendorid} not found!`);
            }
            httpResponseHandler.responseToClient(res, 200, targetVendor);
        } catch (err) {
            console.log(err);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        }
    },
    async deactiveVendor(req: Request, res: Response) {
        const thisUser = req.user as IRequestUser;
        const client = await DatabaseCommand.newClient();
        const vendorid = req.params.vendorid;
        try {
            await DatabaseCommand.beginTransaction(client);
            const targetVendor = (await VendorServices.getVendors([{id: vendorid}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
            if (!targetVendor) {
                return httpResponseHandler.responseErrorToClient(res, 500, `Vendor ${vendorid} not found!`);
            }
            const updateVendor = await StatusServices.setStatus(targetVendor, `${thisUser}.${VendorServices.IVendorTableName}`, "DISABLED", client);
            await DatabaseCommand.commitTransaction(client);
            httpResponseHandler.responseToClient(res, 200, updateVendor);
        } catch (err) {
            console.log(err);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        } finally {
            client.release();
        }
    },
    async activateVendor(req: Request, res: Response) {
        const thisUser = req.user as IRequestUser;
        const client = await DatabaseCommand.newClient();
        const vendorid = req.params.vendorid;
        try {
            await DatabaseCommand.beginTransaction(client);
            const targetVendor = (await VendorServices.getVendors([{id: vendorid}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
            if (!targetVendor) {
                return httpResponseHandler.responseErrorToClient(res, 500, `Vendor ${vendorid} not found!`);
            }
            const updateVendor = await StatusServices.setStatus(targetVendor, `${thisUser}.${VendorServices.IVendorTableName}`, "AVAILABLE", client);
            await DatabaseCommand.commitTransaction(client);
            httpResponseHandler.responseToClient(res, 200, updateVendor);
        } catch (err) {
            console.log(err);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        } finally {
            client.release();
        }
    }
}