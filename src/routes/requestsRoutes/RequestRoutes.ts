import { Application, Router } from "express";
import { passportJwt } from "../../config/passport";
import Routes from "../../constant/Routes";
import checkUser from "../../cors/checkUser";
import RequestController from "./RequestController";

const router: Router = Router();

export default function (app: Application) {
    router.route('/')
        .get(passportJwt, checkUser, RequestController.getRequest)
        .post(passportJwt, checkUser, RequestController.createRequest);
    router.route('/:requestid/approved')
        .post(passportJwt, checkUser, RequestController.approveRequest,RequestController.getRequest);
    router.route('/:requestid/rejected')
        .post(passportJwt, checkUser, RequestController.rejectRequest,RequestController.getRequest);
    app.use(Routes.REQUESTS, router);
}