import { NextFunction, Request, Response } from "express";
import httpResponseHandler from "../../cors/httpResponseHandler";
import { IRequestUser } from "../../models/temp/IRequestUser"
import CheckServices from "../../services/CheckServices";
import StatusServices from "../../services/public/StatusServices";
import ReceiptServices from "../../services/ReceiptServices";
import RequestServies from "../../services/RequestServies";
import DatabaseCommand from "../../utils/DatabaseCommand";

export default {
    async createRequest(req: Request, res: Response) {
        const thisUser = req.user as IRequestUser;
        const client = await DatabaseCommand.newClient();
        const bodyRequest = req.body; 
        bodyRequest.createdby = thisUser.id;
        bodyRequest.createdat = new Date();
        try {
            await DatabaseCommand.beginTransaction(client);
            const newRequest = await RequestServies.createRequest(bodyRequest, thisUser.companycode, client);
            await DatabaseCommand.commitTransaction(client);
            httpResponseHandler.responseToClient(res, 200, newRequest);
        } catch (err) {
            console.log(err);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        } finally {
            client.release();
        }
    },
    async approveRequest(req: Request, res: Response,next: NextFunction) {
        //create check for receipt -> create request for this check
        const thisUser = req.user as IRequestUser;
        const bodyRequest = req.body;
        const client = await DatabaseCommand.newClient();
        const requestId = req.params.requestid;
        let updateFlexibleObject;
        let updateRequest;
        try {
            await DatabaseCommand.beginTransaction(client);
            //find request
            const targetRequest = (await RequestServies.getRequests([{id: requestId}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
            if (!targetRequest) {
                return httpResponseHandler.responseErrorToClient(res, 500, `Request ${requestId} not found!`);
            }
            if (targetRequest.statusid === 18) {
                return httpResponseHandler.responseErrorToClient(res, 500, `Cannot access Request ${requestId}`);
            }
            //find request reference
            switch (targetRequest.requesttypeid) {
                case 1:
                    //change menu
                    break;
                case 2:
                    //export receipt
                    break;
                case 3:
                    //import receipt
                    const targetReceipt = (await ReceiptServices.getReceipts([{id: targetRequest.referenceid}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
                    if (!targetReceipt) {
                        return httpResponseHandler.responseErrorToClient(res, 500, `Receipt ${targetRequest.referenceid} not found!`);
                    }
                    const foundCheck = (await CheckServices.getChecks([{receiptid: targetReceipt.id}], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client))[0];
                    if (foundCheck && foundCheck.statuscode !== "REJECTED") {
                        return httpResponseHandler.responseErrorToClient(res, 500, `Existed Check for Receipt ${targetReceipt.id}`);
                    }
                    const check = {accountid: 1, createdat: new Date(), receiptid: targetReceipt.id, createdby: thisUser.id, oldbalance: 0, debitamount: 0};
                    const newCheck = await CheckServices.createCheck(check, thisUser.companycode, client);
                    const requestForCheck = await RequestServies.createRequest({requesttypeid: 4, createdby: thisUser.id, referenceid: newCheck.id, createdat: new Date()}, thisUser.companycode, client);
                    targetReceipt.statusid = 20;
                    targetReceipt.statuscode = "PENDINGPAYMENT";
                    targetReceipt.approvedrequestid = targetRequest.id;
                    updateFlexibleObject = await ReceiptServices.updateReceipts(targetReceipt, [{id: targetReceipt.id}], thisUser.companycode, client);
                    break;
                case 4:
                    //check
                    const targetCheck = (await CheckServices.getChecks([{id: targetRequest.referenceid}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
                    if (!targetCheck) {
                        return httpResponseHandler.responseErrorToClient(res, 500, `Check ${targetRequest.referenceid} not found!`);
                    }                    
                    const targetReceiptCheck = (await ReceiptServices.getReceipts([{id: targetCheck.receiptid}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
                    if (!targetReceiptCheck) {
                        return httpResponseHandler.responseErrorToClient(res, 500, `Receipt ${targetCheck.receiptid} not found!`);
                    }
                    //any account finance here 
                    targetCheck.approvedrequestid = targetRequest.id;
                    targetCheck.statuscode = "DONE";
                    targetCheck.statusid = 11,
                    await CheckServices.updateChecks(targetCheck, [{id: targetCheck.id}], thisUser.companycode, client);
                    targetReceiptCheck.referenceid = targetCheck.id;
                    targetReceiptCheck.statusid = 21;
                    targetReceiptCheck.statuscode = "PENDINGRECEIVED";
                    updateFlexibleObject = await ReceiptServices.updateReceipts(targetReceiptCheck, [{id: targetReceiptCheck.id}], thisUser.companycode, client);
                    break;
                default: 
                    break;
            }
            targetRequest.approvedat = new Date();
            targetRequest.approvedby = thisUser.id;
            targetRequest.statuscode = "APPROVED";
            targetRequest.statusid = 18;
            targetRequest.note = bodyRequest.note;
            // updateRequest = await StatusServices.setStatus(targetRequest, `${thisUser.companycode}.${RequestServies.IRequestTableName}`, "APPROVED", client);
            updateRequest = await RequestServies.updateRequest(targetRequest, thisUser.companycode, client);
            await DatabaseCommand.commitTransaction(client);
            next();
            } catch (err) {
            console.log(err);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        } finally {
            client.release();
        }
    },
    async getRequest(req: Request, res: Response) {        
        const thisUser = req.user  as IRequestUser;
        try {
            const listRequests = await RequestServies.getRequests([], thisUser.companycode, DatabaseCommand.lockStrength.NO_KEY_UPDATE, undefined);
            httpResponseHandler.responseToClient(res, 200, listRequests);
        } catch (err) {
            console.log(err);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        }
    },
    async updateRequest(req: Request, res: Response) {

    },
    async rejectRequest(req: Request, res: Response, next: NextFunction ) {
        const thisUser = req.user as IRequestUser;
        const client = await DatabaseCommand.newClient();
        const requestId = req.params.requestid;
        let updateRequest;
        //FlexibleObject can be: check, menu, receipt
        let updateFlexibleObject;
        try {
            await DatabaseCommand.beginTransaction(client);
            const targetRequest = (await RequestServies.getRequests([{id: requestId}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
            if (!targetRequest) {
                return httpResponseHandler.responseErrorToClient(res, 500, `Request ${requestId} not found!`);
            }
            switch (targetRequest.requesttypeid) {
                case 1:
                    //change menu 
                    break;
                case 2:
                    //export receipt
                    break;
                case 3:
                    //import receipt
                    //receiptis id request.referenceid
                    const targetReceipt = (await ReceiptServices.getReceipts([{id: targetRequest.referenceid}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
                    if (!targetReceipt) {
                        return httpResponseHandler.responseErrorToClient(res, 500, `Receipt ${targetRequest.referenceid} not found!`)
                    }
                    targetReceipt.approvedrequestid = targetRequest.id;
                    targetReceipt.statusid = 19;
                    targetReceipt.statuscode = "REJECTED";
                    updateFlexibleObject = await ReceiptServices.updateReceipt(targetReceipt, thisUser.companycode, client);
                    // updateFlexibleObject = await StatusServices.setStatus(targetReceipt, `${thisUser.companycode}.${ReceiptServices.IReceiptTableName}`, "REJECTED", client);
                    break;
                case 4:
                    //check
                    const targetCheck = (await CheckServices.getChecks([{id: targetRequest.referenceid}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
                    if (!targetCheck) {
                        return httpResponseHandler.responseErrorToClient(res, 500, `Check ${targetRequest.referenceid} not found!`);
                    }
                    const targetCheckReceipt = (await ReceiptServices.getReceipts([{id: targetCheck.receiptid}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
                    if (!targetCheckReceipt) {
                        return httpResponseHandler.responseErrorToClient(res, 500, `Receipt ${targetCheck.receiptid} not found!`);
                    }
                    targetCheck.approvedrequestid = targetRequest.id;
                    targetCheck.statusid = 19;
                    targetCheck.statuscode = "REJECTED";
                    updateFlexibleObject = await CheckServices.updateChecks(targetCheck, [{id: targetCheck.id}], thisUser.companycode, client);
                    break;
                default: 
                    break;
            }
            targetRequest.statuscode = "REJECTED";
            targetRequest.statusid = 19;
            targetRequest.approvedat = new Date();
            targetRequest.approvedby = thisUser.id;
            // updateRequest = await StatusServices.setStatus(targetRequest, `${thisUser.companycode}.${RequestServies.IRequestTableName}`, "REJECTED", client);
            updateRequest = await RequestServies.updateRequest(targetRequest, thisUser.companycode, client);
            await DatabaseCommand.commitTransaction(client);
            next();
            } catch (err) {
            console.log(err);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        } finally {
            client.release();
        }
    },
    
}