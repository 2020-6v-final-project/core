import {Application, Router} from "express";
import Routes from "../../constant/Routes";
import {passportJwt} from "../../config/passport";
import checkUser from "../../cors/checkUser";
import OptionController from "./OptionController";

const router = Router();

export default function (app: Application) {

  router.route('/')
    .get(passportJwt, checkUser, OptionController.getOptions)
    .post(passportJwt, checkUser, OptionController.createOption);

  router.route('/:optionId')
    .put(passportJwt, checkUser, OptionController.updateOption, OptionController.getOptions)
    .delete(passportJwt, checkUser, OptionController.deleteOption);

  app.use(Routes.ITEM_OPTIONS, router);
}
