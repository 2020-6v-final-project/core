import {NextFunction, Request, Response} from "express";
import httpResponseHandler from "../../cors/httpResponseHandler";
import OptionServices from "../../services/OptionServices";
import {IRequestUser} from "../../models/temp/IRequestUser";
import DatabaseCommand from "../../utils/DatabaseCommand";

export default {

	async getOptions(req: Request, res: Response) {
		const thisUser = req.user as IRequestUser;
		try {
			const options = await OptionServices.getOptions([], thisUser.companycode);
			return httpResponseHandler.responseToClient(res, 200, options);
		} catch (e) {
			console.log(e);
			return httpResponseHandler.responseErrorToClient(res, 500, e.message);
		}
	},
	async createOption(req: Request, res: Response) {
		const thisUser = req.user as IRequestUser;
		const body = req.body as {
			name: string;
		}
		const client = await DatabaseCommand.newClient();
		try {
			await DatabaseCommand.beginTransaction(client);
			const createdOption = await OptionServices.createOptions([{
				name: body.name
			}], thisUser.companycode, client);
			if (!createdOption) {
				await DatabaseCommand.rollbackTransaction(client);
				return httpResponseHandler.responseErrorToClient(res, 400);
			}
			await DatabaseCommand.commitTransaction(client);
			return httpResponseHandler.responseToClient(res, 201, createdOption);
		} catch (e) {
			console.log(e);
			await DatabaseCommand.rollbackTransaction(client);
			return httpResponseHandler.responseErrorToClient(res, 500, e.message);
		} finally {
			client.release();
		}
	},
	async deleteOption(req: Request, res: Response) {
		const optionId = parseInt(req.params.optionId);
		const thisUser = req.user as IRequestUser;
		const client = await DatabaseCommand.newClient();
		try {
			await DatabaseCommand.beginTransaction(client);
			const option = (await OptionServices.getOptions([{
				id: optionId
			}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
			if (!option) return httpResponseHandler.responseErrorToClient(res, 404, `Option has id ${optionId} not found.`);
			const deletedOption = (await OptionServices.deleteOptions([{
				id: optionId
			}], thisUser.companycode, client))[0];
			if (!deletedOption) {
				await DatabaseCommand.rollbackTransaction(client);
				return httpResponseHandler.responseErrorToClient(res, 400);
			}
			await DatabaseCommand.commitTransaction(client);
			return httpResponseHandler.responseToClient(res, 202);
		} catch (e) {
			console.log(e);
			await DatabaseCommand.rollbackTransaction(client);
			return httpResponseHandler.responseErrorToClient(res, 500, e.message);
		} finally {
			client.release();
		}
	},
	async updateOption(req: Request, res: Response, next: NextFunction) {
		const optionId = parseInt(req.params.optionId);
		const thisUser = req.user as IRequestUser;
		const body = req.body as {
			name: string;
		}
		const client = await DatabaseCommand.newClient();
		try {
			await DatabaseCommand.beginTransaction(client);
			const option = (await OptionServices.getOptions([{
				id: optionId
			}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
			if (!option) return httpResponseHandler.responseErrorToClient(res, 404, `Option has id ${optionId} not found.`);
			const updatedOption = (await OptionServices.updateOptions({
				name: body.name
			}, [{
				id: optionId
			}], thisUser.companycode, client));
			if (!updatedOption) {
				await DatabaseCommand.rollbackTransaction(client);
				return httpResponseHandler.responseErrorToClient(res, 400);
			}
			await DatabaseCommand.commitTransaction(client);
			return next();
		} catch (e) {
			console.log(e);
			await DatabaseCommand.rollbackTransaction(client);
			return httpResponseHandler.responseErrorToClient(res, 500, e.message);
		} finally {
			client.release();
		}
	}
}
