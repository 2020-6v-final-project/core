import { Request, Response } from "express";
import httpResponseHandler from "../../cors/httpResponseHandler";
import { IRequestUser } from "../../models/temp/IRequestUser";
import CheckServices from "../../services/CheckServices";
import RequestServies from "../../services/RequestServies";
import DatabaseCommand from "../../utils/DatabaseCommand";

export default {
    async createCheck(req: Request, res: Response) {
        //create a check -> create a request
        //state of check will be changed by handling request reference to this check 
        const thisUser = req.user as IRequestUser;
        const client = await DatabaseCommand.newClient();
        const bodyCheck = req.body;
        try {
            await DatabaseCommand.beginTransaction(client);
            const newCheck =  await CheckServices.createCheck(bodyCheck, thisUser.companycode, client);
            const request = {requesttypeid: 4, createdby: thisUser.id, createdat: new Date(), referenceid: newCheck.id};
            const newRequest = await RequestServies.createRequest(request, thisUser.companycode, client);
            await DatabaseCommand.commitTransaction(client);
            httpResponseHandler.responseToClient(res, 200, {newCheck, newRequest});
        } catch (err) {
            console.log(err);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        } finally {
            client.release();
        }
    },
    async updateCheck(req: Request, res: Response) {
        const thisUser = req.user as IRequestUser;
        const client = await DatabaseCommand.newClient();
        const checkid = req.params.checkid;
        const bodyCheck = req.body;
        delete bodyCheck.id;
        delete bodyCheck.statusid;
        delete bodyCheck.statuscode;
        try {
            await DatabaseCommand.beginTransaction(client);
            const targetCheck = (await CheckServices.getChecks([{id: checkid}], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
            if (!targetCheck) {
                return httpResponseHandler.responseErrorToClient(res, 500, `Check ${checkid} not found!`);
            }
            if (targetCheck.statuscode) {
                return httpResponseHandler.responseErrorToClient(res, 500, `Cannot access Check ${checkid}`);
            }
            Object.assign(targetCheck, bodyCheck);
            const updatedCheck = await CheckServices.updateCheck(targetCheck, thisUser.companycode, client);
            await DatabaseCommand.commitTransaction(client);
            httpResponseHandler.responseToClient(res, 200, updatedCheck);

        } catch (err) {
            console.log(err);
            await DatabaseCommand.rollbackTransaction(client);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        } finally {
            client.release();
        }
    },
    async getChecks(req: Request, res: Response) {
        const thisUser = req.user as IRequestUser;
        try {
            const Checks = await CheckServices.getChecks([], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, undefined);
            httpResponseHandler.responseToClient(res, 200, Checks);

        } catch (err) {
            console.log(err);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        }
    },
    async getCheckById(req: Request, res: Response) {
        const thisUser = req.user as IRequestUser;
        const checkid = req.params.checkid;
        try {
            const targetCheck = (await CheckServices.getChecks([{id: checkid}], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, undefined))[0];
            if (!targetCheck) {
                return httpResponseHandler.responseErrorToClient(res, 500, `Check ${checkid} not found!`);
            }
            httpResponseHandler.responseToClient(res, 200, targetCheck);
        } catch (err) {
            console.log(err);
            httpResponseHandler.responseErrorToClient(res, 500, err.message);
        }
    }
}