import { Application, Router } from "express";
import { passportJwt } from "../../config/passport";
import Routes from "../../constant/Routes";
import checkUser from "../../cors/checkUser";
import CheckController from "./CheckController";

const router: Router = Router();

export default function(app: Application) {
    router.route('/')
        .get(passportJwt, checkUser, CheckController.getChecks)
        .post(passportJwt, checkUser, CheckController.createCheck);
    router.route('/:checkid')   
        .get(passportJwt, checkUser, CheckController.getCheckById)
        .put(passportJwt, checkUser, CheckController.updateCheck);
    router.use(Routes.CHECKS, app);
}