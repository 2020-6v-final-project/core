import {Application, Router} from "express";
import {passportJwt} from "../../config/passport";
import checkUser from "../../cors/checkUser";
import IngredientTypeController from "./IngredientTypeController";
import Routes from "../../constant/Routes";

const router = Router();

export default function(app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, IngredientTypeController.getIngredientTypes)
    .post(passportJwt, checkUser, IngredientTypeController.createIngredientType, IngredientTypeController.getIngredientType);

  router.route('/:typeId')
    .get(passportJwt, checkUser, IngredientTypeController.getIngredientType)
    .put(passportJwt, checkUser, IngredientTypeController.updateIngredientType, IngredientTypeController.getIngredientType)
    .delete(passportJwt, checkUser, IngredientTypeController.deleteIngredientType);

  app.use(Routes.INGREDIENT_TYPES, router);
}
