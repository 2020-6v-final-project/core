import {NextFunction, Request, Response} from "express";
import httpResponseHandler from "../../cors/httpResponseHandler";
import IngredientTypeServices from "../../services/IngredientTypeServices";
import {IUser} from "../../models/public/IUser";
import DatabaseCommand from "../../utils/DatabaseCommand";
import IngredientServices from "../../services/IngredientServices";
import {IRequestUser} from "../../models/temp/IRequestUser";

export default {

  async getIngredientTypes(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    try {
      let ingredientTypes = await IngredientTypeServices.getIngredientTypes([], thisUser.companycode);
      if (typeof (req.query.withingredients) !== 'undefined') {
        ingredientTypes = await Promise.all(ingredientTypes.map(async ingredientType => {
          const thisIngredientType = ingredientType as any;
          thisIngredientType.ingredients = await IngredientServices.getIngredients([{
            typeid: thisIngredientType.id
          }], thisUser.companycode);
          return thisIngredientType;
        }));
      }
      return httpResponseHandler.responseToClient(res, 200, ingredientTypes);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async createIngredientType(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const body = req.body;
    delete body.id;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const type = await IngredientTypeServices.addIngredientType(body, thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      req.params.typeId = type.id.toString();
      next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async getIngredientType(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    const typeId = parseInt(req.params.typeId);
    try {
      let ingredientTypes = await IngredientTypeServices.getIngredientTypes([{
        id: typeId
      }], thisUser.companycode);
      if (!ingredientTypes.length) return httpResponseHandler.responseErrorToClient(res, 404, `Ingredient type has id ${typeId} not found.`);
      const ingredientType = ingredientTypes[0] as any;
      ingredientType.ingredients = await IngredientServices.getIngredients([{
        typeid: ingredientType.id
      }], thisUser.companycode);
      return httpResponseHandler.responseToClient(res, 200, ingredientType);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async updateIngredientType(req: Request, res: Response, next: NextFunction) {
    const thisUser = req.user as IRequestUser;
    const typeId = parseInt(req.params.typeId);
    const body = req.body;
    delete body.id;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const type = (await IngredientTypeServices.getIngredientTypes([{
        id: typeId
      }], thisUser.companycode, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client))[0];
      if (!type) return httpResponseHandler.responseErrorToClient(res, 404, `Ingredient type has id ${typeId} not found.`);
      Object.assign(type, body);
      const updatedType = (await IngredientTypeServices.updateIngredientTypes(type, [{
        id: typeId
      }], thisUser.companycode, client))[0];
      if (!updatedType) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async deleteIngredientType(req: Request, res: Response) {
    const thisUser = req.user as IRequestUser;
    const typeId = parseInt(req.params.typeId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const type = (await IngredientTypeServices.getIngredientTypes([{
        id: typeId
      }], thisUser.companycode, DatabaseCommand.lockStrength.UPDATE, client))[0];
      if (!type) return httpResponseHandler.responseErrorToClient(res, 404, `Ingredient type has id ${typeId} not found.`);
      const dependencyIngredients = await IngredientServices.getIngredients([{
        typeid: typeId
      }], thisUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (dependencyIngredients.length) return httpResponseHandler.responseErrorToClient(res, 400, `Please delete ingredients of type first.`);
      await IngredientTypeServices.deleteIngredientTypes([{
        id: typeId
      }], thisUser.companycode, client);
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 202);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
