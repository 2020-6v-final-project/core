import DbSingleton from "../config/database";
import { PoolClient } from "pg";

const db = DbSingleton.getInstance();

const prepareConditionString = function (condition: object[], valueStart: number = 1): { nextValueNum: number; conditionString: string; parameters: any[] } {
  let result = '';
  let valueNum = valueStart;
  let values: any[] = [];
  const conditionLength = condition.length;
  for (let i = 0; i < conditionLength; i++) {
    const object = condition[i];
    const keys = Object.keys(object);
    const keysCount = keys.length;
    values.push(...Object.values(object));
    for (let j = 0; j < keysCount; j++) {
      result += `(${keys[j]}=$${valueNum + j})`;
      if (j !== keysCount - 1) result += ' AND '
    }
    valueNum += keysCount;
    if (i !== conditionLength - 1) result += ' OR '
  }
  return {
    conditionString: result,
    parameters: [...values],
    nextValueNum: valueNum
  };
};

const prepareObjectString = function (object: object, valueStart: number = 1): { nextValueNum: number; objectString: string; parameters: any[] } {
  let result = '';
  let valueNum = valueStart;
  const keys = Object.keys(object);
  const keysCount = keys.length;
  for (let i = 0; i < keysCount; i++) {
    result += `${keys[i]}=$${valueNum + i}`;
    if (i !== keysCount - 1) result += ", ";
  }
  valueNum += keysCount;
  return {
    objectString: result,
    parameters: [...Object.values(object)],
    nextValueNum: valueNum
  };
}

export default {
  insert: async function (object: any, tableName: string, inputClient?: PoolClient): Promise<object> {
    const keys = Object.keys(object);
    let query = `INSERT INTO ${tableName} (${keys.join(',')}) VALUES (`;
    for (let i = 0; i < keys.length; i++) {
      query += `$${i + 1}`;
      if (i !== keys.length - 1) query += ',';
    }
    const result = await db.query(query + `) RETURNING *`, Object.values(object), inputClient);
    return result.rows[0];
  },
  select: async function (columns: string[], tableName: string, condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<object[]> {
    let query = `SELECT ${columns.join(',')} FROM ${tableName}`;
    const params = [];
    if (condition.length) {
      let prepareConditionResult = prepareConditionString(condition);
      query += ' WHERE ' + prepareConditionResult.conditionString;
      params.push(...prepareConditionResult.parameters);
    }
    if (lockStrength) query += ` FOR ${lockStrength}`;
    const result = await db.query(query, params, inputClient);
    return result.rows;
  },
  update: async function (object: any, tableName: string, condition: object[], inputClient?: PoolClient): Promise<object[]> {
    let query = `UPDATE ${tableName} SET`;
    const params = [];
    let prepareObjectResult = prepareObjectString(object);
    query += ' ' + prepareObjectResult.objectString;
    params.push(...prepareObjectResult.parameters);
    if (condition.length) {
      let prepareConditionResult = prepareConditionString(condition, prepareObjectResult.nextValueNum);
      query += ' WHERE ' + prepareConditionResult.conditionString;
      params.push(...prepareConditionResult.parameters);
    }
    const result = await db.query(query + ` RETURNING *`, params, inputClient);
    return result.rows;
  },
  delete: async function (tableName: string, condition: object[], inputClient?: PoolClient): Promise<object[]> {
    let query = `DELETE FROM ${tableName}`;
    const params = [];
    if (condition.length) {
      let prepareConditionResult = prepareConditionString(condition);
      query += ' WHERE ' + prepareConditionResult.conditionString;
      params.push(...prepareConditionResult.parameters);
    }
    const result = await db.query(query + ` RETURNING *`, params, inputClient);
    return result.rows;
  },
  customQuery: async function (query: string, params: any[], inputClient?: PoolClient): Promise<any> {
    return await db.query(query, params, inputClient);
  },
  beginTransaction: async function (inputClient: PoolClient): Promise<any> {
    return await db.query('BEGIN TRANSACTION', undefined, inputClient);
  },
  commitTransaction: async function (inputClient: PoolClient): Promise<any> {
    return await db.query('COMMIT TRANSACTION', undefined, inputClient);
  },
  rollbackTransaction: async function (inputClient: PoolClient): Promise<any> {
    return await db.query('ROLLBACK TRANSACTION', undefined, inputClient);
  },
  newClient: async function (): Promise<PoolClient> {
    return await db.getPool().connect();
  },
  async createSequenceForId(type: string, prefix: string, inputClient?: PoolClient) {
    if (type !== 'bigint') type = 'integer';
    const query = `CREATE SEQUENCE ${prefix}_id_seq
    AS ${type}
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1`
    await db.query(query, undefined, inputClient);
    return `${prefix}_id_seq`;
  },
  async getNextSequenceId(sequenceName: string, inputClient?: PoolClient): Promise<number> {
    const query = `SELECT nextval('${sequenceName}')`;
    const result = await db.query(query, undefined, inputClient);
    return result.rows[0].nextval as number;
  },
  async dropSequenceForId(prefix: string, inputClient?: PoolClient) {
    const query = `DROP SEQUENCE ${prefix}_id_seq`;
    await db.query(query, undefined, inputClient);
  },
  lockStrength: {
    UPDATE: 'UPDATE',
    NO_KEY_UPDATE: 'NO KEY UPDATE',
    SHARE: 'SHARE',
    KEY_SHARE: 'KEY SHARE'
  }
};
