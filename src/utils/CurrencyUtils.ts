import axios from 'axios';
import * as dotenv from "dotenv";
dotenv.config({ path: ".env" });

const convertCurrency = async (amount: number, fromCurrency: string, toCurrency: string, cb: Function) => {
    var apiKey = process.env.CURRENCY_CONVERTER_API_KEY;

    fromCurrency = encodeURIComponent(fromCurrency);
    toCurrency = encodeURIComponent(toCurrency);
    var query = fromCurrency + '_' + toCurrency;

    var url = 'https://free.currconv.com/api/v7/convert?q='
        + query + '&compact=ultra&apiKey=' + apiKey;

    await axios.get(url).then(response => {
        try {
            var jsonObj = response.data;
            var val = jsonObj[query];
            if (val) {
                var total = val * amount;
                cb(null, Math.round(total * 100) / 100);
            } else {
                var err = new Error("Value not found for " + query);
                console.log(err);
            }
        } catch (e) {
            console.log("Parse error: ", e);
            cb(e);
        }
    }).catch(function (error) {
        console.log(error);
    })
}

const getExchangeRate = async (fromCurrency: string, toCurrency: string) => {
    try {
        let x: number = 1;
        await convertCurrency(1, fromCurrency, toCurrency, (err: Error, amount: number) => {
            x = amount;
        });
        return 1 / x;
    } catch (error) {
        console.log(error);
    }
    return null;
}
export { convertCurrency, getExchangeRate };

//uncomment to test

// async function testGetExchangeRate(params: void) {
//     const rate = await getExchangeRate('USD', 'VND');
//     if (rate) console.log(rate);
//     else console.log("Error.....");

// }
// testGetExchangeRate();

