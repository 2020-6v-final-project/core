import * as storage from '@azure/storage-blob';
import * as dotenv from "dotenv";
dotenv.config({ path: ".env" });
const accountName = "cookit6v";
const key = process.env.STORAGE_ACCESS_KEY;
const signedPermissions = 'rwdlac';
const signedService = 'b';
const signedResourceType = 'sco';
const signedVersion = '2020-02-10';
const cerds = new storage.StorageSharedKeyCredential(accountName, key!);

const generateSASToken = () => {
    let startDate = new Date();
    let expiryDate = new Date();
    startDate.setTime(startDate.getTime() - 15 * 60 * 1000);
    expiryDate.setTime(expiryDate.getTime() + 15 * 60 * 1000);

    let sasToken = storage.generateAccountSASQueryParameters({
        expiresOn: expiryDate,
        permissions: storage.AccountSASPermissions.parse(signedPermissions),
        protocol: storage.SASProtocol.Https,
        resourceTypes: storage.AccountSASResourceTypes.parse(signedResourceType).toString(),
        services: storage.AccountSASServices.parse(signedService).toString(),
        startsOn: startDate,
        version: signedVersion
    }, cerds).toString();
    return sasToken;
}

export { generateSASToken };