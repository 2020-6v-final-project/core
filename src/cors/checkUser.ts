import {Request, Response, NextFunction} from "express";
import httpResponseHandler from "./httpResponseHandler";
import DatabaseCommand from "../utils/DatabaseCommand";
import UserBranchServices from "../services/UserBranchServices";
import {IRequestUser} from "../models/temp/IRequestUser";
import AuthenticationServices from "../services/commonServices/AuthenticationServices";
import RoleServices from "../services/public/RoleServices";

/**
 * This middleware check user for specific route
 * @param req
 * @param res
 * @param next
 */
export default async function checkUser(req: Request, res: Response, next: NextFunction) {
  try {
    if (!req.user) return httpResponseHandler.responseErrorToClient(res, 400);
    let requestUser = req.user as IRequestUser;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const roles = await RoleServices.getRolesOfUser(requestUser, DatabaseCommand.lockStrength.KEY_SHARE, client);
      roles.sort((a, b) => +b.isall - +a.isall);
      // const canAccessApiRole = await AuthenticationServices.checkPermissionReturnRole(requestUser, roles, requestUser.businessContract.typeid, req.originalUrl, req.method, 'CORE', client);
      const canAccessApiRole = roles[0];
      if (canAccessApiRole) {
        requestUser.validRole = canAccessApiRole;
        requestUser.branchIds = (await UserBranchServices.getUserBranches([{
          userid: requestUser.id
        }], requestUser.companycode, DatabaseCommand.lockStrength.KEY_SHARE, client)).map(item => item.branchid);
        req.user = requestUser;
        return next();
      }
      return httpResponseHandler.responseErrorToClient(res, 403, `You do not have permission to access this api.`);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  } catch (e) {
    console.log(e);
    return httpResponseHandler.responseErrorToClient(res, 500);
  }
}
