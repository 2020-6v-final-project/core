import { PoolClient } from "pg";
import { ITable } from "../models/ITable";
import DatabaseCommand from "../utils/DatabaseCommand";

const ITableTableName = "tables";
export default {
    ITableTableName,
    async createSequenceWithBranchId(branchId: number, schema: string, inputClient?: PoolClient) {
        return await DatabaseCommand.createSequenceForId('integer', `${schema}.${ITableTableName}${branchId}`, inputClient);
    },
    async dropSequenceWithCompanyId(branchId: number, schema: string, inputClient?: PoolClient) {
        await DatabaseCommand.dropSequenceForId(`${schema}.${ITableTableName}${branchId}`, inputClient);
    },
    async nextId(branchId: number, schema: string, inputClient?: PoolClient): Promise<number> {
        return await DatabaseCommand.getNextSequenceId(`${schema}.${ITableTableName}${branchId}_id_seq`, inputClient);
    },
    async createTable(table: any, schema: string, inputClient?: PoolClient): Promise<ITable> {
        table.id = await this.nextId(table.branchid, schema, inputClient);
        const result = await DatabaseCommand.insert(table, `${schema}.${ITableTableName}`, inputClient);
        return result as ITable;
    },
    updateTable: async (table: ITable, schema: string, inputClient?: PoolClient): Promise<ITable | null> => {
        const rows = await DatabaseCommand.update(table, `${schema}.${ITableTableName}`, [{ id: table.id,branchid:table.branchid }], inputClient);
        if (rows.length) return rows[0] as ITable;
        return null;
    },
    getTables: async (condition: object[], schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<ITable[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${ITableTableName}`, condition, lockStrength, inputClient);
        return rows as ITable[];
    },
    getTableById: async (table: ITable, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<ITable[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${ITableTableName}`, [{ id: table.id,branchid: table.branchid  }], lockStrength, inputClient);
        return rows as ITable[];
    },
    removeTableById: async (table: ITable, schema: string, inputClient?: PoolClient): Promise<ITable | null> => {
        const rows = await DatabaseCommand.delete(`${schema}.${ITableTableName}`, [{ id: table.id,branchid: table.branchid }], inputClient);
        if (rows.length) return rows[0] as ITable;
        return null;
    }
}
