import {PoolClient} from "pg";
import { IIngredient } from "../models/IIngredient";
import {IReceipt} from "../models/IReceipt";
import DatabaseCommand from "../utils/DatabaseCommand";
import IngredientServices from "./IngredientServices";
import ReceiptIngredientServices from "./ReceiptIngredientServices";

const IReceiptTableName = 'receipts'

export default {
  IReceiptTableName,
  async getReceipts(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IReceipt[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IReceiptTableName}`, conditions, lockStrength, inputClient) as IReceipt[];
  },
  async createReceipts(objects: object[], schema: string, inputClient?: PoolClient): Promise<IReceipt[]> {
    const tableName = `${schema}.${IReceiptTableName}`;
    return await Promise.all(objects.map(async obj => await DatabaseCommand.insert(obj, tableName, inputClient))) as IReceipt[];
  },
  async createReceipt(object: any, schema: string, inputClient?: PoolClient): Promise<IReceipt> {
    return await DatabaseCommand.insert(object, `${schema}.${IReceiptTableName}`, inputClient) as IReceipt;
  },
  async updateReceipt(receipt: IReceipt, schema: string, inputClient?: PoolClient): Promise<IReceipt | null> {
    const rows = await DatabaseCommand.update(receipt, `${schema}.${IReceiptTableName}`, [{id: receipt.id}], inputClient);
    if (rows.length) return rows[0] as IReceipt;
    return null;
  },
  async updateReceipts(object: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IReceipt[]> {
    return await DatabaseCommand.update(object, `${schema}.${IReceiptTableName}`, conditions, inputClient) as IReceipt[];
  },
  async deleteReceipts(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IReceipt[]> {
    return await DatabaseCommand.delete(`${schema}.${IReceiptTableName}`, conditions, inputClient) as IReceipt[];
  },
  async getReceiptDetail(receiptid: number, schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IIngredient[]> {
    const result = await DatabaseCommand.customQuery(`select * from ${schema}.${ReceiptIngredientServices.IReceiptIngredientTableName} left join ${schema}.${IngredientServices.IIngredientTableName}
     on ${schema}.${ReceiptIngredientServices.IReceiptIngredientTableName}.ingredientid = ${schema}.${IngredientServices.IIngredientTableName}.id where receiptid = $1`, [receiptid], inputClient);
    return result.rows as IIngredient[];
  }
}
