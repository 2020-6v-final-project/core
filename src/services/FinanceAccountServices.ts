import {PoolClient} from "pg";
import {IFinanceAccount} from "../models/IFinanceAccount";
import DatabaseCommand from "../utils/DatabaseCommand";

const IFinanceAccountTableName = 'financeaccounts'

export default {
  IFinanceAccountTableName,
  async getFinanceAccounts(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IFinanceAccount[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IFinanceAccountTableName}`, conditions, lockStrength, inputClient) as IFinanceAccount[];
  },
  async createFinanceAccounts(objects: object[], schema: string, inputClient?: PoolClient): Promise<IFinanceAccount[]> {
    const tableName = `${schema}.${IFinanceAccountTableName}`;
    return await Promise.all(objects.map(async obj => await DatabaseCommand.insert(obj, tableName, inputClient))) as IFinanceAccount[];
  },
  async updateFinanceAccounts(object: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IFinanceAccount[]> {
    return await DatabaseCommand.update(object, `${schema}.${IFinanceAccountTableName}`, conditions, inputClient) as IFinanceAccount[];
  },
  async deleteFinanceAccounts(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IFinanceAccount[]> {
    return await DatabaseCommand.delete(`${schema}.${IFinanceAccountTableName}`, conditions, inputClient) as IFinanceAccount[];
  }
}
