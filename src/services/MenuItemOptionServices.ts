import {PoolClient} from "pg";
import {IMenuItemOption} from "../models/IMenuItemOption";
import DatabaseCommand from "../utils/DatabaseCommand";

const IMenuItemOptionTableName = 'menus_items_options';

export default {
  IMenuItemOptionTableName,
  async getMenuItemOptions(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IMenuItemOption[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IMenuItemOptionTableName}`, conditions, lockStrength, inputClient) as IMenuItemOption[];
  },
  async createMenuItemOptions(objects: object[], schema: string, inputClient?: PoolClient): Promise<IMenuItemOption[]> {
    const tableName = `${schema}.${IMenuItemOptionTableName}`;
    return await Promise.all(objects.map(async obj => await DatabaseCommand.insert(obj, tableName, inputClient))) as IMenuItemOption[];
  },
  async updateMenuItemOptions(object: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IMenuItemOption[]> {
    return await DatabaseCommand.update(object, `${schema}.${IMenuItemOptionTableName}`, conditions, inputClient) as IMenuItemOption[];
  },
  async deleteMenuItemOptions(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IMenuItemOption[]> {
    return await DatabaseCommand.delete(`${schema}.${IMenuItemOptionTableName}`, conditions, inputClient) as IMenuItemOption[];
  }
}
