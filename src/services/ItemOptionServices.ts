import {PoolClient} from "pg";
import {IItemOption} from "../models/IItemOption";
import DatabaseCommand from "../utils/DatabaseCommand";

const IItemOptionTableName = 'items_options'

export default {
  IItemOptionTableName,
  async getItemOptions(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IItemOption[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IItemOptionTableName}`, conditions, lockStrength, inputClient) as IItemOption[];
  },
  async createItemOption(itemOption: IItemOption, schema: string, inputClient?: PoolClient): Promise<IItemOption> {
    const result = await DatabaseCommand.insert(itemOption, `${schema}.${IItemOptionTableName}`, inputClient);
    return result as IItemOption;
  },
  async createItemOptions(objects: object[], schema: string, inputClient?: PoolClient): Promise<IItemOption[]> {
    const tableName = `${schema}.${IItemOptionTableName}`;
    return await Promise.all(objects.map(async obj => await DatabaseCommand.insert(obj, tableName, inputClient))) as IItemOption[];
  },
  async updateItemOptions(object: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IItemOption[]> {
    return await DatabaseCommand.update(object, `${schema}.${IItemOptionTableName}`, conditions, inputClient) as IItemOption[];
  },
  async deleteItemOptions(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IItemOption[]> {
    return await DatabaseCommand.delete(`${schema}.${IItemOptionTableName}`, conditions, inputClient) as IItemOption[];
  }
}
