import { PoolClient } from "pg";
import DbSingleton from "../config/database";
import { IOrder } from "../models/IOrder";
import DatabaseCommand from "../utils/DatabaseCommand";
const IOrderTableName = "orders";
const IOrderItemTableName = "orders_items";

export default {
    IOrderTableName,
    createOrder: async (order: any, schema: string, inputClient?: PoolClient): Promise<IOrder> => {
        const result = await DatabaseCommand.insert(order, `${schema}.${IOrderTableName}`, inputClient);
        return result as IOrder;
    },
    updateOrder: async (order: IOrder, schema: string, inputClient?: PoolClient): Promise<IOrder | null> => {
        const rows = await DatabaseCommand.update(order, `${schema}.${IOrderTableName}`, [{id: order.id}], inputClient);
        if (rows.length) return rows[0] as IOrder;
        return null;
    },
    getOrders: async (condition: object[], schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IOrder[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IOrderTableName}`, condition, lockStrength, inputClient);
        return rows as IOrder[];
    }, 
    getOrdersWithDetail: async (orderId: number, schema: string, inputClient?: PoolClient, loclStrength?: string): Promise<any> => {
        console.log(`select * from ${schema}.${IOrderTableName} left join ${schema}.${IOrderItemTableName} on ${schema}.${IOrderTableName}.id = ${schema}.${IOrderItemTableName}.orderid where orderid = $1`);
        const result = await DatabaseCommand.customQuery(`select * from ${schema}.${IOrderTableName} left join ${schema}.${IOrderItemTableName} on ${schema}.${IOrderTableName}.id = ${schema}.${IOrderItemTableName}.orderid where orderid = $1`, [orderId], inputClient);
        return result.rows;
    }
}