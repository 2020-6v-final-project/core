import {PoolClient} from "pg";
import {IReceiptIngredient} from "../models/IReceiptIngredient";
import DatabaseCommand from "../utils/DatabaseCommand";

const IReceiptIngredientTableName = 'receipts_ingredients'

export default {
  IReceiptIngredientTableName,
  async getReceiptIngredients(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IReceiptIngredient[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IReceiptIngredientTableName}`, conditions, lockStrength, inputClient) as IReceiptIngredient[];
  },
  async createReceiptIngredients(objects: object[], schema: string, inputClient?: PoolClient): Promise<IReceiptIngredient[]> {
    const tableName = `${schema}.${IReceiptIngredientTableName}`;
    return await Promise.all(objects.map(async obj => await DatabaseCommand.insert(obj, tableName, inputClient))) as IReceiptIngredient[];
  },
  async createReceiptIngredient(objects: object, schema: string, inputClient?: PoolClient): Promise<IReceiptIngredient> {
    return await DatabaseCommand.insert(objects, `${schema}.${IReceiptIngredientTableName}`, inputClient) as IReceiptIngredient;
  },
  async updateReceiptIngredients(object: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IReceiptIngredient[]> {
    return await DatabaseCommand.update(object, `${schema}.${IReceiptIngredientTableName}`, conditions, inputClient) as IReceiptIngredient[];
  },
  async deleteReceiptIngredients(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IReceiptIngredient[]> {
    return await DatabaseCommand.delete(`${schema}.${IReceiptIngredientTableName}`, conditions, inputClient) as IReceiptIngredient[];
  },
  async removeOne(object: IReceiptIngredient, schema: string, inputClient?: PoolClient): Promise<IReceiptIngredient | null> {
    const rows = await DatabaseCommand.delete(`${schema}.${IReceiptIngredientTableName}`, [{receiptid: object.receiptid, vendorid: object.vendorid, ingredientid: object.ingredientid}], inputClient);
    if (rows.length) return rows[0] as IReceiptIngredient;
    return null;
  }
}
