import {PoolClient} from "pg";
import {IDeposit} from "../models/IDeposit";
import DatabaseCommand from "../utils/DatabaseCommand";

const IDepositTableName = 'deposits'

export default {
  IDepositTableName,
  async getDeposits(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IDeposit[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IDepositTableName}`, conditions, lockStrength, inputClient) as IDeposit[];
  },
  async createDeposits(objects: object[], schema: string, inputClient?: PoolClient): Promise<IDeposit[]> {
    const tableName = `${schema}.${IDepositTableName}`;
    return await Promise.all(objects.map(async obj => await DatabaseCommand.insert(obj, tableName, inputClient))) as IDeposit[];
  },
  async updateDeposits(object: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IDeposit[]> {
    return await DatabaseCommand.update(object, `${schema}.${IDepositTableName}`, conditions, inputClient) as IDeposit[];
  },
  async deleteDeposits(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IDeposit[]> {
    return await DatabaseCommand.delete(`${schema}.${IDepositTableName}`, conditions, inputClient) as IDeposit[];
  }
}
