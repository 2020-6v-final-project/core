import { PoolClient } from "pg";
import { IRequest } from "../models/IRequest";
import DatabaseCommand from "../utils/DatabaseCommand";

const IRequestTableName = 'requests';
export default {
    IRequestTableName,
    async createRequest(request: any, schema: string, inputClient?: PoolClient): Promise<IRequest> {
        const result = await DatabaseCommand.insert(request, `${schema}.${IRequestTableName}`, inputClient);
        return result as IRequest;
    },
    async getRequests(condition: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IRequest[]> {
        const result = await DatabaseCommand.select(['*'], `${schema}.${IRequestTableName}`, condition, lockStrength, inputClient);
        return result as IRequest[];
    },
    async updateRequest(request: IRequest, schema: string, inputClient?: PoolClient): Promise<IRequest | null> {
        const rows = await DatabaseCommand.update(request, `${schema}.${IRequestTableName}`, [{id: request.id}], inputClient);
        if (rows.length) return rows[0] as IRequest;
        return null;
    }
}