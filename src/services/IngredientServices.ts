import { PoolClient } from "pg";
import { IIngredient } from "../models/IIngredient";
import DatabaseCommand from "../utils/DatabaseCommand";

const IIngredientTableName = "ingredients";
export default {
    IIngredientTableName,
    createIngredient: async (Ingredient: any, schema: string, inputClient?: PoolClient): Promise<IIngredient> => {
        const result = await DatabaseCommand.insert(Ingredient, `${schema}.${IIngredientTableName}`, inputClient);
        return result as IIngredient;
    },
    //update
    updateIngredient: async (Ingredient: IIngredient, schema: string, inputClient?: PoolClient): Promise<IIngredient | null> => {
        const rows = await DatabaseCommand.update(Ingredient, `${schema}.${IIngredientTableName}`, [{id: Ingredient.id}], inputClient);
        if (rows.length) return rows[0] as IIngredient;
        return null;
    },
    //get
    getIngredients: async (condition: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IIngredient[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IIngredientTableName}`, condition, lockStrength, inputClient);
        return rows as IIngredient[];
    }
}