import {PoolClient} from "pg";
import {IItemType} from "../models/IItemType";
import DatabaseCommand from "../utils/DatabaseCommand";

const IItemTypeTableName = 'itemtypes';

export default {
  IItemTypeTableName,
  async getItemTypes(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IItemType[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IItemTypeTableName}`, conditions, lockStrength, inputClient) as IItemType[];
  },
  async addItemType(itemType: object, schema: string, inputClient?: PoolClient): Promise<IItemType> {
    return await DatabaseCommand.insert(itemType, `${schema}.${IItemTypeTableName}`, inputClient) as IItemType;
  },
  async updateItemTypes(updatedInfo: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IItemType[]> {
    return await DatabaseCommand.update(updatedInfo, `${schema}.${IItemTypeTableName}`, conditions, inputClient) as IItemType[];
  },
  async deleteItemTypes(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IItemType[]> {
    return await DatabaseCommand.delete(`${schema}.${IItemTypeTableName}`, conditions, inputClient) as IItemType[];
  }
}
