import { PoolClient } from "pg";
import { IItem } from "../models/IItem";
import DatabaseCommand from "../utils/DatabaseCommand";

const IItemTableName = 'items';
export default {
    IItemTableName,
    //create
    createItem: async (item: any, schema: string, inputClient?: PoolClient): Promise<IItem> => {
        const result = await DatabaseCommand.insert(item, `${schema}.${IItemTableName}`, inputClient);
        return result as IItem;
    },
    //update
    updateItem: async (item: IItem, schema: string, inputClient?: PoolClient): Promise<IItem | null> => {
        const rows = await DatabaseCommand.update(item, `${schema}.${IItemTableName}`, [{id: item.id}], inputClient);
        if (rows.length) return rows[0] as IItem;
        return null;
    },
    //get
    getItems: async (condition: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IItem[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IItemTableName}`, condition, lockStrength, inputClient);
        return rows as IItem[];
    },
}