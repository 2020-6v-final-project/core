import {PoolClient} from "pg";
import {IAccount} from "../../models/public/IAccount";
import CompanyServices from "../public/CompanyServices";
import DatabaseCommand from "../../utils/DatabaseCommand";
import AccountServices from "../public/AccountServices";
import BusinessContractServices from "../public/BusinessContractServices";
import StatusServices from "../public/StatusServices";
import BusinessContractPaymentServices from "../public/BusinessContractPaymentServices";
import {ICompany} from "../../models/public/ICompany";
import {IUser} from "../../models/public/IUser";
import {IApi} from "../../models/public/IApi";
import PermissionServices from "../public/PermissionServices";
import {IRole} from "../../models/public/IRole";
import {IBusinessContract} from "../../models/public/IBusinessContract";
import UserRoleServices from "../public/UserRoleServices";
import ApiServices from "../public/ApiServices";
import RoleServices from "../public/RoleServices";

export default {
  async getAccountByUsernameAndCompanyId(username: string, companyId: number, lockStrength?: string, inputClient?: PoolClient): Promise<IAccount | null> {
    const rows = await AccountServices.getAccounts([{
      username,
      companyid: companyId,
      statusid: 2
    }], lockStrength, inputClient);
    if (rows.length) return rows[0] as IAccount;
    return null;
  },
  async getAccountByIdAndCompanyId(id: number, companyId: number, lockStrength?: string, inputClient?: PoolClient): Promise<IAccount | null> {
    const rows = await AccountServices.getAccounts([{
      id,
      companyid: companyId,
      statusid: 2
    }], lockStrength, inputClient);
    if (rows.length) return rows[0] as IAccount;
    return null;
  },
  async checkBusinessContractOfAvailableCompany(company: ICompany, inputClient?: PoolClient): Promise<IBusinessContract | null> {
    const businessContracts = await BusinessContractServices.getBusinessContracts([{
      companyid: company.id,
      // statusid: 6 //valid contract
      isprimary: true
    }], DatabaseCommand.lockStrength.SHARE, inputClient);
    if (!businessContracts.length) return null;
    const thisDate = new Date();
    const thisBusinessContract = businessContracts[0]; //only one primary contract can be found
    if (thisBusinessContract.statusid === 6) { //valid
      const endDate = new Date(thisBusinessContract.start.getTime());
      endDate.setMonth(endDate.getMonth() + thisBusinessContract.quantity);
      if (endDate >= thisDate) return thisBusinessContract;
    }
    const payments = await BusinessContractPaymentServices.getBusinessContractPayments([{
      businesscontractid: thisBusinessContract.id, companyid: company.id,
      statusid: 8 //PAID payment
    }], DatabaseCommand.lockStrength.UPDATE, inputClient);
    if (!payments.length) {
      await StatusServices.setStatusCodeToObjects(BusinessContractServices.IBusinessContractTableName, 'INVALID', [{
        id: thisBusinessContract.id,
        companyid: thisBusinessContract.companyid
      }], '', inputClient);
      return null;
    }
    const payment = payments[0];
    const newStartDate = thisDate;
    const newEndDate = new Date(newStartDate.getTime());
    newEndDate.setMonth(newEndDate.getMonth() + thisBusinessContract.quantity);
    thisBusinessContract.start = newStartDate;
    thisBusinessContract.expired = newEndDate;
    thisBusinessContract.statusid = 6;
    thisBusinessContract.statuscode = 'VALID';
    await Promise.all([
      StatusServices.setStatusCodeToObjects(BusinessContractPaymentServices.IBusinessContractPaymentTableName, 'DONE', [{
        id: payment.id,
        companyid: payment.companyid,
        businesscontractid: payment.businesscontractid
      }], '', inputClient),
      BusinessContractServices.updateBusinessContract(thisBusinessContract, inputClient)
    ]);
    return thisBusinessContract;
  },
  async checkPermissionReturnRole(user: IUser, roles: IRole[], typeId: number, originalUrl: string, method: string, server: string, inputClient?: PoolClient): Promise<IRole | null> {
    originalUrl = originalUrl.split('?')[0];
    for (const role of roles) {
      const permissionIds = (await PermissionServices.getPermissionsOfRoleIdFilterWithTypeId(role.id, typeId, 'CORE', inputClient)).map(permission => permission.id);
      if (!permissionIds.length) continue;
      const apis = await ApiServices.getApisOfPermissionIds(permissionIds, inputClient);
      for (const api of apis) {
        const regex = new RegExp(api.route);
        const testResult = regex.test(originalUrl);
        if (testResult && method === api.method) return role;
      }
    }
    return null;
  },
  async getRolesAndItsPermissionsOfUser(user: IUser, typeId: number, lockStrength?: string, inputClient?: PoolClient): Promise<any[]> {
    const roles = await RoleServices.getRolesOfUser(user, lockStrength, inputClient);
    return Promise.all(roles.map(async role => {
      const thisRole = role as any;
      thisRole.haspermissions = await PermissionServices.getPermissionsOfRoleIdsFilterWithTypeId([thisRole.id], typeId, 'CORE', inputClient);
      // thisRole.nothavepermissions = (await PermissionServices.getPermissions([{
      //   server: 'CORE'
      // }], DatabaseCommand.lockStrength.KEY_SHARE, inputClient)).filter(permission => {
      //   const found = thisRole.haspermissions.find((item: { id: number; }) => item.id === permission.id);
      //   return found === undefined;
      // });
      return thisRole;
    }));
  }
}
