import { PoolClient } from "pg";
import { IRequestType } from "../../models/public/IRequestType";
import DatabaseCommand from "../../utils/DatabaseCommand";

const IRequestTableName = "public.requesttypes";

export default {
    async getRequestTypes(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IRequestType[]> {
        const rows = await DatabaseCommand.select(['*'], `${IRequestTableName}`, condition, lockStrength, inputClient);
        return rows as IRequestType[];
    }
}