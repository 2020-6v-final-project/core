import { PoolClient } from 'pg';
import { IUser } from '../../models/public/IUser';
import DatabaseCommand from "../../utils/DatabaseCommand";
import Functions from "../../utils/Functions";
import AccountServices from "./AccountServices";
import {setPassword} from "../../models/public/IAccount";
import {IRole} from "../../models/public/IRole";
import UserRoleServices from "./UserRoleServices";

const IUserTableName = 'public.users';

export default {
  IUserTableName,
  getUserById: async (userId: number, lockStrength?: string, inputClient?: PoolClient): Promise<IUser | null> => {
    const rows = await DatabaseCommand.select(['*'], IUserTableName, [{ id: userId }], lockStrength, inputClient);
    if (rows.length) return rows[0] as IUser;
    return null;
  },
  async getUsers(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IUser[]> {
    const rows = await DatabaseCommand.select(['*'], IUserTableName, condition, lockStrength, inputClient);
    return rows as IUser[];
  },
  async nextId(companyId: number, inputClient?: PoolClient): Promise<number> {
    return await DatabaseCommand.getNextSequenceId(`${IUserTableName}${companyId}_id_seq`, inputClient);
  },
  async createUser(user: { id: number, companyid: number }, inputClient?: PoolClient): Promise<IUser> {
    user.id = await this.nextId(user.companyid, inputClient);
    return await DatabaseCommand.insert(user, IUserTableName, inputClient) as Promise<IUser>;
  },
  async createUserWithAccount(user: { id: number, companyid: number, companycode: string }, inputClient?: PoolClient) {
    const insertedUser = await this.createUser(user, inputClient);
    let preUsername;
    const companyCodeLowerCase = insertedUser.companycode.toLowerCase();
    preUsername = `${insertedUser.lastname[0] || ''}${insertedUser.middlename[0] || ''}${insertedUser.firstname}`;
    preUsername = preUsername.toLowerCase();
    preUsername = Functions.nonAccentVietnamese(preUsername);
    let tempUsername = `${preUsername}@${companyCodeLowerCase}`;
    let accounts = await AccountServices.getAccounts([{
      username: tempUsername,
      companyid: insertedUser.companyid
    }], DatabaseCommand.lockStrength.KEY_SHARE, inputClient);
    let i = 1;
    while (accounts.length) {
      preUsername = preUsername + i;
      tempUsername = `${preUsername}@${companyCodeLowerCase}`;
      accounts = await AccountServices.getAccounts([{
        username: tempUsername,
        companyid: insertedUser.companyid
      }], DatabaseCommand.lockStrength.KEY_SHARE, inputClient);
      i = i + 1;
    }
    const password = companyCodeLowerCase + preUsername;
    let newAccount = {
      id: 0,
      userid: insertedUser.id,
      username: tempUsername,
      hash: '',
      salt: '',
      companyid: insertedUser.companyid,
    };
    setPassword(password, newAccount);
    const createdAccount = await AccountServices.createAccount(newAccount, inputClient);
    return {
      newUser: insertedUser,
      newAccount: {
        username: createdAccount.username,
        password
      }
    }
  },
  /**
   * Update / save user to database
   * @param user The user query from database
   * @param inputClient
   */
  updateOrSaveUser: async (user: IUser, inputClient?: PoolClient): Promise<IUser | null> => {
    const rows = await DatabaseCommand.update(user, IUserTableName, [{ id: user.id }], inputClient);
    if (rows.length > 0) return rows[0] as IUser;
    return null;
  },
  getAllUser: async (lockStrength?: string, inputClient?: PoolClient): Promise<IUser[]> => {
    const rows = await DatabaseCommand.select(['*'], IUserTableName, [], lockStrength, inputClient);
    return rows as IUser[];
  },
  //==feat-company-api-get-list-and-detail==/
  getAllUserByCompanyCode: async (companyCode: string, lockStrength?: string, inputClient?: PoolClient): Promise<IUser[]> => {
    const rows = await DatabaseCommand.select(['*'], IUserTableName, [{ companycode: companyCode }], lockStrength, inputClient);
    return rows as IUser[];
  },
  getAllUsersByCompanyId: async (companyId: number, lockStrength?: string, inputClient?: PoolClient): Promise<IUser[]> => {
    const rows = await DatabaseCommand.select(['*'], IUserTableName, [{ companyid: companyId }], lockStrength, inputClient);
    return rows as IUser[];
  },
  async getUsersOfRole(role: IRole, companyId: number, lockStrength?: string, inputClient?: PoolClient): Promise<IUser[]> {
    return await Promise.all((await UserRoleServices.getUserRoles([{
      roleid: role.id, companyid: companyId
    }], DatabaseCommand.lockStrength.KEY_SHARE, inputClient)).map(async userRole => {
      return (await this.getUsers([{
        id: userRole.userid, companyid: userRole.companyid
      }], lockStrength, inputClient))[0];
    }));
  }
}
