import {PoolClient} from "pg";
import {ICompanyRole} from "../../models/public/ICompanyRole";
import DatabaseCommand from "../../utils/DatabaseCommand";

const ICompanyRoleTableName = `public.companies_roles`

export default {
  ICompanyRoleTableName,
  async getCompanyRoles(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<ICompanyRole[]> {
    const rows = await DatabaseCommand.select(['*'], ICompanyRoleTableName, condition, lockStrength, inputClient);
    return rows as ICompanyRole[];
  }
}
