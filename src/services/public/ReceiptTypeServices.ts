import { PoolClient } from "pg";
import { IReceiptType } from "../../models/public/IReceiptType";
import DatabaseCommand from "../../utils/DatabaseCommand";

const IReceiptTypeTableName = 'public.receipttypes';
export default {
    async getReceiptTypes(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IReceiptType[]> {
        const rows = await DatabaseCommand.select(['*'], `${IReceiptTypeTableName}`, condition, lockStrength, inputClient);
        return rows as IReceiptType[];
    }
}