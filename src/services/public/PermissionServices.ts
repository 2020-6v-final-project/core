import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IPermission} from "../../models/public/IPermission";

const IPermissionTableName = 'public.permissions';

export default {
  async getPermissions(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IPermission[]> {
    const rows = await DatabaseCommand.select(['*'], IPermissionTableName, condition, lockStrength, inputClient);
    return rows as IPermission[];
  },
  async getPermissionsOfRoleIdsFilterWithTypeId(roleIds: number[], typeId: number, server: string, inputClient?: PoolClient): Promise<IPermission[]> {
    const result = await DatabaseCommand.customQuery(`SELECT * FROM public.permissions WHERE server = '${server}' AND id IN
(SELECT distinct permissionid FROM public.roles_permissions WHERE roleid IN (${roleIds.join(',')}) INTERSECT
SELECT distinct permissionid FROM public.contracttypes_permissions WHERE contracttypeid = ${typeId}) FOR KEY SHARE`, [], inputClient);
    return result.rows as IPermission[];
  },
  async getPermissionsOfRoleIdFilterWithTypeId(roleId: number, typeId: number, server: string, inputClient?: PoolClient): Promise<IPermission[]> {
    const result = await DatabaseCommand.customQuery(`SELECT * FROM public.permissions WHERE server = '${server}' AND id IN
(SELECT distinct permissionid FROM public.roles_permissions WHERE roleid = ${roleId} INTERSECT
SELECT distinct permissionid FROM public.contracttypes_permissions WHERE contracttypeid = ${typeId}) FOR KEY SHARE`, [], inputClient);
    return result.rows as IPermission[];
  },
}
