import { IAccount } from '../../models/public/IAccount';
import { PoolClient } from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";

const IAccountTableName = 'public.accounts';

export default {
  IAccountTableName,
  /**
   * Get Account from database
   * @param username
   * @param lockStrength
   * @param inputClient
   */
  getAccountByUsername: async (username: string, lockStrength?: string, inputClient?: PoolClient): Promise<IAccount | null> => {
    const rows = await DatabaseCommand.select(['*'], IAccountTableName, [{ username }], lockStrength, inputClient);
    if (rows.length) return rows[0] as IAccount;
    return null;
  },
  async nextId(companyId: number, inputClient?: PoolClient): Promise<number> {
    return await DatabaseCommand.getNextSequenceId(`${IAccountTableName}${companyId}_id_seq`, inputClient);
  },
  /**
   * Create new account
   * @param account
   * @param inputClient
   */
  async createAccount(account: { id: number, companyid: number }, inputClient?: PoolClient): Promise<IAccount> {
    account.id = await this.nextId(account.companyid, inputClient);
    return await DatabaseCommand.insert(account, IAccountTableName, inputClient) as Promise<IAccount>;
  },
  /**
   *
   * @param account
   * @param inputClient
   */
  updateOrSaveAccount: async (account: IAccount, inputClient?: PoolClient): Promise<IAccount | null> => {
    const rows = await DatabaseCommand.update(account, IAccountTableName, [{ id: account.id }], inputClient);
    if (rows.length) return rows[0] as IAccount;
    return null;
  },
  async getAccounts(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IAccount[]> {
    const rows = await DatabaseCommand.select(['*'], IAccountTableName, condition, lockStrength, inputClient);
    return rows as IAccount[];
  },
}
