import { PoolClient } from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import { IBusinessContractPayment } from "../../models/public/IBusinessContractPayment";
import { IBusinessContract } from "../../models/public/IBusinessContract";
import BusinessContractServices from "./BusinessContractServices";

const IBusinessContractPaymentTableName = 'public.businesscontracts_payments';

export default {
  IBusinessContractPaymentTableName,
  async getBusinessContractPayments(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IBusinessContractPayment[]> {
    const rows = await DatabaseCommand.select(['*'], IBusinessContractPaymentTableName, condition, lockStrength, inputClient);
    return rows as IBusinessContractPayment[];
  }
}
