import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IApi} from "../../models/public/IApi";

const IApiTableName = 'public.apis';

export default {
  IApiTableName,
  async getApisOfPermissionIds(permissionIds: number[], inputClient?: PoolClient): Promise<IApi[]> {
    const result = await DatabaseCommand.customQuery(`SELECT * FROM ${IApiTableName} WHERE id IN
(SELECT distinct apiid FROM public.permissions_apis WHERE permissionid IN (${permissionIds.join(',')})) FOR KEY SHARE`, [], inputClient);
    return result.rows as IApi[];
  }
}
