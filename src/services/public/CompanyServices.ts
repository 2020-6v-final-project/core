import { ICompany } from "../../models/public/ICompany";
import { PoolClient } from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";

const ICompanyTableName = 'public.companies';

export default {
  ICompanyTableName,
  getCompanyByCompanyId: async (companyId: number, lockStrength?:string, inputClient?: PoolClient): Promise<ICompany | null> => {
    const rows = await DatabaseCommand.select(['*'], ICompanyTableName, [{id: companyId}], lockStrength, inputClient);
    if (rows.length) return rows[0] as ICompany;
    return null;
  },
  ////==feat-company-api-get-list-and-detail==/
  getAllCompany: async (lockStrength?: string, inputClient?: PoolClient): Promise<ICompany[]> => {
    const rows = await DatabaseCommand.select(['*'], ICompanyTableName, [], lockStrength, inputClient);
    return rows as ICompany[];
  },
  getCompanyByCompanyCode: async (companyCode: String, lockStrength?:string, inputClient?: PoolClient): Promise<ICompany | null> => {
    const rows = await DatabaseCommand.select(['*'], ICompanyTableName, [{code: companyCode}], lockStrength, inputClient);
    if (rows.length) return rows[0] as ICompany;
    return null;
  },
  createCompany: async (company: any, inputClient?: PoolClient): Promise<ICompany> => {
    const rows = await DatabaseCommand.insert(company, ICompanyTableName, inputClient);
    return rows as ICompany;
  },
  updateCompanies: async (company: ICompany, inputClient?: PoolClient): Promise<ICompany[]> => {
    return await DatabaseCommand.update(company, ICompanyTableName, [{id: company.id}], inputClient) as ICompany[];
  },
  getCompanies: async (condition: object[], lockStrength?: string, inputClient?: PoolClient,): Promise<ICompany[]> => {
    const rows = await DatabaseCommand.select(['*'], ICompanyTableName, condition, lockStrength, inputClient);
    return rows as ICompany[];
  }
};
