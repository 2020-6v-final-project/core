import {PoolClient} from "pg";
import {IPaymentType} from "../../models/public/IPaymentType";
import DatabaseCommand from "../../utils/DatabaseCommand";

const IPaymentTypeTableName = 'public.paymenttypes'

export default {
  async getPaymentTypes(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IPaymentType[]> {
    return await DatabaseCommand.select(['*'], IPaymentTypeTableName, condition, lockStrength, inputClient) as IPaymentType[];
  }
}
