import {IUserRole} from "../../models/public/IUserRole";
import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";

const IUserRoleTableName = 'public.users_roles';

export default {
  IUserRoleTableName,
  getAllByUserId: async function (userId: number, lockStrength?: string, inputClient?: PoolClient): Promise<IUserRole[]> {
    const rows = await DatabaseCommand.select(['*'], IUserRoleTableName, [{userid: userId}], lockStrength, inputClient);
    return rows as IUserRole[];
  },
  getAllByRoleId: async function (roleId: number, lockStrength?: string, inputClient?: PoolClient): Promise<IUserRole[]> {
    const rows = await DatabaseCommand.select(['*'], IUserRoleTableName, [{roleid: roleId}], lockStrength, inputClient);
    return rows as IUserRole[];
  },
  getOne: async function (userId: number, roleId: number, lockStrength?: string, inputClient?: PoolClient): Promise<IUserRole | null> {
    const rows = await DatabaseCommand.select(['*'], IUserRoleTableName, [{userid: userId, roleid: roleId}], lockStrength, inputClient);
    if (rows.length) return rows[0] as IUserRole;
    return null;
  },
  addOne: async function (userRole: IUserRole, inputClient?: PoolClient): Promise<IUserRole> {
    const result = await DatabaseCommand.insert(userRole, IUserRoleTableName, inputClient);
    return result as IUserRole;
  },
  removeOne: async function (userRole: IUserRole, inputClient?: PoolClient): Promise<IUserRole | null> {
    const rows = await DatabaseCommand.delete(IUserRoleTableName, [userRole], inputClient);
    if (rows.length) return rows[0] as IUserRole;
    return null;
  },
  async getUserRoles(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IUserRole[]> {
    const rows = await DatabaseCommand.select(['*'], IUserRoleTableName, condition, lockStrength, inputClient);
    return rows as IUserRole[];
  },
  async addUserRole(userRole: IUserRole, inputClient?: PoolClient): Promise<IUserRole> {
    return await DatabaseCommand.insert(userRole, IUserRoleTableName, inputClient) as Promise<IUserRole>;
  },
  async removeUserRole(userRole: IUserRole, inputClient?: PoolClient): Promise<IUserRole | null> {
    const rows = await DatabaseCommand.delete(IUserRoleTableName, [userRole], inputClient);
    if (rows.length) return rows[0] as IUserRole;
    return null;
  }
}
