import { IBusinessContract } from '../../models/public/IBusinessContract';
import { PoolClient } from 'pg';
import DatabaseCommand from '../../utils/DatabaseCommand';

const IBusinessContractTableName = 'public.businesscontracts';

export default {
    IBusinessContractTableName,
    /**
     * get BusinessContract(s)
     * @param condition
     * @param lockStrength
     * @param inputClient
     */
    async getBusinessContracts(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IBusinessContract[]> {
        const rows = await DatabaseCommand.select(['*'], IBusinessContractTableName, condition, lockStrength, inputClient);
        return rows as IBusinessContract[];
    },
    /**
     * update BusinessContract by id
     * @param businessContract
     * @param inputClient
     */
    async updateBusinessContract(businessContract: IBusinessContract, inputClient?: PoolClient): Promise<IBusinessContract | null> {
        const rows = await DatabaseCommand.update(businessContract, IBusinessContractTableName, [{ id: businessContract.id, companyid: businessContract.companyid }], inputClient);
        return rows[0] as IBusinessContract;
    },
}
