import {IRole} from "../../models/public/IRole";

import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IUser} from "../../models/public/IUser";
import UserRoleServices from "./UserRoleServices";
import CompanyRoleServices from "./CompanyRoleServices";

const IRoleTableName = 'public.roles';

export default {
  IRoleTableName,
  getAll: async function (lockStrength?: string, inputClient?: PoolClient): Promise<IRole[]> {
    const rows = await DatabaseCommand.select(['*'], IRoleTableName, [], lockStrength, inputClient);
    return rows as IRole[];
  },
  getById: async function (id: number, lockStrength?: string, inputClient?: PoolClient): Promise<IRole | null> {
    const rows = await DatabaseCommand.select(['*'], IRoleTableName, [{id}], lockStrength, inputClient);
    if (rows.length) return rows[0] as IRole;
    return null;
  },
  getByCode: async function (code: string, lockStrength?: string, inputClient?: PoolClient): Promise<IRole | null> {
    const rows = await DatabaseCommand.select(['*'], IRoleTableName, [{code}], lockStrength, inputClient);
    if (rows.length) return rows[0] as IRole;
    return null;
  },
  async getRoles(condition: object[], companyId?: number, lockStrength?: string, inputClient?: PoolClient): Promise<IRole[]> {
    const rows = await DatabaseCommand.select(['*'], IRoleTableName, condition, lockStrength, inputClient);
    const roles = rows as IRole[];
    if (companyId) {
      const companyRoles = await CompanyRoleServices.getCompanyRoles([{
        companyid: companyId
      }], DatabaseCommand.lockStrength.KEY_SHARE, inputClient);
      roles.filter(role => {
        return role.isprimary || (companyRoles.find(companyRole => companyRole.companyid === companyId) !== undefined);
      });
    }
    return roles;
  },
  async getRolesOfUser(thisUser: IUser, lockStrength?: string, inputClient?: PoolClient): Promise<IRole[]> {
    return await Promise.all((await UserRoleServices.getUserRoles([{
      userid: thisUser.id, companyid: thisUser.companyid
    }], DatabaseCommand.lockStrength.KEY_SHARE, inputClient)).map(async userRole => {
      return (await this.getRoles([{
        id: userRole.roleid
      }], thisUser.companyid, lockStrength, inputClient))[0];
    }));
  },
  async addRoleToUser(thisUser: IUser, roleCode: string, inputClient?: PoolClient) {
    const role = (await this.getRoles([{
      code: roleCode
    }], thisUser.companyid, DatabaseCommand.lockStrength.KEY_SHARE, inputClient))[0];
    if (!role) throw 'Role code ' + roleCode + ' not found';
    const userRole = (await UserRoleServices.getUserRoles([{
      userid: thisUser.id, companyid: thisUser.companyid, roleid: role.id
    }], DatabaseCommand.lockStrength.KEY_SHARE, inputClient));
    if (!userRole) await UserRoleServices.addUserRole({
      userid: thisUser.id, companyid: thisUser.companyid, roleid: role.id
    }, inputClient);
  }
}
