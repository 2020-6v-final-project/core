import { IStatus } from "../../models/public/IStatus";
import { PoolClient } from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";

const IStatusTableName = 'public.status';

export default {
  IStatusTableName,
  /**
   * Update status for object and return the object if success
   * @param object
   * @param tableName
   * @param statusCode
   * @param lockStrength
   * @param inputClient
   */
  setStatus: async (object: any, tableName: string, statusCode: string, inputClient?: PoolClient): Promise<any> => {
    const rows = await DatabaseCommand.select(['*'], IStatusTableName, [{ code: statusCode }], DatabaseCommand.lockStrength.SHARE, inputClient);
    if (!rows.length) return null;
    const thisStatus = rows[0] as IStatus;
    const updatedRows = await DatabaseCommand.update({ statusid: thisStatus.id, statuscode: thisStatus.code }, tableName, [{ id: object.id }], inputClient);
    if (updatedRows.length) return updatedRows[0];
    return null;
  },
  async getStatus(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IStatus[]> {
    const rows = await DatabaseCommand.select(['*'], IStatusTableName, condition, lockStrength, inputClient);
    return rows as IStatus[];
  },
  async setStatusCodeToObjects(tableName: string, statusCode: string, condition: object[], schema?: string, inputClient?: PoolClient): Promise<{ statusid: number, statuscode: string }[]> {
    const rows = await this.getStatus([{ code: statusCode }], DatabaseCommand.lockStrength.SHARE, inputClient);
    if (!rows.length) return [];
    const thisStatus = rows[0] as IStatus;
    if (schema) tableName = `${schema}.${tableName}`;
    const updatedRows = await DatabaseCommand.update({ statusid: thisStatus.id, statuscode: thisStatus.code }, tableName, condition, inputClient);
    return updatedRows as { statusid: number, statuscode: string }[];
  }
}
