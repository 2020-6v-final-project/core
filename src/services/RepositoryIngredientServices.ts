import { PoolClient } from "pg";
import { IRepositoryIngredient } from "../models/IRepositoryIngredient";
import DatabaseCommand from "../utils/DatabaseCommand";

const IRepositoryIngredientTableName = 'repositories_ingredients';
export default {
    IRepositoryIngredientTableName,
    getAll: async (condition: object[], schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IRepositoryIngredient[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IRepositoryIngredientTableName}`, condition, lockStrength, inputClient);
        return rows as IRepositoryIngredient[];
    },
    getAllByRepoId: async (repoId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IRepositoryIngredient[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IRepositoryIngredientTableName}`, [{repositoryid: repoId}], lockStrength, inputClient);
        return rows as IRepositoryIngredient[];
    },
    getAllByIngredientId: async (ingreId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IRepositoryIngredient[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IRepositoryIngredientTableName}`, [{ingredientid: ingreId}], lockStrength, inputClient);
        return rows as IRepositoryIngredient[];
    },
    getOne: async(repoId: number, ingreId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IRepositoryIngredient | null> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IRepositoryIngredientTableName}`, [{repositoryid: repoId, ingredientid: ingreId}], lockStrength, inputClient);
        if (rows.length) return rows[0] as IRepositoryIngredient;
        return null;
    },
    addOne: async(repoIngre: IRepositoryIngredient, schema: string, inputClient?: PoolClient): Promise<IRepositoryIngredient> => {
        const result = await DatabaseCommand.insert(repoIngre, `${schema}.${IRepositoryIngredientTableName}`, inputClient);
        return result as IRepositoryIngredient;
    },
    removeOne: async(repoIngre: IRepositoryIngredient, schema: string, inputClient?: PoolClient): Promise<IRepositoryIngredient | null> => {
        const rows = await DatabaseCommand.delete(`${schema}.${IRepositoryIngredientTableName}`, [{repositoryid: repoIngre.repositoryid, ingredientid: repoIngre.ingredientid}], inputClient);
        if (rows.length) return rows[0] as IRepositoryIngredient;
        return null;
    },
    getMenuItem: async (condition: object[], schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IRepositoryIngredient[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IRepositoryIngredientTableName}`, condition, lockStrength, inputClient);
        return rows as IRepositoryIngredient[];
    }
}