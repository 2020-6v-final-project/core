import { PoolClient } from "pg";
import { IMenuItem } from "../models/IMenuItem";
import DatabaseCommand from "../utils/DatabaseCommand";
const IMenuItemTableName = 'menus_items';
const IItemTableName = "items";
export default {
    IMenuItemTableName,
    getAllByMenuId: async (menuId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IMenuItem[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IMenuItemTableName}`, [{menuid: menuId}], lockStrength, inputClient);
        return rows as IMenuItem[];
    },
    getAllByItemId: async (itemId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IMenuItem[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IMenuItemTableName}`, [{itemid: itemId}], lockStrength, inputClient);
        return rows as IMenuItem[];
    },
    getOne: async(itemId: number, menuId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IMenuItem | null> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IMenuItemTableName}`, [{menuid: menuId, itemid: itemId}], lockStrength, inputClient);
        if (rows.length) return rows[0] as IMenuItem;
        return null;
    },
    addOne: async(menuItem: IMenuItem, schema: string, inputClient?: PoolClient): Promise<IMenuItem> => {
        const result = await DatabaseCommand.insert(menuItem, `${schema}.${IMenuItemTableName}`, inputClient);
        return result as IMenuItem;
    },
    removeOne: async(menuItem: IMenuItem, schema: string, inputClient?: PoolClient): Promise<IMenuItem | null> => {
        const rows = await DatabaseCommand.delete(`${schema}.${IMenuItemTableName}`, [menuItem], inputClient);
        if (rows.length) return rows[0] as IMenuItem;
        return null;
    },
    getMenuItem: async (condition: object[], schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IMenuItem[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IMenuItemTableName}`, condition, lockStrength, inputClient);
        return rows as IMenuItem[];
    },
    getItemsByMenuId: async (menuId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<any> => {
        const result = await DatabaseCommand.customQuery(`SELECT * FROM ${schema}.${IItemTableName} LEFT JOIN ${schema}.${IMenuItemTableName} on ${schema}.${IItemTableName}.id = ${schema}.${IMenuItemTableName}.itemid WHERE menuid = $1`,
        [menuId], inputClient);
        return result.rows;
    }
}