import {PoolClient} from "pg";
import {IFinanceValidate} from "../models/IFinanceValidate";
import DatabaseCommand from "../utils/DatabaseCommand";

const IFinanceValidateTableName = 'financevalidates'

export default {
  IFinanceValidateTableName,
  async getFinanceValidates(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IFinanceValidate[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IFinanceValidateTableName}`, conditions, lockStrength, inputClient) as IFinanceValidate[];
  },
  async createFinanceValidates(objects: object[], schema: string, inputClient?: PoolClient): Promise<IFinanceValidate[]> {
    const tableName = `${schema}.${IFinanceValidateTableName}`;
    return await Promise.all(objects.map(async obj => await DatabaseCommand.insert(obj, tableName, inputClient))) as IFinanceValidate[];
  },
  async updateFinanceValidates(object: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IFinanceValidate[]> {
    return await DatabaseCommand.update(object, `${schema}.${IFinanceValidateTableName}`, conditions, inputClient) as IFinanceValidate[];
  },
  async deleteFinanceValidates(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IFinanceValidate[]> {
    return await DatabaseCommand.delete(`${schema}.${IFinanceValidateTableName}`, conditions, inputClient) as IFinanceValidate[];
  }
}
