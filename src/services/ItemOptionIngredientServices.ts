import {PoolClient} from "pg";
import {IItemOptionIngredient} from "../models/IItemOptionIngredient";
import DatabaseCommand from "../utils/DatabaseCommand";

const IItemOptionIngredientTableName = 'items_options_ingredients'

export default {
  IItemOptionIngredientTableName,
  async getItemOptionIngredients(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IItemOptionIngredient[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IItemOptionIngredientTableName}`, conditions, lockStrength, inputClient) as IItemOptionIngredient[];
  },
  async createItemOptionIngredient(itemOptionIngredient: IItemOptionIngredient, schema: string, inputClient?: PoolClient): Promise<IItemOptionIngredient> {
    const result = await DatabaseCommand.insert(itemOptionIngredient, `${schema}.${IItemOptionIngredientTableName}`, inputClient);
    return result as IItemOptionIngredient;
  },
  async createItemOptionIngredients(objects: object[], schema: string, inputClient?: PoolClient): Promise<IItemOptionIngredient[]> {
    const tableName = `${schema}.${IItemOptionIngredientTableName}`;
    return await Promise.all(objects.map(async obj => await DatabaseCommand.insert(obj, tableName, inputClient))) as IItemOptionIngredient[];
  },
  async updateItemOptionIngredients(object: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IItemOptionIngredient[]> {
    return await DatabaseCommand.update(object, `${schema}.${IItemOptionIngredientTableName}`, conditions, inputClient) as IItemOptionIngredient[];
  },
  async deleteItemOptionIngredients(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IItemOptionIngredient[]> {
    return await DatabaseCommand.delete(`${schema}.${IItemOptionIngredientTableName}`, conditions, inputClient) as IItemOptionIngredient[];
  }
}
