import {PoolClient} from "pg";
import {ICheck} from "../models/ICheck";
import DatabaseCommand from "../utils/DatabaseCommand";

const ICheckTableName = 'checks'

export default {
  ICheckTableName,
  async getChecks(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<ICheck[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${ICheckTableName}`, conditions, lockStrength, inputClient) as ICheck[];
  },
  async createCheck(object: any, schema: string, inputClient?: PoolClient): Promise<ICheck> {
    return await DatabaseCommand.insert(object, `${schema}.${ICheckTableName}`, inputClient) as ICheck;
  },
  async createChecks(objects: object[], schema: string, inputClient?: PoolClient): Promise<ICheck[]> {
    const tableName = `${schema}.${ICheckTableName}`;
    return await Promise.all(objects.map(async obj => await DatabaseCommand.insert(obj, tableName, inputClient))) as ICheck[];
  },
  async updateCheck(check: ICheck, schema: string, inputClient?: PoolClient): Promise <ICheck | null> {
    const result = await DatabaseCommand.update(check, `${schema}.${ICheckTableName}`, [{id: check.id}], inputClient);
    if (result.length) return result[0] as ICheck;
    return null;
  } ,
  async updateChecks(object: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<ICheck[]> {
    return await DatabaseCommand.update(object, `${schema}.${ICheckTableName}`, conditions, inputClient) as ICheck[];
  },
  async deleteChecks(conditions: object[], schema: string, inputClient?: PoolClient): Promise<ICheck[]> {
    return await DatabaseCommand.delete(`${schema}.${ICheckTableName}`, conditions, inputClient) as ICheck[];
  }
}
