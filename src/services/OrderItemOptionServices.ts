import {PoolClient} from "pg";
import {IOrderItemOption} from "../models/IOrderItemOption";
import DatabaseCommand from "../utils/DatabaseCommand";

const IOrderItemOptionTableName = 'items_options'

export default {
  IOrderItemOptionTableName,
  async getOrderItemOptions(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IOrderItemOption[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IOrderItemOptionTableName}`, conditions, lockStrength, inputClient) as IOrderItemOption[];
  },
  async createOrderItemOptions(objects: object[], schema: string, inputClient?: PoolClient): Promise<IOrderItemOption[]> {
    const tableName = `${schema}.${IOrderItemOptionTableName}`;
    return await Promise.all(objects.map(async obj => await DatabaseCommand.insert(obj, tableName, inputClient))) as IOrderItemOption[];
  },
  async updateOrderItemOptions(object: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IOrderItemOption[]> {
    return await DatabaseCommand.update(object, `${schema}.${IOrderItemOptionTableName}`, conditions, inputClient) as IOrderItemOption[];
  },
  async deleteOrderItemOptions(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IOrderItemOption[]> {
    return await DatabaseCommand.delete(`${schema}.${IOrderItemOptionTableName}`, conditions, inputClient) as IOrderItemOption[];
  }
}
