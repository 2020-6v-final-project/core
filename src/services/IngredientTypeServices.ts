import {PoolClient} from "pg";
import {IIngredientType} from "../models/IIngredientType";
import DatabaseCommand from "../utils/DatabaseCommand";

const IIngredientTypeTableName = 'ingredienttypes';

export default {
  IIngredientTypeTableName,
  async getIngredientTypes(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IIngredientType[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IIngredientTypeTableName}`, conditions, lockStrength, inputClient) as IIngredientType[];
  },
  async addIngredientType(itemType: object, schema: string, inputClient?: PoolClient): Promise<IIngredientType> {
    return await DatabaseCommand.insert(itemType, `${schema}.${IIngredientTypeTableName}`, inputClient) as IIngredientType;
  },
  async updateIngredientTypes(updatedInfo: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IIngredientType[]> {
    return await DatabaseCommand.update(updatedInfo, `${schema}.${IIngredientTypeTableName}`, conditions, inputClient) as IIngredientType[];
  },
  async deleteIngredientTypes(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IIngredientType[]> {
    return await DatabaseCommand.delete(`${schema}.${IIngredientTypeTableName}`, conditions, inputClient) as IIngredientType[];
  }
}
