import {PoolClient} from "pg";
import {IBranch} from "../models/IBranch";
import DatabaseCommand from "../utils/DatabaseCommand";
import {IUser} from "../models/public/IUser";
import UserBranchServices from "./UserBranchServices";
import CurrencyServices from "./public/CurrencyServices";

const IBranchTableName = 'branches';

export default {
  IBranchTableName,
  //create branch
  createBranch: async (branch: any, schema: string, inputClient?: PoolClient): Promise<IBranch> => {
    const result = await DatabaseCommand.insert(branch, `${schema}.${IBranchTableName}`, inputClient);
    return result as IBranch;
  },

  //update branch
  updateBranch: async (branch: IBranch, schema: string, inputClient?: PoolClient): Promise<IBranch | null> => {
    const rows = await DatabaseCommand.update(branch, `${schema}.${IBranchTableName}`, [{id: branch.id}], inputClient);
    if (rows.length) return rows[0] as IBranch;
    return null;
  },
  //get List branch with condition or not ?
  getBranches: async (condition: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IBranch[]> => {
    const rows = await DatabaseCommand.select(['*'], `${schema}.${IBranchTableName}`, condition, lockStrength, inputClient);
    return rows as IBranch[];
  },
  async getBranchesOfUser(user: IUser, lockStrength?: string, inputClient?: PoolClient, withCurrency?: boolean): Promise<IBranch[]> {
    return await Promise.all((await UserBranchServices.getUserBranches([{
      userid: user.id
    }], user.companycode, DatabaseCommand.lockStrength.KEY_SHARE, inputClient)).map(async userBranch => {
      const branch = (await this.getBranches([{
        id: userBranch.branchid
      }], user.companycode, lockStrength, inputClient))[0] as any;
      if (withCurrency) {
        branch.currency = (await CurrencyServices.getCurrencies([{
          id: branch.currencyid
        }]))[0] as any;
      }
      return branch;
    }))
  },
  async addUserToBranch(user: IUser, branch: IBranch, inputClient?: PoolClient) {
    const userBranches = (await UserBranchServices.getUserBranches([{
      userid: user.id, branchid: branch.id, companyid: user.companyid
    }], user.companycode, DatabaseCommand.lockStrength.KEY_SHARE, inputClient))[0];
    if (!userBranches) {
      branch.numberofusers++;
      await this.updateBranch(branch, user.companycode, inputClient);
      await UserBranchServices.addOne({
        userid: user.id, branchid: branch.id
      }, user.companycode, inputClient);
    }
  }
}
