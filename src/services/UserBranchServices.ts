import { PoolClient } from "pg";
import { IUserBranch } from "../models/IUserBranch";
import DatabaseCommand from "../utils/DatabaseCommand";

const IUserBranchTableName = 'users_branches';
export default {
    IUserBranchTableName,
    getAllUserId: async (userId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IUserBranch[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IUserBranchTableName}`, [{userid: userId}], lockStrength, inputClient);
        return rows as IUserBranch[];
    },
    getAllBranchId: async (branchId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IUserBranch[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IUserBranchTableName}`, [{branchid: branchId}], lockStrength, inputClient);
        return rows as IUserBranch[];
    },
    getOne: async(userId: number, branchId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IUserBranch | null> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IUserBranchTableName}`, [{userid: userId, branchid: branchId}], lockStrength, inputClient);
        if (rows.length) return rows[0] as IUserBranch;
        return null;
    },
    addOne: async(userBranch: IUserBranch, schema: string, inputClient?: PoolClient): Promise<IUserBranch> => {
        const result = await DatabaseCommand.insert(userBranch, `${schema}.${IUserBranchTableName}`, inputClient);
        return result as IUserBranch;
    },
    removeOne: async(userBranch: IUserBranch, schema: string, inputClient?: PoolClient): Promise<IUserBranch | null> => {
        const rows = await DatabaseCommand.delete(`${schema}.${IUserBranchTableName}`, [{userid: userBranch.userid, branchid: userBranch.branchid}], inputClient);
        if (rows.length) return rows[0] as IUserBranch;
        return null;
    },
    getMenuItem: async (condition: object[], schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IUserBranch[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IUserBranchTableName}`, condition, lockStrength, inputClient);
        return rows as IUserBranch[];
    },
    async getUserBranches(condition: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IUserBranch[]> {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IUserBranchTableName}`, condition, lockStrength, inputClient);
        return rows as IUserBranch[];
    }
}
