import {PoolClient} from "pg";
import {IOption} from "../models/IOption";
import DatabaseCommand from "../utils/DatabaseCommand";

const IOptionTableName = 'options'

export default {
  IOptionTableName,
  async getOptions(conditions: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IOption[]> {
    return await DatabaseCommand.select(['*'], `${schema}.${IOptionTableName}`, conditions, lockStrength, inputClient) as IOption[];
  },
  async createOptions(objects: object[], schema: string, inputClient?: PoolClient): Promise<IOption[]> {
    const tableName = `${schema}.${IOptionTableName}`;
    return await Promise.all(objects.map(async obj => await DatabaseCommand.insert(obj, tableName, inputClient))) as IOption[];
  },
  async updateOptions(object: object, conditions: object[], schema: string, inputClient?: PoolClient): Promise<IOption[]> {
    return await DatabaseCommand.update(object, `${schema}.${IOptionTableName}`, conditions, inputClient) as IOption[];
  },
  async deleteOptions(conditions: object[], schema: string, inputClient?: PoolClient): Promise<IOption[]> {
    return await DatabaseCommand.delete(`${schema}.${IOptionTableName}`, conditions, inputClient) as IOption[];
  }
}
