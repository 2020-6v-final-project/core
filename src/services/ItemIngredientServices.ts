import { PoolClient } from "pg";
import { IItemIngredient } from "../models/IItemIngredient";
import DatabaseCommand from "../utils/DatabaseCommand";

const IItemIngredientTableName = 'items_ingredients';
export default {
    IItemIngredientTableName,
    getAllByItemId: async (itemId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IItemIngredient[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IItemIngredientTableName}`, [{itemid: itemId}], lockStrength, inputClient);
        return rows as IItemIngredient[];
    },
    getAllByIngredientId: async (ingredientId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IItemIngredient[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IItemIngredientTableName}`, [{ingredientid: ingredientId}], lockStrength, inputClient);
        return rows as IItemIngredient[];
    },
    getOne: async(itemId: number, ingredientId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IItemIngredient | null> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IItemIngredientTableName}`, [{itemid: itemId, ingredientid: ingredientId}], lockStrength, inputClient);
        if (rows.length) return rows[0] as IItemIngredient;
        return null;
    },
    addOne: async(itemIngredient: IItemIngredient, schema: string, inputClient?: PoolClient): Promise<IItemIngredient> => {
        const result = await DatabaseCommand.insert(itemIngredient, `${schema}.${IItemIngredientTableName}`, inputClient);
        return result as IItemIngredient;
    },
    removeOne: async(itemIngreadient: IItemIngredient, schema: string, inputClient?: PoolClient): Promise<IItemIngredient | null> => {
        const rows = await DatabaseCommand.delete(`${schema}.${IItemIngredientTableName}`, [itemIngreadient], inputClient);
        if (rows.length) return rows[0] as IItemIngredient;
        return null;
    },
    getItemIngredient: async (condition: object[], schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IItemIngredient[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IItemIngredientTableName}`, condition, lockStrength, inputClient);
        return rows as IItemIngredient[];
    },
    updateItemIngredient: async (itemIngredient: IItemIngredient, schema: string, inputClient?: PoolClient): Promise<IItemIngredient | null> => {
        const rows = await DatabaseCommand.update(itemIngredient, `${schema}.${IItemIngredientTableName}`, [{itemid: itemIngredient.itemid, ingredientid: itemIngredient.ingredientid}], inputClient);
        if (rows.length) return rows[0] as IItemIngredient;
        return null;
    },
}