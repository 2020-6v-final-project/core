import { PoolClient } from "pg";
import { IRepository } from "../models/IRepository";
import DatabaseCommand from "../utils/DatabaseCommand";

const IRepositoryTableName = 'repositories';
export default {
    IRepositoryTableName,
    //create
    createRepository: async (repository: any, schema: string, inputClient?: PoolClient): Promise<IRepository> => {
        const result = await DatabaseCommand.insert({name:repository.name,address:repository.address}, `${schema}.${IRepositoryTableName}`, inputClient);
        return result as IRepository;
    },
    //update
    updateRepository: async (repository: IRepository, schema: string, inputClient?: PoolClient): Promise<IRepository | null> => {
        const rows = await DatabaseCommand.update(repository, `${schema}.${IRepositoryTableName}`, [{id: repository.id}], inputClient);
        if (rows.length) return rows[0] as IRepository;
        return null;
    },
    //get
    getRepositories: async (condition: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IRepository[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IRepositoryTableName}`, condition, lockStrength, inputClient);
        return rows as IRepository[];
    }
}