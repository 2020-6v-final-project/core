import { PoolClient } from "pg";
import { IVendorIngredient } from "../models/IVendorIngredient";
import DatabaseCommand from "../utils/DatabaseCommand";

const IVendorIngredientTableName = "vendors_ingredients";
export default {
    async getAll(condition: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IVendorIngredient[]> {
        return await DatabaseCommand.select(["*"], `${schema}.${IVendorIngredientTableName}`, condition, lockStrength, inputClient) as IVendorIngredient[];
    },
    async getAllByVendorId (vendorid: number, schema: string, lockStrength?: string, inputClient?:PoolClient): Promise<IVendorIngredient[]>{
        return await DatabaseCommand.select(["*"], `${schema}.${IVendorIngredientTableName}`, [{vendorid: vendorid}], lockStrength, inputClient) as IVendorIngredient[];
    },
    async getAllByIngrediendtId (ingredientid: number, schema: string, lockStrength?: string, inputClient?:PoolClient): Promise<IVendorIngredient[]>{
        return await DatabaseCommand.select(["*"], `${schema}.${IVendorIngredientTableName}`, [{ingredientid: ingredientid}], lockStrength, inputClient) as IVendorIngredient[];
    },
    async addOne (vendorIngre: any, schema: string, inputClient?:PoolClient): Promise<IVendorIngredient> {
        return await DatabaseCommand.insert(vendorIngre, `${schema}.${IVendorIngredientTableName}`, inputClient) as IVendorIngredient;
    },
    async removeOne(vendorIngre: IVendorIngredient, schema: string, inputClient?:PoolClient): Promise<IVendorIngredient | null> {
        const rows = await DatabaseCommand.delete(`${schema}.${IVendorIngredientTableName}`, [{vendorid: vendorIngre.vendorid, ingredientid: vendorIngre.ingredientid}], inputClient);
        if (rows.length) return rows[0] as IVendorIngredient;
        return null;
    },
    async getAllWithDetail(condition: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IVendorIngredient[]> {
        return (await DatabaseCommand.customQuery(`select * from ${schema}.${IVendorIngredientTableName} left join ${schema}.ingredients on ${schema}.${IVendorIngredientTableName}.ingredientid = ${schema}.ingredients.id;`, [],inputClient)).rows as any[];
    },
    //select * from cookit.vendors_ingredients left join cookit.ingredients on cookit.vendors_ingredients.vendorid = cookit.ingredients.id;
}