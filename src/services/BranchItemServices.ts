import { PoolClient } from "pg";
import { IBranchItem } from "../models/IBranchItem";
import DatabaseCommand from "../utils/DatabaseCommand";

const IBranchItemTableName = 'branches_items';
export default {
    IBranchItemTableName,
    getAllByBranchId: async (branchId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IBranchItem[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IBranchItemTableName}`,[{branchid: branchId}], lockStrength, inputClient);
        return rows as IBranchItem[];
    },
    getAllByItemId: async (itemId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IBranchItem[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IBranchItemTableName}`, [{itemid: itemId}], lockStrength, inputClient);
        return rows as IBranchItem[];
    },
    getOne: async (itemId: number, branchId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IBranchItem | null> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IBranchItemTableName}`, [{itemid: itemId, branchid: branchId}], lockStrength, inputClient);
        if (rows.length) return rows[0] as IBranchItem;
        return null;
    },
    addOne: async (branchItem: IBranchItem, schema: string, inputClient?: PoolClient): Promise<IBranchItem> => {
        const result = await DatabaseCommand.insert(branchItem, `${schema}.${IBranchItemTableName}`, inputClient);
        return result as IBranchItem;
    },
    removeOne: async (branchItem: IBranchItem, schema: string, inputClient?: PoolClient): Promise<IBranchItem | null> => {
        const rows = await DatabaseCommand.delete(`${schema}.${IBranchItemTableName}`, [{branchid: branchItem.branchid, itemid: branchItem.itemid}], inputClient);
        if (rows.length) return rows[0] as IBranchItem;
        return null;    
    }, 
    getBranchItem: async (condition: object[], schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IBranchItem[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IBranchItemTableName}`, condition, lockStrength, inputClient);
        return rows as IBranchItem[];
    }
}