import { PoolClient } from "pg";
import { IOrderItem } from "../models/IOrderItem";
import DatabaseCommand from "../utils/DatabaseCommand";

const IOrderItemTableName = 'orders_items';
const IItemTableName = 'items';
export default {
    IOrderItemTableName,
    getAllByOrderId: async (orderId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IOrderItem[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IOrderItemTableName}`, [{orderid: orderId}], lockStrength, inputClient);
        return rows as IOrderItem[];
    },
    getAllByItemId: async (itemId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IOrderItem[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IOrderItemTableName}`, [{itemid: itemId}], lockStrength, inputClient);
        return rows as IOrderItem[];
    },
    getOne: async(orderId: number, itemId: number, schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IOrderItem | null> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IOrderItemTableName}`, [{oderid: orderId, itemid: itemId}], lockStrength, inputClient);
        if (rows.length) return rows[0] as IOrderItem;
        return null;
    },
    addOne: async(orderItem: IOrderItem, schema: string, inputClient?: PoolClient): Promise<IOrderItem> => {
        const result = await DatabaseCommand.insert(orderItem, `${schema}.${IOrderItemTableName}`, inputClient);
        return result as IOrderItem;
    },
    removeOne: async(orderItem: IOrderItem, schema: string, inputClient?: PoolClient): Promise<IOrderItem | null> => {
        const rows = await DatabaseCommand.delete(`${schema}.${IOrderItemTableName}`, [{orderid: orderItem.orderid, itemid: orderItem.itemid}], inputClient);
        if (rows.length) return rows[0] as IOrderItem;
        return null;
    },
    updateOne: async (orderItem: IOrderItem, schema: string, inputClient?: PoolClient): Promise<IOrderItem | null> => {
        const rows = await DatabaseCommand.update(orderItem, `${schema}.${IOrderItemTableName}`,
         [{orderid: orderItem.orderid, itemid: orderItem.itemid}], inputClient);
        if (rows.length) return rows[0] as IOrderItem;
        return null;
    }
    ,
    getMenuItem: async (condition: object[], schema: string, inputClient?: PoolClient, lockStrength?: string): Promise<IOrderItem[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IOrderItemTableName}`, condition, lockStrength, inputClient);
        return rows as IOrderItem[];
    },
    getOrdersWithDetail: async (orderId: number, schema: string, inputClient?: PoolClient, loclStrength?: string): Promise<any> => {
        const result = await DatabaseCommand.customQuery(`select * from ${schema}.${IOrderItemTableName} left join ${schema}.${IItemTableName} on ${schema}.${IItemTableName}.id = ${schema}.${IOrderItemTableName}.itemid where orderid = $1`, [orderId], inputClient);
        return result.rows;
    }
}