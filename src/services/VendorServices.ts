import { PoolClient } from "pg";
import { IVendor } from "../models/IVendor";
import DatabaseCommand from "../utils/DatabaseCommand";

const IVendorTableName = "vendors";
export default {
    IVendorTableName,
    async createVendor(vendor: any, schema: string, inputClient?: PoolClient): Promise<IVendor> {
        return await DatabaseCommand.insert(vendor, `${schema}.${IVendorTableName}`, inputClient) as IVendor;
    },
    async updateVendor(vendor: IVendor, schema: string, inputClient?: PoolClient): Promise<IVendor | null> {
        const result = await DatabaseCommand.update(vendor, `${schema}.${IVendorTableName}`, [{id: vendor.id}], inputClient);
        if (result.length) return result[0] as IVendor;
        return null;
    },
    async getVendors(condition: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<IVendor[]> {
        return await DatabaseCommand.select(["*"], `${schema}.${IVendorTableName}`, condition, lockStrength, inputClient) as IVendor[];
    }
}