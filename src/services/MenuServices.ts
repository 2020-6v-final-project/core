import { PoolClient } from "pg";
import { IItem } from "../models/IItem";
import { IMenu } from "../models/IMenu";
import DatabaseCommand from "../utils/DatabaseCommand";
import ItemServices from "./ItemServices";
import MenuItemServices from "./MenuItemServices";

const IMenuTableName = 'menus';
export default {
    IMenuTableName,
    createMenu: async (menu: any, schema: string, inputClient?: PoolClient): Promise<IMenu> => {
        const result = await DatabaseCommand.insert(menu, `${schema}.${IMenuTableName}`, inputClient);
        return result as IMenu;
    },
    updateMenu: async (menu: IMenu, schema: string, inputClient?: PoolClient): Promise<IMenu | null> =>{
        const rows = await DatabaseCommand.update(menu, `${schema}.${IMenuTableName}`, [{id: menu.id}], inputClient);
        if (rows.length) return rows[0] as IMenu;
        return null;
    },
    getMenus: async (condition: object[], schema: string,  lockStrength?: string,inputClient?: PoolClient): Promise<IMenu[]> => {
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IMenuTableName}`, condition, lockStrength, inputClient);
        return rows as IMenu[];
    },
    getMenuDetail: async(condition: object[], schema: string, lockStrength?: string, inputClient?: PoolClient): Promise<any> => {
        let listMenuDetail = [];
        const listItem = await ItemServices.getItems([], schema, lockStrength, inputClient) as IItem[];
        const rows = await DatabaseCommand.select(['*'], `${schema}.${IMenuTableName}`, condition, lockStrength, inputClient) as IMenu[];
        for (let imenu of rows) {
            const listItemMenu = await MenuItemServices.getAllByMenuId(imenu.id, schema, inputClient, lockStrength);
            const listHasItem = [];
            const listNotHasItem = [];
            for (let item of listItem) {
                if (listItemMenu.find(value => value.itemid === item.id)) {
                    listHasItem.push(item);
                } else {
                    listNotHasItem.push(item);
                }
            }
            listMenuDetail.push({imenu, listHasItem, listNotHasItem});
        }
        return listMenuDetail;
    },
    removeOne: async(menu: IMenu, schema: string, inputClient?: PoolClient): Promise<IMenu | null> => {
        const rows = await DatabaseCommand.delete(`${schema}.${IMenuTableName}`, [{id:menu.id}], inputClient);
        if (rows.length) return rows[0] as IMenu;
        return null;
    },
}