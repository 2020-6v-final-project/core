export interface IMenuItemOption {
	menuid: number;
	itemid: number;
	optionid: number;
	price: number;
}