export interface IBranchTax {
    branchid: number;
    taxid: number;
}