export interface IVendor {
    id: number;
    name: string;
    phonenumber: string;
    address: string;
    createdat: Date;
}