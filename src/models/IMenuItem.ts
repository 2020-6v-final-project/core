export interface IMenuItem {
    menuid: number;
    itemid: number;
    price: number;
}