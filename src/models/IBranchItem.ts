export interface IBranchItem {
    branchid: number;
    itemid: number;
    currentquantity: number;
    statusid: number;
    statuscode: string;
}