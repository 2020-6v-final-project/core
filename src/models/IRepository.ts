export interface IRepository {
    id: number;
    name: string;
    address: string;
    statusid: number;
    statuscode: string;
}