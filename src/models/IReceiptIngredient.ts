export interface IReceiptIngredient {
  vendorid: number;
  receiptid: number;
  ingredientid: number;
  factorydate: Date;
  expiredat: Date;
  price: number;
  quantity: number;
  total: number;
}
