export interface IIngredientType {
  id: number;
  name: string;
}
