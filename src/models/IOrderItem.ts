export interface IOrderItem {
    line: number;
    orderid: number;
    itemid: number;
    itemprice: number;
    optionsprice: number;
    quantity: number;
    total: number;
    note: string;
}
