export interface IFinanceAccount{
    id: number;
    currentbalance: number;
    branchid: number;
    accounttype: string;
    currencyid: number;
    statusid: number;
    statuscode: string;
}
