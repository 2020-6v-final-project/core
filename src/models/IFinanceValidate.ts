export interface IFinanceValidate {
  id: number;
  accountid: number;
  currentbalance: number;
  actualbalance: number;
  difference: number;
  note: string;
  validatedat: Date;
  validatedby: number;
}
