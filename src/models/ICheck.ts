export interface ICheck {
  id: number;
  accountid: number;
  oldbalance: number;
  debitamount: number;
  fee: number;
  newbalance: number;
  note: string;
  createdat: Date;
  createdby: number;
  receiptid: number;
  approvedby: number;
  approvedat: Date;
  statusid: number;
  statuscode: string;
  approvedrequestid: number;
}
