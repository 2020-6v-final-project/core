export interface IRepositoryIngredient {
    line: number;
    receiptid: number;
    ingredientid: number;
    startquantity: number;
    receivedat: Date;
    factorydate: Date;
    expiredat: Date;
    currentquantity: number;
    statusid: number;
    statuscode: string;
    repositoryid: number;
}
