export interface IItem {
    id: number;
    name: string;
    unit: string;
    price: number;
    lastmodified: Date;
    statusid: number;
    statuscode: string;
    recipe: string;
    description: string;
    imgpath: string;
    typeid: number;
    createdat: Date;
}
