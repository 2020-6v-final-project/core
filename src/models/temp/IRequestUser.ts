import {IUser} from "../public/IUser";
import {IAccount} from "../public/IAccount";
import {IBusinessContract} from "../public/IBusinessContract";
import {ICompany} from "../public/ICompany";
import {IRole} from "../public/IRole";

export interface IRequestUser extends IUser {
  branchIds: number[];
  account: IAccount;
  businessContract: IBusinessContract;
  company: ICompany;
  validRole: IRole;
}
