export interface IIngredient {
    id: number;
    name: string;
    unit: string;
    statusid: number;
    statuscode: string;
    imgpath: string;
    typeid: number;
    lastmodified: Date;
}
