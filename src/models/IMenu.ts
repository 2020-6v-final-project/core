export interface IMenu {
    id: number;
    name: string;
    statusid: number;
    statuscode: string;
    imgpath: string;
    description: string;
    createdat: Date;
    createdby: number;
    approvedrequestid: number;
    predata: string;
}
