export interface IItemOptionIngredient {
  itemid: number;
  optionid: number;
  ingredientid: number;
  ingredientquantity: number;
}
