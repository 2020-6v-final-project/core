export interface ITable {
    id: number;
    branchid: number;
    slots: number;
    statusid: number;
    statuscode: string;
    floor: string;
    name: string;
}
