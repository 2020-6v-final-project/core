export interface IReceipt {
  id: number;
  branchid: number;
  respositoryid: number;
  createdby: number;
  createdat: Date;
  total: number;
  statusid: number;
  statuscode: string;
  note: string;
  billdata: string;
  referenceid: number;
  receipttypeid: number;
  approvedrequestid: number;
}
