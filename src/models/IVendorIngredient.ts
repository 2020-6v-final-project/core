export interface IVendorIngredient {
    vendorid: number;
    ingredientid: number;
}