export interface IItemIngredient {
    itemid: number;
    ingredientid: number;
    quantity: number;
}