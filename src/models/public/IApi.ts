export interface IApi {
  id: number;
  name: string;
  route: string;
  description: string;
  method: string;
  server: string;
}
