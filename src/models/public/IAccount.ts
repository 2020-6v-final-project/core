import crypto from 'crypto';
import jsonwebtoken from 'jsonwebtoken';
import UtilityFunctions from "../../utils/Functions";

export interface IAccount {
  id: number;
  userid: number;
  username: string;
  hash: string;
  salt: string;
  validatetoken: string;
  validatetokenexpiredat: Date;
  statusid: number;
  statuscode: string;
  createdat: Date;
  companyid: number;
}

/**
 * This function create hash and salt from a password with a complex algorithm
 * @param password
 * @param account
 */
const setPassword = (password: string, account: { hash: string, salt: string }): void => {
  account.salt = crypto.pseudoRandomBytes(16).toString('hex');
  account.hash = crypto.pbkdf2Sync(password, account.salt, 1000, 64, 'sha512').toString('hex');
};

/**
 * This function compare the input password with the password of account
 * @param inputPassword
 * @param account
 */
const validPassword = (inputPassword: string, account: IAccount): boolean => {
  if (account.hash && account.salt) {
    const hash = crypto.pbkdf2Sync(inputPassword, account.salt, 1000, 64, 'sha512').toString('hex');
    return account.hash === hash;
  }
  return false;
}

/**
 * Generate jwt from account
 * @param account
 */
const generateJWT = (account: IAccount) => {
  const expiry = new Date();
  // The token will invalid after 24 hours
  expiry.setDate(expiry.getDate() + 1);
  return jsonwebtoken.sign({
    id: account.id,
    companyid: account.companyid,
    exp: parseInt(String(expiry.getTime() / 1000), 10)
  }, process.env.JWT_SECRET as jsonwebtoken.Secret);
}

/**
 * Get validate token to unlock account
 * @param account
 */
const getValidateToken = (account: IAccount) => {
  account.validatetoken = UtilityFunctions.randomString(6);
  const expiry = new Date();
  //Can use in one week
  expiry.setDate(expiry.getDate() + 7);
  account.validatetokenexpiredat = expiry;
}

export {
  setPassword,
  generateJWT,
  validPassword,
  getValidateToken
}
