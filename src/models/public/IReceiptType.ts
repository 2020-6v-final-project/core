export interface IReceiptType {
    id: number;
    code: string;
    name: string;
}