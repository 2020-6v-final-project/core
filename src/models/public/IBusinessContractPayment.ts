export interface IBusinessContractPayment {
  id: number;
  businesscontractid: number;
  companyid: number;
  total: number;
  createdat: Date;
  paidat: Date;
  description: string;
  statusid: number;
  statuscode: string;
}
