export interface IRequestType {
    id: number;
    code: string;
}