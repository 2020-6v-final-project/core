export interface IBranch {
  id: number;
  code: string;
  name: string;
  address: string;
  numberofusers: number;
  statusid: number;
  statuscode: string;
  mainmenu: number;
  currencyid: number;
  manager: number;
  repositoryid: number;
}
