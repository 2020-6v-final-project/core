export interface IOrder {
    id: number;
    branchid: number;
    userid: number;
    total: number;
    tableid: number;
    createdat: Date;
    paidat: Date;
    statusid: number;
    statuscode: string;
    note: string;
    paymenttypeid: string;
    paymentdeposit: number;
    taxcalculated: boolean;
    taxamount: number;
    totalamount: number;
    billdata: string;
}
