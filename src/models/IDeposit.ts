export interface IDeposit {
  id: number;
  accountid: number;
  creditamount: number;
  fee: number;
  statusid: number;
  receivedat: Date;
  oldbalance: number;
  newbalance: number;
  createdby: number;
  orderid: number;
  note: string;
  checkid: number;
  statuscode: string;
}
