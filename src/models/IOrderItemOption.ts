export interface IOrderItemOption {
  line: number;
  orderid: number;
  itemid: number;
  optionid: number;
  optionprice: number;
}
