import bodyParser from "body-parser";
import config from "config";
import cookieParser from "cookie-parser";
import cors from "cors";
import * as dotenv from "dotenv";
import express, { Application } from "express";
import passport from "passport";
import swaggerUi from "swagger-ui-express";
import * as swaggerDocument from "./swagger.json";
dotenv.config({ path: ".env" });


import("./config/passport");

const app: Application = express();
const mode: string | undefined = process.env.NODE_ENV;

app.use(cors());
app.use(express.json());
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(bodyParser.json());
app.use(cookieParser());
app.use(passport.initialize());

import("./routes").then(async result => await result.default(app));

const PORT = process.env.PORT || config.get(`${mode}.port`);

app.listen(PORT, () => {
  app.use("/swagger", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  console.log(".......................................");
  console.log(config.get(`${mode}.name`));
  console.log(`Port:\t\t${config.get(`${mode}.port`)}`);
  console.log(`Mode:\t\t${config.get(`${mode}.mode`)}`);
  console.log(".......................................");
});
