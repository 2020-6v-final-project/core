// import { IUser } from '../models/IUser';
// import { IAccount } from '../models/IAccount';
// import { ICompany } from '../models/ICompany';
// import * as faker from 'faker';
//
// export const userSample: IUser = {
//     id: faker.random.number({ min: 7, max: 1000 }),
//     firstname: faker.name.firstName(),
//     middlename: faker.name.middleName(),
//     lastname: faker.name.lastName(),
//     email: faker.internet.email(),
//     dateofbirth: faker.date.past(),
//     address: faker.address.streetAddress() + ", " + faker.address.streetName() + ", " + faker.address.city() + ", " + faker.address.country(),
//     phone: faker.phone.phoneNumber("##############"),
//     ismale: faker.random.number({ min: 1, max: 2}) % 2 === 0,
//     joinedat: faker.date.past(),
//     validatetoken: faker.random.uuid(),
//     validatetokenexpiredat: faker.date.future(),
//     statusid: 2,
//     statuscode: "AVAILABLE",
//     companycode: "COOKIT",
//     companyid: 1,
//     branchid: 1,
//     branchcode: "COOKIT-1",
// }
//
// export const accountSample: IAccount = {
//     id: faker.random.number({ min: 7, max: 1000 }),
//     userid: faker.random.number({ min: 7, max: 1000 }),
//     username: faker.internet.userName(),
//     hash: faker.random.uuid(),
//     salt: faker.random.uuid(),
//     validatetoken: faker.random.uuid(),
//     validatetokenexpiredat: faker.date.future(),
//     statusid: 2,
//     statuscode: "AVAILABLE",
//     createdat: new Date(),
// }
//
// export const companySample: ICompany = {
//     id: faker.random.number({ min: 7, max: 1000 }),
//     code: "COOKIT",
//     name: faker.name.findName(),
//     owner: null,
//     statusid: 2,
//     statuscode: 'AVAILABLE',
//     numberofusers: 6,
//     numberofbranches: 1,
//     businesscontract: 1
// }
//
