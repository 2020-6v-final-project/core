// import {expect} from 'chai';
// import StatusServices from '../services/StatusServices';
// import UserServices from '../services/UserServices';
// import {userSample} from './sampleTestCase';
// import {PoolClient} from "pg";
// import DatabaseCommand from "../utils/DatabaseCommand";
//
// let user = userSample;
//
// let client: PoolClient;
//
// describe('Status services Test', () => {
//   before(async function () {
//     client = await DatabaseCommand.newClient();
//     await DatabaseCommand.beginTransaction(client);
//   });
//
//   after(async function () {
//     await DatabaseCommand.rollbackTransaction(client);
//     client.release();
//   });
//
//   it('#Get status by status id success', async () => {
//     const result = await StatusServices.getById(1, undefined, client);
//     expect(result).to.be.an('object');
//     expect(result?.code).equal('LOCKED');
//   });
//
//   it('#Get status by status id fail', async () => {
//     const result = await StatusServices.getById(-1, undefined, client);
//     expect(result).to.be.null;
//   });
//
//   it('#Set status success', async () => {
//     const userTest = await UserServices.createUser(user, client);
//     if (userTest !== null) user = {...user, id: userTest.id};
//
//     const result = await StatusServices.setStatus(user, UserServices.IUserTableName, "UNAVAILABLE", client);
//     expect(result).to.be.an('object');
//     expect(result?.statusid).equal(5);
//     expect(result?.statuscode).equal('UNAVAILABLE');
//   });
//
//   it('#Set status fail', async () => {
//     const result = await StatusServices.setStatus(user, UserServices.IUserTableName, "UNKNOWN", client);
//     expect(result).to.be.null;
//   });
// });
