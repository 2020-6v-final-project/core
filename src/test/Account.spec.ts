// import {expect} from 'chai';
// import AccountServices from '../services/AccountServices';
// import UserService from '../services/UserServices';
// import {accountSample, userSample} from './sampleTestCase';
// import {PoolClient} from "pg";
// import DatabaseCommand from "../utils/DatabaseCommand";
//
// let account = accountSample;
//
// let client: PoolClient;
//
// describe("Account services Test", () => {
//   before(async function () {
//     client = await DatabaseCommand.newClient();
//     await DatabaseCommand.beginTransaction(client);
//   });
//
//   after(async function () {
//     await DatabaseCommand.rollbackTransaction(client);
//     client.release();
//   });
//
//   it("#Create account success", async () => {
//     const userTest = await UserService.createUser(userSample, client);
//     if (userTest !== null) account = {...account, userid: userTest.id};
//
//     const result = await AccountServices.createAccount(account, client);
//     expect(result).to.be.an('object');
//     expect(result?.username).equal(account.username);
//     if (result !== null) account = {...account, id: result.id};
//   });
//
//   it("#Get account by user name success", async () => {
//     const result = await AccountServices.getAccountByUsername(account.username, undefined, client);
//     expect(result).to.be.an('object');
//     expect(result?.username).equal(account.username);
//   });
//
//   it("#Get account by user name fail", async () => {
//     const result = await AccountServices.getAccountByUsername(account.username + "1", undefined, client);
//     expect(result).to.be.null;
//   });
//
//   it("#Update or save account success", async () => {
//     const result = await AccountServices.updateOrSaveAccount({
//       ...account,
//       statusid: 1,
//       statuscode: "LOCKED"
//     }, client);
//     expect(result).to.be.an('object');
//     expect(result?.statusid).equal(1);
//     expect(result?.statuscode).equal("LOCKED");
//   });
//
//   it("#Update or save account fail", async () => {
//     const result = await AccountServices.updateOrSaveAccount({
//       ...account,
//       id: -1
//     }, client);
//     expect(result).to.be.null;
//   });
// });
