// import {expect} from 'chai';
// import UserServices from '../services/UserServices';
// import {userSample} from './sampleTestCase';
// import {PoolClient} from "pg";
// import DatabaseCommand from "../utils/DatabaseCommand";
//
// let user = userSample;
//
// let client: PoolClient;
//
// describe("User services Test", () => {
//   before(async function () {
//     client = await DatabaseCommand.newClient();
//     await DatabaseCommand.beginTransaction(client);
//   });
//
//   after(async function () {
//     await DatabaseCommand.rollbackTransaction(client);
//     client.release();
//   });
//
//   it("#Create user success", async () => {
//     const result = await UserServices.createUser(user, client);
//     expect(result).to.be.an('object');
//     expect(result?.email).equal(user.email);
//     if (result !== null) user = {...user, id: result.id};
//   });
//
//   it("#Get user by Id success", async () => {
//     const result = await UserServices.getUserById(user.id, undefined, client);
//     expect(result).to.be.an('object');
//     expect(result?.email).equal(user.email);
//   });
//
//   it("#Get user by Id fail", async () => {
//     const result = await UserServices.getUserById(-user.id, undefined, client);
//     expect(result).to.be.null;
//   });
//
//   it('#Update or save user success', async () => {
//     const result = await UserServices.updateOrSaveUser({
//       ...user,
//       address: "TPHCM",
//     }, client);
//     expect(result).to.be.an('object');
//     expect(result?.address).equal('TPHCM');
//   });
//
//   it('#Update or save user fail', async () => {
//     const result = await UserServices.updateOrSaveUser({
//       ...user,
//       address: "TPHCM",
//       id: -1
//     }, client);
//     expect(result).to.be.null;
//   });
//
//   it('#Get all user success', async () => {
//     const result = await UserServices.getAllUser(undefined, client);
//     expect(result).to.be.an('array').not.empty;
//   });
//
//   it('#Get all by company code success', async () => {
//     const result = await UserServices.getAllUserByCompanyCode("COOKIT", undefined, client);
//     expect(result).to.be.an('array').not.empty;
//   });
//
//   it('#Get all by company code fail', async () => {
//     const result = await UserServices.getAllUserByCompanyCode("COOKIT_WITH_LENGTH_MORE_THAN_15", undefined, client);
//     expect(result).to.be.an('array').empty;
//   });
// });
